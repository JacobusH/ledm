import os
import ctypes

# load the shared library into c types
libname  = os.path.abspath(os.path.join(os.path.dirname(__file__), "../.build/libledm.so"))
lib_ledm = ctypes.CDLL(libname)

class RGBA(ctypes.Structure):
    _fields_ = [  ('r', ctypes.c_uint8)
                , ('g', ctypes.c_uint8)
                , ('b', ctypes.c_uint8)
                , ('a', ctypes.c_uint8) ]

    #def __init__(self, r, g, b, a):
    #    self.r = ctypes.c_uint8(r)
    #    self.g = ctypes.c_uint8(g)
    #    self.b = ctypes.c_uint8(b)
    #    self.a = ctypes.c_uint8(a)

# define return and args for each C func (when necessary)
lib_ledm.ledm_config_create.restype = ctypes.c_void_p
lib_ledm.ledm_initialize.restype    = ctypes.c_void_p
lib_ledm.ledm_paint_grid.restype    = ctypes.c_void_p
lib_ledm.ledm_canvas_get_grid_width.restype = ctypes.c_uint
lib_ledm.ledm_canvas_get_grid_height.restype = ctypes.c_uint

lib_ledm.ledm_config_destroy.argtypes       = [ctypes.c_void_p, ]
lib_ledm.ledm_config_load_default.argtypes  = [ctypes.c_void_p, ]
lib_ledm.ledm_initialize.argtypes           = [ctypes.c_void_p, ]
lib_ledm.ledm_paint_grid.argtypes           = [ctypes.c_void_p, ctypes.c_uint, ctypes.c_uint, RGBA ]
lib_ledm.ledm_destroy.argtypes              = [ctypes.c_void_p, ]
lib_ledm.ledm_io_push.argtypes              = [ctypes.c_void_p, ]
lib_ledm.ledm_canvas_get_grid_width.argtypes        = [ctypes.c_void_p, ]
lib_ledm.ledm_canvas_get_grid_height.argtypes        = [ctypes.c_void_p, ]

class LedmConfig: 

    def __init__(self):
        self.config = lib_ledm.ledm_config_create()

    def __del__(self):
        lib_ledm.ledm_config_destroy(self.config)

    def load_default(self):
        lib_ledm.ledm_config_load_default(self.config)
    

class LedmState:

    def __init__(self, ledm_config):
        self.state = lib_ledm.ledm_initialize(ledm_config.config)
    
    def __del__(self):
        lib_ledm.ledm_destroy(self.state)

    def paint_grid(self, x, y, color):
        lib_ledm.ledm_paint_grid(self.state, x, y, color)

    def io_push(self):
        lib_ledm.ledm_io_push(self.state)

    def get_grid_width(self):
        return lib_ledm.ledm_canvas_get_grid_width(self.state)

    def get_grid_height(self):
        return lib_ledm.ledm_canvas_get_grid_height(self.state)

# config = LedmConfig()
# config.load_default()
# state = LedmState(config)

# while(True):

#     color = RGBA(0, 0, 255, 255)

#     state.paint_grid(1, 1, color)
#     state.io_push()
