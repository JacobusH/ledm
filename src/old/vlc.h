#ifndef VLC_H
#define VLC_H

#include "interpreter.h"

extern cmd_t subcommands_vlc[];

void cmd_vlc_load(context *ctx, uint *args);
void cmd_vlc_play(context *ctx, uint *args);
void cmd_vlc_pause(context *ctx, uint *args);
void cmd_vlc_stop(context *ctx, uint *args);
void cmd_vlc_unload(context *ctx, uint *args);

#endif
