#include <pulse/simple.h>
#include <pulse/error.h>
#include <pulse/pulseaudio.h>
#include "BTrack.h"

extern "C" {
#include "analyze.h"
#include "interpreter.h"
}

analyze_data_t analyze_data = {NULL};

#define FRAMESIZE 1024
BTrack b(FRAMESIZE / 2, FRAMESIZE);
double frame[FRAMESIZE];
float buf[FRAMESIZE];

static const pa_sample_spec ss = {
	.format = PA_SAMPLE_FLOAT32LE,
	.rate =  44100,
	.channels = 1
};
static const pa_buffer_attr pb = {
	.maxlength = (uint32_t) -1,
	.tlength = 0, // gets ignored
	.prebuf = 0, // gets ignored
	.minreq = 0, // gets ignored
	.fragsize = FRAMESIZE*sizeof(float)
};

pa_simple *s = NULL;

cmd_t subcommands_analyze[] = {
	CMD_ARGUMENTS("init", cmd_analyze_init, "s"),
	CMD_SIMPLE("compute", cmd_analyze_compute),
	CMD_ARGUMENTS("beat", cmd_analyze_beat, "v"),
	CMD_SIMPLE("deinit", cmd_analyze_deinit),
	CMD_LIST_END
};

void cmd_analyze_init(context *ctx, uint *args) {
	int error;

	if (!(s = pa_simple_new(NULL, "ledm", PA_STREAM_RECORD, ctx->vars[*args].s, "audio recording for fourier analysis and beat tracking", &ss, NULL, &pb, &error))) {
		runtime_error(ctx, "Could not open pulseaudio source '%s': %s.", ctx->vars[*args].s, pa_strerror(error));
		return;
	}

	analyze_data.frame = (double*)&frame;
}

void cmd_analyze_compute(context *ctx, uint *args) {
	int error;

	if (pa_simple_read(s, buf, sizeof(buf), &error) < 0) {
		runtime_error(ctx, "pa_simple_read() failed: %s", pa_strerror(error));
		return;
	}
	for (int i = 0; i < FRAMESIZE; i += 1)
		frame[i] = (double) buf[i];

	b.processAudioFrame(frame);
}

void cmd_analyze_beat(context *ctx, uint *args) {
	ctx->vars_types[*args] = 'u';
	if (b.beatDueInCurrentFrame())
		ctx->vars[*args].u = 1;
	else
		ctx->vars[*args].u = 0;
}

void cmd_analyze_deinit(context *ctx, uint *args) {
	pa_simple_free(s);
	s = NULL;
	analyze_data.frame = NULL;
}

