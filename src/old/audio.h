#ifndef AUDIO_H
#define AUDIO_H

#include "interpreter.h"

typedef struct {
	double *frame;
} analyze_data_t;

extern analyze_data_t analyze_data;

extern cmd_t subcommands_analyze[];

void cmd_analyze_init(context *ctx, uint *args);
void cmd_analyze_compute(context *ctx, uint *args);
void cmd_analyze_beat(context *ctx, uint *args);
void cmd_analyze_deinit(context *ctx, uint *args);

#endif
