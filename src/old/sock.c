#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "sock.h"
#include "interpreter.h"
#include "paint.h"

#define MAX 80
#define SA struct sockaddr

struct {
	int sockfd;
	int connfd;
	struct sockaddr_in servaddr, cli;
} sock_data;

cmd_t subcommands_sock[] = {
	CMD_ARGUMENTS("init", cmd_sock_init, "u"),
	CMD_SIMPLE("listen", cmd_sock_listen),
	CMD_SIMPLE("poll", cmd_sock_poll),
	CMD_SIMPLE("paint", cmd_sock_paint),
	CMD_SIMPLE("deinit", cmd_sock_deinit),
	CMD_LIST_END
};

void cmd_sock_init(context *ctx, uint *args) {
	uint port = ctx->vars[*args].u;

	// socket creation and verification
	sock_data.sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_data.sockfd == -1) {
		runtime_error(ctx, "Socket creation failed.");
		return;
	}
	bzero(&sock_data.servaddr, sizeof(sock_data.servaddr));

	// assign IP, PORT
	sock_data.servaddr.sin_family = AF_INET;
	sock_data.servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	sock_data.servaddr.sin_port = htons(port);

	// Binding newly created socket to given IP and verification
	if ((bind(sock_data.sockfd, (SA*)&sock_data.servaddr, sizeof(sock_data.servaddr))) != 0) {
		runtime_error(ctx, "Socket bind failed.");
		return; // TOOD: clearup socket
	}
}

void cmd_sock_listen(context *ctx, uint *args) {
	if ((listen(sock_data.sockfd, 5)) != 0) {
		runtime_error(ctx, "Listening on socket failed.");
		return;
	}
	uint len = sizeof(sock_data.cli);

	// Accept the data packet from client and verification
	sock_data.connfd = accept(sock_data.sockfd, (SA*)&sock_data.cli, &len);
	if (sock_data.connfd < 0) {
		runtime_error(ctx, "Acccepting client connection failed.");
		return;
	}
	printf("Acccepted a client.\n");
}

void cmd_sock_poll(context *ctx, uint *args) {
	char buff[MAX];
	memset(buff, 0, MAX);

	// read the message from client and copy it in buffer
	if (read(sock_data.connfd, buff, sizeof(buff)) == -1) {
		runtime_error(ctx, "Cannot read from socket: %s", strerror(errno));
	}
	// print buffer which contains the client contents
	printf("Read from sock client: %s\n", buff);
}

void cmd_sock_paint(context *ctx, uint *args) {
	int n = write(sock_data.connfd, paint_data.buffer, paint_data.num_LEDs*3);
	if (n == -1)
		runtime_error(ctx, "Failed to write to socket: %s", strerror(errno));
}

void cmd_sock_deinit(context *ctx, uint *args) {
	close(sock_data.sockfd);
}
