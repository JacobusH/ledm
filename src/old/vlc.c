#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <vlc/vlc.h>

#include "vlc.h"
#include "interpreter.h"

struct {
	libvlc_media_t *media;
	libvlc_media_player_t *media_player;
	libvlc_instance_t *vlc_inst;
} vlc_data = {NULL, NULL, NULL};

cmd_t subcommands_vlc[] = {
	CMD_ARGUMENTS("load", cmd_vlc_load, "s"),
	CMD_SIMPLE("play", cmd_vlc_play),
	CMD_SIMPLE("pause", cmd_vlc_pause),
	CMD_SIMPLE("stop", cmd_vlc_stop),
	CMD_SIMPLE("unload", cmd_vlc_unload),
	CMD_LIST_END
};

void cmd_vlc_load(context *ctx, uint *args) {
	vlc_data.vlc_inst = libvlc_new(0, NULL);
	vlc_data.media_player = libvlc_media_player_new(vlc_data.vlc_inst);
	vlc_data.media = libvlc_media_new_path(vlc_data.vlc_inst, ctx->vars[*args].s);
	libvlc_media_player_set_media(vlc_data.media_player, vlc_data.media);
}

void cmd_vlc_play(context *ctx, uint *args) {
	libvlc_media_player_play(vlc_data.media_player);
}

void cmd_vlc_pause(context *ctx, uint *args) {
	libvlc_media_player_pause(vlc_data.media_player);
}

void cmd_vlc_stop(context *ctx, uint *args) {
	libvlc_media_player_stop(vlc_data.media_player);
}

void cmd_vlc_unload(context *ctx, uint *args) {
	libvlc_media_release(vlc_data.media);
	libvlc_media_player_release(vlc_data.media_player);
	libvlc_release(vlc_data.vlc_inst);
	vlc_data.media = NULL;
	vlc_data.media_player = NULL;
	vlc_data.vlc_inst = NULL;
}
