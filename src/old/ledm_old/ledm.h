#ifndef LEDM_H
#define LEDM_H

#include <stdint.h>
#include "interpreter.h"


// TYPES
struct snd_seq_event;
union SDL_Event;
typedef void (*on_input_handle)(struct context*, struct snd_seq_event *midi_event, union SDL_Event *sdl_event);
typedef void (*on_frame_handle)(struct context*);

struct led {
	struct color c;
	float x;
	float y;
};

struct painter {
	const char *name;
	const char *format;
	struct color (*func)(struct context *ctx, uint *args, float x, float y);
};

struct pattern {
	const char *name;
	const char *format;
	float (*func)(struct context *ctx, uint *args, float x, float y);
};

struct midi_ref {
	uint cc_index;
	uint note_index;
	uint timer_index;
	uint last_time;
};

struct state {
	int initialized;


	// Init file config
	int load_init;
	int load_init_set_by_user;
	char *init_file;


	// Serial output config and state
	int serial_enabled;
	int serial_enabled_set_by_user;
	char *serial_path;
	int serial_baudrate;
	int serial_socket;


	// Open Pixel Control config and state
	int opc_enabled;
	int opc_enabled_set_by_user;
	char *opc_host;
	int8_t opc_out;
	struct color *opc_buffer;


	// SDL config and state
	int sdl_enabled;
	int sdl_enabled_set_by_user;
	struct SDL_Renderer* sdl_renderer;
	struct SDL_Window* sdl_window;


	// MIDI config and state
	int midi_enabled;
	int midi_enabled_set_by_user;
	struct _snd_seq* midi_sequencer;
	int midi_port;
	int midi_channel;
	char *midi_autoconnect;
	struct midi_ref *midi_refs;


	// Plugin execution command line config
	int exec_plugin;
	char *plugin_path;
	char *plugin_params;


	// LED painting
	struct led *leds;
	uint num_leds;
	uint size_leds;
	struct painter *painters;
	uint num_painters;
	struct pattern *patterns;
	uint num_patterns;
	float canvas_width;
	float canvas_height;
	uint (*index_from_coords)(uint col, uint row);
	uint grid_width;
	uint grid_height;
	enum {
		OVERWRITE,
		MULTIPLY,
		SCREEN,
		ON_BLACK
	} paint_mode;


	// Frame config and state
	float fps;
	int measure_fps;
	uint usec_per_frame;


	// Scene config and state
	uint scene_selector;
	uint scene_current;
	uint emcee_section_pointer;
	uint emcee_section_count;
	uint emcee_section_var_block;
	uint emcee_current_loop; // only relevant if section pointer variable is set to 0. then, 0 signifies that emcee proceed does not jump, while any number i>0 indicates that emcee proceed should jump to instruction number i-1.
};




// FUNCTIONS


// Hooks called by the interpreter - implemented in hooks.c
struct state *create_state(); // called on create_context();
void destroy_state(struct state *state); // called on destroy_context();
void on_new_var(struct context *ctx, uint index, const char *name); // called whenever interpreter encounters a previously undefined variable


// Painting leds - implemented in paint.c
void add_paint_cmds(struct context *ctx); // adds 'paint' and 'paint-mode' commands
void add_led(struct state *state, float x, float y);
void add_painter(struct context *ctx, const char *name, const char *format, struct color (*func)(struct context*, uint*, float, float));
void add_pattern(struct context *ctx, const char *name, const char *format, float (*func)(struct context*, uint*, float, float));
void add_standard_patterns(struct context *ctx); // adds default patterns from patterns.c
void add_standard_painters(struct context *ctx); // adds default painters from painters.c
void paint_at_coords(struct context *ctx, uint col, uint row, struct color color);


// Manage MIDI, SDL, openpixelcontrol and serial input and output components - implemented in io.h
void add_io_cmds(struct context *ctx); // adds 'io' command
void io_init(struct context *ctx); // initializes io components, called by 'io init' command
void io_poll(struct context *ctx, on_input_handle on_input); // polls input components and updates ctx->time_ms, called by 'io poll' command. if specified, on_input gets called instead of standard handler
void io_push(struct context *ctx); // pushes to output components, called by 'io push' command
void io_loop(struct context *ctx, on_input_handle on_input, on_frame_handle on_frame); // convenience function to provide plugins with io loop
void set_fps(struct context *ctx, float fps); // sets fps. a value <= 0 turns off frame rate limiting
void wait_frame(struct context *ctx);


// MIDI input - implemented in midi.c
void midi_init(struct context *ctx);
void midi_poll(struct context *ctx, on_input_handle on_input);


// SDL output - implemented in sdl.c
void sdl_init(struct context *ctx);
void sdl_poll(struct context *ctx, on_input_handle on_input);
void sdl_push(struct context *ctx);


// Serial output - implemented in serial.c
void serial_init(struct context *ctx);
void serial_push(struct context *ctx);


// Open Pixel Control output - implemented in opc.c
void opc_init(struct context *ctx);
void opc_push(struct context *ctx);


// Scene manager - implemented in scene.c
void add_scene_cmds(struct context *ctx); // adds 'scene' command


#endif // LEDM_H

