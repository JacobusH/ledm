#include "interpreter.h"

// following conversion maps adapted from hsv2rgb.cpp from the FastLED library!

#define HSV_SECTION_6 (0x20)
#define HSV_SECTION_3 (0x40)
struct color hsv2rgb_spectrum(uint hue, uint sat, uint val) {
	// takes hue between 0 and 191, sat and value between 0 and 255

	struct color rgb;

	// The brightness floor is minimum number that all of
	// R, G, and B will be set to.
	byte invsat = 255 - sat;
	byte brightness_floor = (val * invsat) / 256;

	// The color amplitude is the maximum amount of R, G, and B
	// that will be added on top of the brightness_floor to
	// create the specific hue desired.
	byte color_amplitude = val - brightness_floor;

	// Figure out which section of the hue wheel we're in,
	// and how far offset we are withing that section
	byte section = ((byte)hue) / HSV_SECTION_3; // 0..2
	byte offset = ((byte)hue) % HSV_SECTION_3;  // 0..63

	byte rampup = offset; // 0..63
	byte rampdown = (HSV_SECTION_3 - 1) - offset; // 63..0

	// We now scale rampup and rampdown to a 0-255 range -- at least
	// in theory, but here's where architecture-specific decsions
	// come in to play:
	// To scale them up to 0-255, we'd want to multiply by 4.
	// But in the very next step, we multiply the ramps by other
	// values and then divide the resulting product by 256.
	// So which is faster?
	//   ((ramp * 4) * othervalue) / 256
	// or
	//   ((ramp	) * othervalue) /  64
	// It depends on your processor architecture.
	// On 8-bit AVR, the "/ 256" is just a one-cycle register move,
	// but the "/ 64" might be a multicycle shift process. So on AVR
	// it's faster do multiply the ramp values by four, and then
	// divide by 256.
	// On ARM, the "/ 256" and "/ 64" are one cycle each, so it's
	// faster to NOT multiply the ramp values by four, and just to
	// divide the resulting product by 64 (instead of 256).
	// Moral of the story: trust your profiler, not your insticts.

	// Since there's an AVR assembly version elsewhere, we'll
	// assume what we're on an architecture where any number of
	// bit shifts has roughly the same cost, and we'll remove the
	// redundant math at the source level:

	//  // scale up to 255 range
	//  //rampup *= 4; // 0..252
	//  //rampdown *= 4; // 0..252

	// compute color-amplitude-scaled-down versions of rampup and rampdown
	byte rampup_amp_adj   = (rampup   * color_amplitude) / (256 / 4);
	byte rampdown_amp_adj = (rampdown * color_amplitude) / (256 / 4);

	// add brightness_floor offset to everything
	byte rampup_adj_with_floor   = rampup_amp_adj   + brightness_floor;
	byte rampdown_adj_with_floor = rampdown_amp_adj + brightness_floor;


	if( section ) {
		if( section == 1) {
			// section 1: 0x40..0x7F
			rgb.r = brightness_floor;
			rgb.g = rampdown_adj_with_floor;
			rgb.b = rampup_adj_with_floor;
		} else {
			// section 2; 0x80..0xBF
			rgb.r = rampup_adj_with_floor;
			rgb.g = brightness_floor;
			rgb.b = rampdown_adj_with_floor;
		}
	} else {
		// section 0: 0x00..0x3F
		rgb.r = rampdown_adj_with_floor;
		rgb.g = rampup_adj_with_floor;
		rgb.b = brightness_floor;
	}
	return rgb;
}

#define K255 255
#define K171 171
#define K170 170
#define K85  85
byte scale8( byte i, byte scale) {
	return (((unsigned short)i) * (1+(unsigned short)(scale))) >> 8;
}
byte scale8_video( byte i, byte scale) {
	return (((int)i * (int)scale) >> 8) + ((i&&scale)?1:0);
}
struct color hsv2rgb_rainbow(uint _hue, uint _sat, uint _val) {
	struct color rgb;

	byte hue = _hue;
	byte sat = _sat;
	byte val = _val;

	byte offset = hue & 0x1F; // 0..31

	// offset8 = offset * 8
	byte offset8 = offset << 3;

	byte third = scale8( offset8, (256 / 3)); // max = 85

	if( ! (hue & 0x80) ) {
		// 0XX
		if( ! (hue & 0x40) ) {
			// 00X
			//section 0-1
			if( ! (hue & 0x20) ) {
				// 000
				//case 0: // R -> O
				rgb.r = K255 - third;
				rgb.g = third;
				rgb.b = 0;
			} else {
				// 001
				//case 1: // O -> Y
				rgb.r = K171;
				rgb.g = K85 + third ;
				rgb.b = 0;
			}
		} else {
			//01X
			// section 2-3
			if( !  (hue & 0x20) ) {
				// 010
				//case 2: // Y -> G
				//byte twothirds = (third << 1);
				byte twothirds = scale8( offset8, ((256 * 2) / 3)); // max=170
				rgb.r = K171 - twothirds;
				rgb.g = K170 + third;
				rgb.b = 0;
			} else {
				// 011
				// case 3: // G -> A
				rgb.r = 0;
				rgb.g = K255 - third;
				rgb.b = third;
			}
		}
	} else {
		// section 4-7
		// 1XX
		if( ! (hue & 0x40) ) {
			// 10X
			if( ! ( hue & 0x20) ) {
				// 100
				//case 4: // A -> B
				rgb.r = 0;
				//byte twothirds = (third << 1);
				byte twothirds = scale8( offset8, ((256 * 2) / 3)); // max=170
				rgb.g = K171 - twothirds; //K170?
				rgb.b = K85  + twothirds;
			} else {
				// 101
				//case 5: // B -> P
				rgb.r = third;
				rgb.g = 0;
				rgb.b = K255 - third;
			}
		} else {
			if( !  (hue & 0x20)  ) {
				// 110
				//case 6: // P -- K
				rgb.r = K85 + third;
				rgb.g = 0;
				rgb.b = K171 - third;
			} else {
				// 111
				//case 7: // K -> R
				rgb.r = K170 + third;
				rgb.g = 0;
				rgb.b = K85 - third;
			}
		}
	}

	// Scale down colors if we're desaturated at all
	// and add the brightness_floor to r, g, and b.
	if( sat != 255 ) {
		if( sat == 0) {
			rgb.r = 255; rgb.b = 255; rgb.g = 255;
		} else {
			//nscale8x3_video( r, g, b, sat);
			if( rgb.r ) rgb.r = scale8( rgb.r, sat);
			if( rgb.g ) rgb.g = scale8( rgb.g, sat);
			if( rgb.b ) rgb.b = scale8( rgb.b, sat);

			byte desat = 255 - sat;
			desat = scale8( desat, desat);

			byte brightness_floor = desat;
			rgb.r += brightness_floor;
			rgb.g += brightness_floor;
			rgb.b += brightness_floor;
		}
	}

	// Now scale everything down if we're at value < 255.
	if( val != 255 ) {

		val = scale8_video( val, val);
		if( val == 0 ) {
			rgb.r=0; rgb.g=0; rgb.b=0;
		} else {
			// nscale8x3_video( r, g, b, val);
			if( rgb.r ) rgb.r = scale8( rgb.r, val);
			if( rgb.g ) rgb.g = scale8( rgb.g, val);
			if( rgb.b ) rgb.b = scale8( rgb.b, val);
		}
	}
	return rgb;
} 
