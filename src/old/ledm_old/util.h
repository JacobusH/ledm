#include "interpreter.h"

/*
 * noise1234 Perlin noise by Stefan Gustavson
 * Downloaded from github.com/stegu/perlin-noise/
 *
 * For more information, see util/noise1234.c
 *
 */
extern float noise1( float x );
extern float noise2( float x, float y );
extern float noise3( float x, float y, float z );
extern float noise4( float x, float y, float z, float w );
extern float pnoise1( float x, int px );
extern float pnoise2( float x, float y, int px, int py );
extern float pnoise3( float x, float y, float z, int px, int py, int pz );
extern float pnoise4( float x, float y, float z, float w,
                              int px, int py, int pz, int pw );


/*
 * Adapted from hsv2rgb.cpp from the FastLED library!
 *
 */
struct color hsv2rgb_spectrum(uint hue, uint sat, uint val);
struct color hsv2rgb_rainbow(uint hue, uint sat, uint val);
