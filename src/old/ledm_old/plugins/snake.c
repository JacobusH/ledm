#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include "ledm.h"

// Compile-time configuration
#define MAX_SNAKE_LENGTH 100
#define COLOR_SNAKE1_HEAD (struct color){255,255,0}
#define COLOR_SNAKE2_HEAD (struct color){0,255,255}
#define COLOR_SNAKE1_BODY (struct color){100,100,0}
#define COLOR_SNAKE2_BODY (struct color){0,100,100}
#define COLOR_WALL (struct color){255,0,0}
#define COLOR_FOOD (struct color){0,255,0}
#define COLOR_BLANK (struct color){0,0,0}

const char* ledm_version = VERSION;

// Runtime configuration
int width = 8;
int height = 5;
float fps_initial = 6;
float fps_step = 0.1;
int brightness = 10;
int two_player = 0;

// Context reference
struct context *ctx = NULL;

typedef struct {
	int x;
	int y;
} position;

typedef enum {BLANK, WALL, FOOD, SNAKE1_HEAD, SNAKE2_HEAD, SNAKE1_BODY, SNAKE2_BODY} tile;

typedef enum {UP, DOWN, LEFT, RIGHT} direction;

typedef struct {
	int length;
	direction dir;
	position pos[MAX_SNAKE_LENGTH];
} snake;


// Game state data
tile* board = NULL;
struct color* buffer = NULL;
int uninitialized = 1;
int crash = 0;
snake player1, player2;
direction next_dir1, next_dir2;


position move_direction(position pos, direction dir) {
	position return_pos = pos;
	switch (dir) {
		case UP: return_pos.y++; break;
		case DOWN: return_pos.y--; break;
		case LEFT: return_pos.x--; break;
		case RIGHT: return_pos.x++; break;
	}
	return_pos.x = (return_pos.x + width) % width;
	return_pos.y = (return_pos.y + height) % height;
	return return_pos;
}

direction opposite_direction(direction dir) {
	switch (dir) {
		case UP: return DOWN;
		case DOWN: return UP;
		case LEFT: return RIGHT;
		case RIGHT: return LEFT;
		default: return LEFT;
	}
}

void erase_snakes() {
	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			if (board[x+width*y] != WALL && board[x+width*y] != FOOD) // if tile is covered by some snake part...
				board[x+width*y] = BLANK; // ...then remove it.
}

void draw_snake(int player_nr) {
	if (player_nr == 0 || player_nr == 1) {
		for (int i=0; i<player1.length; ++i) {
			int x = player1.pos[i].x;
			int y = player1.pos[i].y;

			if (x<width && y<height)
				board[x+width*y] = i==0? SNAKE1_HEAD : SNAKE1_BODY;
		}
	}


	if (player_nr == 0 || player_nr == 2) {
		for (int i=0; i<player2.length; ++i) {
			int x = player2.pos[i].x;
			int y = player2.pos[i].y;

			if (x<width && y<height)
				board[x+width*y] = i==0? SNAKE2_HEAD : SNAKE2_BODY;
		}
	}
}

void spawn_food() {

	// Try to find an empty spot by picking randomly
	for (int try=0; try<100; ++try) {
		int x = rand() % width;
		int y = rand() % height;

		if (board[x+width*y] == BLANK ) {
			board[x+width*y] = FOOD;
			return;
		}
	}

	// Try to find an empty spot by going through the whole board
	for (int x=0; x<width; ++x) {
		for (int y=0; y<height; ++y) {
			if (board[x+width*y] == BLANK ) {
				board[x+width*y] = FOOD;
				return;
			}
		}
	}

	// If there is no free space left for food, then tough luck.
}

void initialize_game() {
	uninitialized = 0;
	crash = 0;
	set_fps(ctx, fps_initial);

	// Blank everything
	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			board[x+width*y] = BLANK;

	// Initialize snakes
	player1.length = 1;
	player2.length = 1;
	player1.dir = RIGHT;
	player2.dir = LEFT;
	next_dir1 = player1.dir;
	next_dir2 = player2.dir;

	int x = width / 3;
	int y = height / 2;
	for (int i=0; i<player1.length; ++i) {
		player1.pos[i].x = x-i;
		player1.pos[i].y = y;
	}
	if (two_player) {
		x = 2 * width / 3;
		for (int i=0; i<player2.length; ++i) {
			player2.pos[i].x = x+i;
			player2.pos[i].y = y;
		}
	}
	draw_snake(0);

	// Spawn food
	spawn_food();
}


void draw_state() {

	for (int x=0; x<width; ++x) {
		for (int y=0; y<height; ++y) {
			switch (board[x+width*y]) {
				case BLANK: paint_at_coords(ctx, x, y, COLOR_BLANK); break;
				case WALL: paint_at_coords(ctx, x, y, COLOR_WALL);  break;
				case FOOD: paint_at_coords(ctx, x, y, COLOR_FOOD); break;
				case SNAKE1_HEAD: paint_at_coords(ctx, x, y, COLOR_SNAKE1_HEAD); break;
				case SNAKE2_HEAD: paint_at_coords(ctx, x, y, COLOR_SNAKE2_HEAD); break;
				case SNAKE1_BODY: paint_at_coords(ctx, x, y, COLOR_SNAKE1_BODY); break;
				case SNAKE2_BODY: paint_at_coords(ctx, x, y, COLOR_SNAKE2_BODY); break;
			}
		}
	}
}

void animation_end() {
	for (int i=1; i<8; i++) {
		erase_snakes();
		if (two_player)
			draw_snake(  (i%2) * ((crash%2)+1) );
		else if (i%2)
			draw_snake(1);
		draw_state();
		struct timespec ts;
		ts.tv_sec = 0;
		ts.tv_nsec = 300*1000000;
		nanosleep(&ts, NULL);
	}
}

void move_snake(int player_nr) {
	snake *player = player_nr==1? &player1 : &player2;
	position next_pos = move_direction(player->pos[0], player->dir);
	tile next_tile = board[next_pos.x+width*next_pos.y];

	if (next_tile != BLANK && next_tile != FOOD) {
		crash = player_nr;
	} else if (next_tile == BLANK) {

		// Move snake along
		for (int i=player->length; i>0; i--) {
			player->pos[i] = player->pos[i-1];
		}
		player->pos[0] = next_pos;

	} else if (next_tile == FOOD) {

		// Grow snake if possible, otherwise move along
		if (player->length+1 < MAX_SNAKE_LENGTH) {
			player->length++;
			for (int i=player->length; i>0; i--) {
				player->pos[i] = player->pos[i-1];
			}
			player->pos[0] = next_pos;
		} else {
			for (int i=player->length; i>0; i--) {
				player->pos[i] = player->pos[i-1];
			}
			player->pos[0] = next_pos;
		}

		// Speed snakes up
		set_fps(ctx, ctx->state->fps + fps_step);

		// Spawn new food
		spawn_food();

	}
}



void on_input(struct context *ctx, struct snd_seq_event *midi_event, union SDL_Event *sdl_event) {

	if (sdl_event && sdl_event->type != SDL_KEYDOWN) {
		switch (sdl_event->key.keysym.sym) {
			case SDLK_UP: next_dir1 = DOWN; break;
			case SDLK_DOWN: next_dir1 = UP; break;
			case SDLK_LEFT: next_dir1 = LEFT; break;
			case SDLK_RIGHT: next_dir1 = RIGHT; break;
			case SDLK_w: next_dir2 = UP; break;
			case SDLK_s: next_dir2 = DOWN; break;
			case SDLK_a: next_dir2 = LEFT; break;
			case SDLK_d: next_dir2 = RIGHT; break;
			case SDLK_q: ctx->quit = 1; break;
		}
	}
}

void on_frame(struct context *ctx) {

	if (uninitialized) {
		initialize_game();
		draw_state();
	}

	if (next_dir1 != opposite_direction(player1.dir))
		player1.dir = next_dir1;
	if (next_dir2 != opposite_direction(player2.dir))
		player2.dir = next_dir2;

	if (!two_player) {
		move_snake(1);
		erase_snakes();
		draw_snake(1);
	} else if ( rand() % 2 ) {
		move_snake(1);
		erase_snakes();
		draw_snake(0);
		move_snake(2);
		erase_snakes();
		draw_snake(0);
	} else {
		move_snake(2);
		erase_snakes();
		draw_snake(0);
		move_snake(1);
		erase_snakes();
		draw_snake(0);
	}

	if (crash) {
		animation_end();
		uninitialized = 1;
	} else {
		draw_state();
	}
}


void entry_point(struct context *ctx_arg, char *params) {

	ctx = ctx_arg;

	if (ctx->state->index_from_coords == NULL) {
		error("This game requires that grid drawing is supported!");
		return;
	}

	set_fps(ctx, fps_initial);

	width = ctx->state->grid_width;
	height = ctx->state->grid_height;

	// Set up initial game state
	board = malloc(width*height*sizeof(tile));
	buffer = malloc(width*height*sizeof(struct color));

	io_loop(ctx, on_input, on_frame);

	free(board);
	free(buffer);
}



