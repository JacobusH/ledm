// For now, required for compatibility.
#define _POSIX_C_SOURCE 200809L

// We will do some printing to the screen in these examples
#include <stdio.h>

// Needed when using the io_loop hook on_input to evaluate SDL input. In case
// you want to use the on_input hook but disregard SDL events, you can also
// use the forward declaration union SDL_Event; instead.
#include <SDL2/SDL.h>

// Needed when using the io_loop hook on_input to evaluate MIDI input. In case
// you want to use the on_input hook but disregard MIDI events, you can also
// use the forward declaration struct snd_seq_event; instead.
#include <alsa/asoundlib.h>

// Needed to interface with the rest of ledm
#include "ledm.h"


// ledm will warn on loading a plugin where this variable exists but does not match the version of ledm
const char* ledm_version = VERSION;

// Global state for the various example commands
struct color color = (struct color){255,0,0};

// Function forward declarations. Only entry_point is assumed to exist, and it
// either calls the other functions directly or hands references to them over to ledm
// via some api call.
void entry_point(struct context *ctx, char *params);
void cmd_example_moo(struct context *ctx, uint *args);
void cmd_example_echo(struct context *ctx, uint *args);
void cmd_example_help(struct context *ctx, uint *args);
void cmd_example_flash(struct context *ctx, uint *args);
void on_input(struct context*, struct snd_seq_event *midi_event, union SDL_Event *sdl_event);
void on_frame(struct context*);

// Table that describes the subcommands of the 'example' command so that it can
// be registered with ledm within entry_point.
struct cmd subcommands_example[] = {
	CMD_SIMPLE("", cmd_example_help),
	CMD_SIMPLE("moo", cmd_example_moo),
	CMD_ARGUMENTS("echo", cmd_example_echo, "s"),
	CMD_SIMPLE("help", cmd_example_help),
	CMD_SIMPLE("flash", cmd_example_flash),
	CMD_LIST_END
};

// Plugin entry point, called immediately after loading with a parameter string set by the user (otherwise NULL).
void entry_point(struct context *ctx, char *params) {
	printf("Hello from the example plugin for ledm v%s!\n", ledm_version);
	printf("Parameters passed to plugin: ");
	if (params)
		printf("%s\n\n", params);
	else
		puts("-none-\n");
	puts("Try out 'example'.");

	// register example command in command table
	add_cmd(ctx, CMD_SUBCOMMANDS("example", subcommands_example));
}



void cmd_example_moo(struct context *ctx, uint *args) {
	puts("\n"
	"            ^__^              \n"
	"  MOOOOO!   (oo)\\_______     \n"
	"            (__)\\       )\\  \n"
	"                ||----w | *   \n"
	"                ||     ||     \n");
}

void cmd_example_echo(struct context *ctx, uint *args) {
	printf("%s\n", ctx->vars[args[0]].s);
}

void cmd_example_help(struct context *ctx, uint *args) {
	puts("Usage of 'example':\n"
	"    example moo            Have you moo'ed today?\n"
	"    example echo <string>  Echos the string back at you.\n"
	"    example flash          Demonstrates an IO loop by flashing lights at you.\n"
	"                           You can change its color with the arrow keys!\n"
	"    example help           Show this help message.\n");
}

void cmd_example_flash(struct context *ctx, uint *args) {
	io_loop(ctx, on_input, on_frame);
}

void on_input(struct context *ctx, struct snd_seq_event *midi_event, union SDL_Event *sdl_event) {
	if (sdl_event && sdl_event->type == SDL_KEYDOWN) {
		switch (sdl_event->key.keysym.sym) {
			case SDLK_UP:    color = (struct color){255,0,0}; break;
			case SDLK_DOWN:  color = (struct color){0,255,0}; break;
			case SDLK_LEFT:  color = (struct color){0,0,255}; break;
			case SDLK_RIGHT: color = (struct color){99,0,99}; break;
		}
	}
}

void on_frame(struct context *ctx) {
	for (uint i=0; i<ctx->state->num_leds; ++i) {
		if (ctx->time_ms/800 % 2)
			ctx->state->leds[i].c = color;
		else
			ctx->state->leds[i].c = (struct color){0,0,0};
	}
}
