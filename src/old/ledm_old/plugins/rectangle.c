#include "ledm.h"

const char* ledm_version = VERSION;

uint width = 25;
uint height = 16;

uint index_from_coords(uint col, uint row) {
	if (col%2 ==0)
		return (height-1-row) + 2*height*(col/2);
	else
		return height + row + 2*height*(col/2);
}


void entry_point(struct context *ctx, char *params) {

	ctx->state->canvas_width = (float)width - 1.0;
	ctx->state->canvas_height = (float)height - 1.0;
	ctx->state->grid_width = width;
	ctx->state->grid_height = height;
	ctx->state->index_from_coords = index_from_coords;

// 	for (uint j = 0; j<height; ++j) {
// 		for (uint i = 0; i<width; ++i) {
// 			if (j%2 == 0)
// 				add_led(ctx->state, (float)i, (float)j);
// 			else
// 				add_led(ctx->state, (float)(width-1-i), (float)j);
// 
// 		}
// 	}
	for (uint col = 0; col<width; ++col) {
		for (uint row = 0; row<height; ++row) {
			if (col%2 == 0)
				add_led(ctx->state, (float)col, (float)row);
			else
				add_led(ctx->state, (float)col, (float)(height-1-row));

		}
	}
}


