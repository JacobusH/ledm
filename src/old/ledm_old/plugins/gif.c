#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <alsa/asoundlib.h>
#include "ledm.h"
#include "util.h"
#include "util/gifdec.h"


#define MAX_FINGERS 10


const char* ledm_version = VERSION;

int width = 0;
int height = 0;



void entry_point(struct context *ctx, char *params) {

	if (ctx->state->index_from_coords == NULL) {
		error("This game requires that grid drawing is supported!");
		return;
	}

	width = ctx->state->grid_width;
	height = ctx->state->grid_height;

	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			paint_at_coords(ctx, x, y, (struct color){0,0,0});

	gd_GIF *gif = gd_open_gif("/home/eristik/muh.gif");
	struct color *buffer = malloc(gif->width * gif->height * 3);
	while(1) {
		while (gd_get_frame(gif)) {
			gd_render_frame(gif, (uint8_t*)buffer);
			for (int x=0; x<width; ++x)
				for (int y=0; y<height; ++y)
					if (x<gif->width && y<gif->height)
						paint_at_coords(ctx, x, y, buffer[x+gif->width*y] );
			io_push(ctx);
			io_poll(ctx, NULL);
			if (ctx->quit)
				break;
			struct timespec ts;
			ts.tv_sec = gif->gce.delay/100;
			ts.tv_nsec = (gif->gce.delay % 100) * 10000000;
			nanosleep(&ts, NULL);
		}
		if (ctx->quit)
			break;
		gd_rewind(gif);
	}
	free(buffer);
	gd_close_gif(gif);
}

