#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <alsa/asoundlib.h>
#include "ledm.h"
#include "util.h"



#define MAX_FINGERS 10


const char* ledm_version = VERSION;

struct finger {
	SDL_FingerID id;
	float x;
	float y;
	float hue;
};

int *lattice = NULL;
int *lattice_next = NULL;

struct finger fingers[MAX_FINGERS];
uint number_fingers = 0;

float dt = 50;
int width;
int height;


void updateFingerPos(struct context *ctx, SDL_FingerID id, float x, float y) {

	uint match = number_fingers;
	for (uint i=0; i<number_fingers; ++i)
		if (fingers[i].id == id)
			match = i;

	if (match == number_fingers) {
		if (number_fingers < MAX_FINGERS) {
			++number_fingers;
			fingers[match].id = id;
			fingers[match].hue = 0.01 * (float)(rand() % 100);
		} else {
			return;
		}
	}

	fingers[match].x = x;
	fingers[match].y = y;

}

void removeFinger(SDL_FingerID id) {

	uint next_index = 0;
	for (uint i=0; i<number_fingers; ++i) {

		// if this particle is dead, move on
		if (fingers[i].id == id)
			continue;

		if (next_index != i)
			fingers[next_index] = fingers[i];

		next_index++;
	}

	number_fingers = next_index;
}



void on_input(struct context *ctx, struct snd_seq_event *midi_event, union SDL_Event *sdl_event) {

	if (sdl_event && sdl_event->type == SDL_FINGERDOWN) {
		updateFingerPos(ctx, sdl_event->tfinger.fingerId, sdl_event->tfinger.x, sdl_event->tfinger.y);
	} else if (sdl_event && sdl_event->type == SDL_FINGERMOTION) {
		updateFingerPos(ctx, sdl_event->tfinger.fingerId, sdl_event->tfinger.x, sdl_event->tfinger.y);
	} else if (sdl_event && sdl_event->type == SDL_FINGERUP) {
		removeFinger(sdl_event->tfinger.fingerId);
	}


}


enum dir {NORTH, EAST, SOUTH, WEST, NORTHEAST, NORTHWEST, SOUTHEAST, SOUTHWEST};
int get_neighbour(int x, int y, enum dir dir) {
	int rx = x;
	int ry = y;
	switch (dir) {
		case NORTH: ry = (height+ry+1)%height; break;
		case EAST:  rx = (width+rx+1)%width;  break;
		case SOUTH: ry = (height+ry-1)%height; break;
		case WEST:  rx = (width+rx-1)%width;  break;
		case NORTHEAST: rx = (width+rx+1)%width; ry = (height+ry+1)%height; break;
		case NORTHWEST: rx = (width+rx-1)%width; ry = (height+ry+1)%height; break;
		case SOUTHEAST: rx = (width+rx+1)%width; ry = (height+ry-1)%height; break;
		case SOUTHWEST: rx = (width+rx-1)%width; ry = (height+ry-1)%height; break;
	}
	return lattice[rx+width*ry];
};

int count_neighbours(int x, int y) {
	return get_neighbour(x,y, NORTH)
	     + get_neighbour(x,y, EAST)
	     + get_neighbour(x,y, SOUTH)
	     + get_neighbour(x,y, WEST)
	     + get_neighbour(x,y, NORTHEAST)
	     + get_neighbour(x,y, NORTHWEST)
	     + get_neighbour(x,y, SOUTHEAST)
	     + get_neighbour(x,y, SOUTHWEST);
};

void on_frame(struct context *ctx) {

	// Spawn life with your fingers
	for(uint i=0; i<number_fingers; ++i) {
		int x = (int)(width+fingers[i].x+0.5) % width;
		int y = (int)(2*height-fingers[i].y-0.5) % height;
		lattice[x+width*y] = 1;
	}

	// Apocalypse if 8 fingers
	if (number_fingers >= 8)
		for (int x=0; x<width; ++x)
			for (int y=0; y<height; ++y)
				lattice[x+width*y] = 0;

	for (int x=0; x<width; ++x) {
		for (int y=0; y<height; ++y) {
			int neighbours = count_neighbours(x,y);

			// Draw current step
			if (lattice[x+width*y]) {
				switch (neighbours) {
					default:
					case 0: paint_at_coords(ctx, x, y,  (struct color){50,0,0}); break;
					case 1: paint_at_coords(ctx, x, y,  (struct color){100,30,0}); break;
					case 2: paint_at_coords(ctx, x, y,  (struct color){150,40,0}); break;
					case 3: paint_at_coords(ctx, x, y,  (struct color){200,70,0}); break;
					case 4: paint_at_coords(ctx, x, y,  (struct color){250,100,0}); break;
					case 5: paint_at_coords(ctx, x, y,  (struct color){250,200,0}); break;
					case 6: paint_at_coords(ctx, x, y,  (struct color){250,200,100}); break;
					case 7: paint_at_coords(ctx, x, y,  (struct color){250,250,250}); break;
					case 8: paint_at_coords(ctx, x, y,  (struct color){0,255,255}); break;
				}
			} else {
				paint_at_coords(ctx, x, y,  (struct color){0,0,0});
			}

			// Calculate next step unless paused, as indicated by fingers down
			if (!number_fingers) {
				if (lattice[x+width*y]) { // If cell lives
					if (neighbours == 2 || neighbours == 3)
						lattice_next[x+width*y] = 1;
					else
						lattice_next[x+width*y] = 0;
				} else { // If cell is dead
					if (neighbours == 3)
						lattice_next[x+width*y] = 1;
					else
						lattice_next[x+width*y] = 0;
				}
			} else {
				lattice_next[x+width*y] = lattice[x+width*y];
			}
		}
	}

	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			lattice[x+width*y] = lattice_next[x+width*y];
}

void entry_point(struct context *ctx, char *params) {

	puts("Hello from the ripple plugin!");


	if (ctx->state->index_from_coords == NULL) {
		error("This game requires that grid drawing is supported!");
		return;
	}

	set_fps(ctx, 7);

	width = ctx->state->grid_width;
	height = ctx->state->grid_height;
	dt = (float)ctx->state->usec_per_frame / 1000000.0;
	lattice = malloc(sizeof(int) * width * height);
	lattice_next = malloc(sizeof(int) * width * height);
	
	
	for (int x=0; x<width; ++x) {
		for (int y=0; y<height; ++y) {
			lattice[x + width*y] = rand() % 2;
		}
	}

	io_loop(ctx, on_input, on_frame);

	free(lattice);
	free(lattice_next);
}

