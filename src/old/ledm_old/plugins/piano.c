#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <alsa/asoundlib.h>
#include "ledm.h"
#include "util.h"

/*
 * adapted from
 * https://www.misc.name/piano-and-light/
 * https://github.com/scanlime/fadecandy/blob/master/examples/node/midi_particles.coffee
 *
 * todo:
 *
 * -have all parameters be controllable via cc events
 * -make this use the circle of fifths
 *
 */


#define MAX_PARTICLES 1800
#define MAX_FINGERS 10
#define KEY_LIFETIME 1000
//#define FINGER_FALLOFF 180.0
#define FINGER_FALLOFF 10.0
#define FINGER_LIFETIME 600
#define KEY_FALLOFF 1.0
#define BRIGHTNESS 5.0
#define SPIN_RATE 0.1
#define NOTE_SUSTAIN 800
#define WOBBLE_AMOUNT 24.0
#define MAX_RADIUS 12
#define HUE_ROTATE 1.0

#define LIMINAL_KEY 1
#define MAX_VELOCITY 100
#define LOWEST_A 1
#define HIGHEST_C 124

const char* ledm_version = VERSION;

struct particle {
	enum {KEY, FINGER} type;
	float x;
	float y;
	uint created_at;

	float intensity;
	float falloff;
	struct color color;

	uint key;
	float life;
};

struct finger {
	SDL_FingerID id;
	float x;
	float y;
	float hue;
};

float note_velocity[128] = {0};
uint note_timestamp[128] = {0};
struct particle particles[MAX_PARTICLES];
uint number_particles = 0;
struct finger fingers[MAX_FINGERS];
uint number_fingers = 0;

float x_center = 0;
float y_center = 0;
float spin_angle = 0;
float dt = 50;



float midiToHz(int nr) {
	return 440.0 * powf( 2, (float) (nr - 69) / 12.0 );
}

float midiToAngle(int nr) {
	return (float)nr * 2.0 * PI / 24.0;
}


void updateParticle(uint i) {

	if (particles[i].type == KEY) {
		// calculate new positions
		float theta = spin_angle + midiToAngle(particles[i].key);
		float radius = MAX_RADIUS * (1.0 - particles[i].life);
		float x = x_center + radius * cosf(theta);
		float y = y_center + radius * sinf(theta);

		// update positions
		particles[i].x = x;
		particles[i].y = y;
	}

	// subtract life
	if (particles[i].type == KEY)
		particles[i].life -= dt / (float)KEY_LIFETIME;
	else if (particles[i].type == FINGER)
		particles[i].life -= dt / (float)FINGER_LIFETIME;
}

void removeCorpses() {

	uint next_index = 0;

	for (uint i=0; i<number_particles; ++i) {

		// if this particle is dead, move on
		if (particles[i].life <= 0 || particles[i].intensity <= 0)
			continue;

		if (next_index != i)
			particles[next_index] = particles[i];

		next_index++;
	}

	number_particles = next_index;
}

void spawnKeyParticle(struct context *ctx, uint i, float intensity_base) {

	if (number_particles < MAX_PARTICLES)
		number_particles++;

	struct particle *new_particle = particles + number_particles - 1;
	new_particle->type = KEY;
	new_particle->x = x_center;
	new_particle->y = y_center;
	new_particle->created_at = ctx->time_ms;
	new_particle->intensity = intensity_base*powf((float) note_velocity[i] / MAX_VELOCITY, 2.0) * 0.05 * BRIGHTNESS;
	new_particle->intensity *= fmaxf(0, 1- (float)(ctx->time_ms-note_timestamp[i])/NOTE_SUSTAIN);
	new_particle->falloff = 0.5*powf(1.18, ((float)i - LIMINAL_KEY)/6.0) * KEY_FALLOFF;
	//int hue = 16 * ( (i - LIMINAL_KEY) % 16);
	float hue = (7.0/12.0) * (float)(i - LIMINAL_KEY) + 0.2 + HUE_ROTATE*0.0001*ctx->time_ms;
	new_particle->color = hsv2rgb_spectrum((int) 255*fmodf(hue, 1.0), 240, 140);
	new_particle->key = i;
	new_particle->life = 1.0;

}

void spawnFingerParticle(struct context *ctx, uint i, float intensity_base) {

	if (number_particles < MAX_PARTICLES)
		number_particles++;

	struct particle *new_particle = particles + number_particles - 1;
	new_particle->type = FINGER;
	new_particle->x = fingers[i].x;
	new_particle->y = fingers[i].y;
	new_particle->created_at = ctx->time_ms;
	new_particle->intensity = intensity_base * 0.05 * BRIGHTNESS;
	new_particle->falloff = 0.5 * FINGER_FALLOFF;
	float hue = fingers[i].hue + HUE_ROTATE*0.0001*ctx->time_ms;
	new_particle->color = hsv2rgb_spectrum((int) 255*fmodf(hue, 1.0), 240, 140);
	new_particle->key = 0;
	new_particle->life = 1.0;

}

void updateFingerPos(struct context *ctx, SDL_FingerID id, float x, float y) {

	uint match = number_fingers;
	for (uint i=0; i<number_fingers; ++i)
		if (fingers[i].id == id)
			match = i;

	if (match == number_fingers) {
		if (number_fingers < MAX_FINGERS) {
			++number_fingers;
			fingers[match].id = id;
			fingers[match].hue = 0.01 * (float)(rand() % 100);
		} else {
			return;
		}
	}

	fingers[match].x = x;
	fingers[match].y = y;

}

void removeFinger(SDL_FingerID id) {

	uint next_index = 0;
	for (uint i=0; i<number_fingers; ++i) {

		// if this particle is dead, move on
		if (fingers[i].id == id)
			continue;

		if (next_index != i)
			fingers[next_index] = fingers[i];

		next_index++;
	}

	number_fingers = next_index;
}



void on_input(struct context *ctx, struct snd_seq_event *midi_event, union SDL_Event *sdl_event) {

	if (midi_event && midi_event->type == SND_SEQ_EVENT_NOTEON && midi_event->data.note.note >= LIMINAL_KEY) {
		note_velocity[midi_event->data.note.note] = (float) midi_event->data.note.velocity;
		note_timestamp[midi_event->data.note.note] = ctx->time_ms;
		//if (midi_event->data.note.note >= LIMINAL_KEY)
		//	spawnParticle(ctx, midi_event->data.note.note, 200);
	} else if (midi_event && midi_event->type == SND_SEQ_EVENT_NOTEOFF) {
		note_velocity[midi_event->data.note.note] = 0;
	}

	if (sdl_event && sdl_event->type == SDL_FINGERDOWN) {
		updateFingerPos(ctx, sdl_event->tfinger.fingerId, sdl_event->tfinger.x, sdl_event->tfinger.y);
	} else if (sdl_event && sdl_event->type == SDL_FINGERMOTION) {
		updateFingerPos(ctx, sdl_event->tfinger.fingerId, sdl_event->tfinger.x, sdl_event->tfinger.y);
	} else if (sdl_event && sdl_event->type == SDL_FINGERUP) {
		removeFinger(sdl_event->tfinger.fingerId);
	}


}

void on_frame(struct context *ctx) {
	struct led *led;

	spin_angle += SPIN_RATE * 2 * PI * dt / 1000.0;

	for (uint i=0; i<number_particles; ++i)
		updateParticle(i);

	removeCorpses();

	for(uint i=0; i<128; ++i)
		if (note_velocity[i]>0 && i>= LIMINAL_KEY)
			spawnKeyParticle(ctx, i, dt);

	for(uint i=0; i<number_fingers; ++i)
		spawnFingerParticle(ctx, i, dt);


	for (uint i=0; i<ctx->state->num_leds; ++i) {
		led = &(ctx->state->leds[i]);

		float r = 0;
		float g = 0;
		float b = 0;

		for (uint j=0; j<number_particles; ++j) {
			float dx = particles[j].x - led->x;
			float dy = particles[j].y - led->y;
			float dist2 = dx * dx + dy * dy;

			float intensity = particles[j].life * particles[j].intensity / (1 + particles[j].falloff * dist2);

			r += particles[j].color.r * intensity;
			g += particles[j].color.g * intensity;
			b += particles[j].color.b * intensity;
		}

		led->c.r = (int) fminf(255, r);
		led->c.g = (int) fminf(255, g);
		led->c.b = (int) fminf(255, b);
	}

}

void entry_point(struct context *ctx, char *params) {

	puts("Hello from the piano plugin!");

	x_center = ctx->state->canvas_width/2.0;
	y_center = ctx->state->canvas_height/2.0;

	dt = (float)ctx->state->usec_per_frame / 1000.0;

	io_loop(ctx, on_input, on_frame);

}

