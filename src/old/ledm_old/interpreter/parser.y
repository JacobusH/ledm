%{

#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>

#include <interpreter.h>
#include <interpreter/maths.h>



static struct context *ctx = NULL;
static char *input = NULL;
static uint at_start_of_line = 1;


int hexDigitToInt(char c);
int yylex();
void yyerror(char const *);

// implemented in core.c:
void cmd_print_var(struct context *ctx, uint *args);

%}


%define api.value.type union
%token UNRECOGNIZED COMMENT
%token <uint> VARIABLE
%token <struct color> LITERAL_COLOR
%token <char*> LITERAL_STRING
%token <uint> LITERAL_UINT
%token <float> LITERAL_FLOAT
%token <char*> WORD
%token END 0 "end of file"


%type <uint> literal
%type <struct arg> arg
%type <struct arg*> arg_list
%type <struct expr> expr;

%destructor { free($$); } WORD
%destructor { if ($$.type == ARG_WORD) free($$.content.word); } arg
%destructor { destroy_arg_list($$); } arg_list
%destructor { destroy_expr($$); } expr

%left '=' '<' '>'
%left '&' '|'
%left '+' '-'
%left '*' '/' '%'
%left '!'
%precedence NEG
%left '^'

%%

input: %empty | input line ;


line:
  '\n' {
	++ctx->line_number;
} | WORD arg_list '\n' {
	parse_command($1, $2, ctx->cmds);
	free($1);
	destroy_arg_list($2);
	++ctx->line_number;
} | VARIABLE '=' expr '\n' {
		add_expr_eval_instr(ctx, $3, $1);
		destroy_expr($3);
	++ctx->line_number;
} | VARIABLE '\n' {
	uint *arg = malloc(sizeof(uint));
	*arg = $1;
	add_instr(ctx, (struct instr){cmd_print_var,arg});
	++ctx->line_number;
} | COMMENT '\n' {
	++ctx->line_number;
} | error '\n' {
	++ctx->line_number;
	yyerrok;
};


arg_list: %empty {
	$$ = NULL;
	}
| arg_list arg {
	$$ = add_to_arg_list($1, $2);
	destroy_arg_list($1);
};


arg:
  WORD {
	$$.type = ARG_WORD;
	$$.content.word = $1;
} | VARIABLE {
	$$.type = ARG_VAR;
	$$.content.var = $1;
} | literal {
	$$.type = ARG_LITERAL;
	$$.content.var = $1;
} | '-' literal {
	$$.type = ARG_LITERAL;
	$$.content.var = $2;
	if (ctx->vars_types[$2] == 'f') {
		ctx->vars[$2].f *= -1;
	} else if (ctx->vars_types[$2] == 'u') {
		ctx->vars[$2].f = -(float)ctx->vars[$2].u;
		ctx->vars_types[$2] = 'f';
	} else {
		YYERROR;
	}
} | '[' expr ']' {
	$$.type = ARG_VAR;
	$$.content.var = new_var(ctx);
	add_expr_eval_instr(ctx, $2, $$.content.var);
	destroy_expr($2);
};


literal:
  LITERAL_COLOR {
	$$ = new_var(ctx);
	ctx->vars_types[$$]='c';
	ctx->vars[$$].c = $1;
} | LITERAL_STRING {
	$$ = new_var(ctx);
	ctx->vars_types[$$]='s';
	ctx->vars[$$].s = $1;
} | LITERAL_UINT {
	$$ = new_var(ctx);
	ctx->vars_types[$$]='u';
	ctx->vars[$$].u = $1;
} | LITERAL_FLOAT {
	$$ = new_var(ctx);
	ctx->vars_types[$$]='f';
	ctx->vars[$$].f = $1;
};



expr:
  literal      { $$ = create_expr_from_var($1); }
| VARIABLE     { $$ = create_expr_from_var($1); }
| expr '+' expr {
	$$ = expr_binary_op(ADDITION, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '-' expr {
	$$ = expr_binary_op(SUBTRACTION, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '*' expr {
	$$ = expr_binary_op(MULTIPLICATION, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '/' expr {
	$$ = expr_binary_op(DIVISION, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '=' expr {
	$$ = expr_binary_op(EQUALITY, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '<' expr {
	$$ = expr_binary_op(LOWER_THAN, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '>' expr {
	$$ = expr_binary_op(GREATER_THAN, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '%' expr {
	$$ = expr_binary_op(MODULUS, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '|' expr {
	$$ = expr_binary_op(LOGICAL_OR, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '&' expr {
	$$ = expr_binary_op(LOGICAL_AND, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | expr '^' expr {
	$$ = expr_binary_op(POWER, $1, $3);
	destroy_expr($1);
	destroy_expr($3);

} | '!' expr {
	$$ = expr_unary_op(LOGICAL_NEGATION, $2);
	destroy_expr($2);

} | '-' expr {
	$$ = expr_unary_op(NEGATION, $2);
	destroy_expr($2);

} | WORD '(' ')' {
	int error = 0;
	if (strcmp($1, "rand") == 0) {
		$$ = expr_nullary_op(RANDOM);
	} else if (strcmp($1, "now") == 0) {
		$$ = expr_nullary_op(TIME);
	} else {
		parser_error(ctx, "Unknown nullary maths function '%s'.", $1);
		error = 1;
	}
	free($1);
	if (error)
		YYERROR;


} | WORD '(' expr ')' {
	int error = 0;
	if (strcmp($1, "sin") == 0) {
		$$ = expr_unary_op(SINE, $3);
	} else if (strcmp($1, "cos") == 0) {
		$$ = expr_unary_op(COSINE, $3);
	} else if (strcmp($1, "exp") == 0) {
		$$ = expr_unary_op(EXPONENTIAL, $3);
	} else if (strcmp($1, "uint") == 0) {
		$$ = expr_unary_op(TO_UINT, $3);
	} else if (strcmp($1, "inv") == 0) {
		$$ = expr_unary_op(INVERT_COLOR, $3);
	} else {
		parser_error(ctx, "Unknown unary maths function '%s'.", $1);
		error = 1;
	}
	free($1);
	destroy_expr($3);
	if (error)
		YYERROR;

} | WORD '(' expr ',' expr ')' {
	int error = 0;
	if (strcmp($1, "max") == 0) {
		$$ = expr_binary_op(MAXIMUM, $3, $5);
	} else if (strcmp($1, "min") == 0) {
		$$ = expr_binary_op(MINIMUM, $3, $5);
	} else {
		parser_error(ctx, "Unknown binary maths function '%s'.", $1);
		error = 1;
	}
	free($1);
	destroy_expr($3);
	destroy_expr($5);
	if (error)
		YYERROR;

} | WORD '(' expr ',' expr ',' expr ')' {
	int error = 0;
	if (strcmp($1, "rgb") == 0) {
		$$ = expr_ternary_op(RGB_TO_COLOR, $3, $5, $7);
	} else if (strcmp($1, "hsv") == 0) {
		$$ = expr_ternary_op(HSV_TO_COLOR, $3, $5, $7);
	} else if (strcmp($1, "ifthenelse") == 0) {
		$$ = expr_ternary_op(TERNARY, $3, $5, $7);
	} else {
		parser_error(ctx, "Unknown ternary maths function '%s'.", $1);
		error = 1;
	}
	free($1);
	destroy_expr($3);
	destroy_expr($5);
	destroy_expr($7);
	if (error)
		YYERROR;

} | WORD {
	int error = 0;
	if (strcmp($1, "pi") == 0) {
		$$ = expr_nullary_op(CONST_PI);
	} else {
		parser_error(ctx, "Unknown maths constant '%s'.", $1);
		error = 1;
	}
	free($1);
	if (error)
		YYERROR;

} | '(' expr ')' {
	$$ = $2;
	}
;




%%



struct arg *add_to_arg_list(struct arg *list, struct arg element) {
	uint length = 0;
	if (list)
		while (list[length].type != ARG_END) ++length;

	struct arg *out = malloc( sizeof(struct arg) * (length+2) );
	for (uint i=0; i<length; ++i) {
		out[i] = list[i];

		// have words have their own memory
		if (list[i].type == ARG_WORD)
			out[i].content.word = strdup(list[i].content.word);
	}
	out[length] = element;
	out[length + 1].type = ARG_END;
	
	return out;
}

void destroy_arg_list(struct arg *list) {
	if (!list)
		return;

	for (uint i=0; list[i].type != ARG_END; ++i)
		if (list[i].type == ARG_WORD)
			free(list[i].content.word);

	free(list);
}

void print_arg_list(struct arg *list) {
	if (!list) {
		printf("NULL\n");
		return;
	}

	for (uint i=0; list[i].type != ARG_END; ++i) {
		if (list[i].type == ARG_WORD)
			printf("ARG_WORD (%s) ", list[i].content.word);
		else if (list[i].type == ARG_VAR)
			printf("ARG_VAR (%u) ", list[i].content.var);
		else if (list[i].type == ARG_LITERAL)
			printf("ARG_LITERAL (%u) ", list[i].content.var);
	}
	printf("ARG_END\n");
}


uint arg_count(struct arg *list) {
	// check if arg list is zero
	if (!list)
		return 0;

	// check if arg list is long enough
	for (uint i=0; ; ++i)
		if (list[i].type == ARG_END)
			return i;
}


int check_arg_var(struct context *ctx, struct arg *list, uint nr, char expected_type) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	// check type of arg
	if (list[nr].type != ARG_VAR && list[nr].type != ARG_LITERAL)
		return 0;
	if (ctx->vars_types[list[nr].content.var] != expected_type)
		return 0;

	return 1;
}


int check_arg_word(struct arg *list, uint nr, const char *expected_word) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	// check type of arg
	if (list[nr].type != ARG_WORD)
		return 0;

	// check whether word is expected
	if (expected_word && strcmp(list[nr].content.word, expected_word) != 0)
		return 0;

	return 1;
}


int check_arg_literal(struct arg *list, uint nr) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	if (list[nr].type == ARG_LITERAL)
		return 1;

	return 0;
}


enum parse_result parse_arg_list(struct context *ctx, struct arg *arg_list, const char *format, uint **args) {

	if (!format)
		return SUCCESS;

	uint expected_nr = 0;
	const char *current = format;
	int return_val;

	while (*current != '\0') {
		switch (*current) {
		case 's':
		case 'c':
		case 'f':
		case 'u':
			++expected_nr;
			++current;
			break;
		case '[':
			do { ++current; } while (*current != ']' && *current != '\0');
			if (*current == '\0') return MALFORMED_FORMAT;
			++current;
			++expected_nr;
			break;
		default:
			return MALFORMED_FORMAT;
		}
	}

	current = format;
	*args = malloc(sizeof(uint) * expected_nr);

	for (uint i=0; i<expected_nr; ++i) {

		if (!arg_list || arg_list[i].type == ARG_END) {
			return_val = WRONG_ARG_COUNT;
			goto cleanup;
		}

		switch (*current) {

		case 'c':
		case 'u':
		case 's':
			if (check_arg_var(ctx, arg_list, i, *current)) {
				(*args)[i] = arg_list[i].content.var;
				++current;
				break;
			} else {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}

		case 'f':
			if (check_arg_var(ctx, arg_list, i, 'f')) {
				(*args)[i] = arg_list[i].content.var;
				++current;
				break;
			} else if (check_arg_var(ctx, arg_list, i, 'u') && check_arg_literal(arg_list, i)) {
				(*args)[i] = arg_list[i].content.var;
				ctx->vars_types[arg_list[i].content.var] = 'f';
				ctx->vars[arg_list[i].content.var].f = (float) ctx->vars[arg_list[i].content.var].u;
				++current;
				break;
			} else {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
		case '[':
			if (arg_list[i].type != ARG_WORD) {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
			
			int wordmatch = 0;
			uint matchnr = 0;
			while (*current != ']') {
				++current;
				int wordlen = 0;
				while (current[wordlen] != '|' && current[wordlen] != ']') ++wordlen;
				
				if (strncmp(current, arg_list[i].content.word, wordlen) == 0 && wordlen == (int)strlen(arg_list[i].content.word)) {
					wordmatch = 1;
					(*args)[i] = matchnr;
					while (*current != ']') ++current;
				} else {
					current += wordlen;
					++matchnr;
				}
			}
			++current;
			if (!wordmatch) {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
		}
	}
	if (arg_list[expected_nr].type == ARG_END)
		return SUCCESS;
	else
		return_val = WRONG_ARG_COUNT;

cleanup:
	free(*args);
	*args = NULL;
	return return_val;
}


void parse_command(const char *cmd, struct arg *arg_list, struct cmd *command_list) {

	int match = -1;
	for (int i = 0; command_list[i].name != NULL; i++)
		if ( strcmp(cmd, command_list[i].name) == 0 )
			match = i;

	if (match < 0) {
		parser_error(ctx, "Could not find (sub)command '%s'!", cmd);

	} else if (command_list[match].subcommands) {
		if (arg_list && arg_list[0].type == ARG_WORD)
			parse_command(arg_list[0].content.word, arg_list+1, command_list[match].subcommands);
		else if (!arg_list || arg_list[0].type == ARG_END)
			parse_command("", arg_list, command_list[match].subcommands);
		else
			parser_error(ctx, "Need a word as subcommand of '%s'!", cmd);

	} else if (command_list[match].cmd) {
		uint *args = NULL;

		switch (parse_arg_list(ctx, arg_list, command_list[match].format, &args)) {
		case WRONG_ARG_COUNT:
			parser_error(ctx, "Bad number of arguments!");
			break;

		case MISMATCHED_ARGS:
			parser_error(ctx, "Type error when evaluating arguments!");
			break;

		case MALFORMED_FORMAT:
			parser_error(ctx, "Command format string is malformed!");
			break;

		case SUCCESS:
			if (command_list[match].parser)
				command_list[match].parser(ctx, arg_list);
			add_instr(ctx, (struct instr){command_list[match].cmd,args});
			break;
		}

	} else if (command_list[match].parser) {
		command_list[match].parser(ctx, arg_list);

	} else {
		parser_error(ctx, "Something went wrong - the command '%s' seems to lack parsing information!\n", cmd);
	}
}


int hexDigitToInt(char c) {

	if (c >= '0' && c <= '9') {
		return (c - '0');
	} else if (c >= 'a' && c <= 'f') {
		return (10 + c - 'a');
	} else if (c >= 'A' && c <= 'F') {
		return (10 + c - 'F');
	} else {
		return -1;
	}
}


int yylex() {

	input += strspn(input, WHITESPACE);

	uint length = 0;

	// discard comments
	if (at_start_of_line && *input == '#') {

		while (input[length] != '\n' && input[length] != '\0') ++length;
		input += length;
		return COMMENT;

	} else {
		at_start_of_line = 0;
	}

	switch (*input) {
	
	case '\n':
	case '\0':
		at_start_of_line = 1;
		return *(input++);
	//TODO deal with EOF?
	case '=': case '+': case '-': case '*': case '/':
	case '&': case '|': case '%': case ',': case '?':
	case ':': case '@': case '<': case '>': case '[':
	case ']': case '(': case ')': case '^': case '!':
		return *(input++);


	case '#':
		++input;
		for (uint i=0; i<6 && input[i]!='\0'; ++i)
			if (hexDigitToInt(input[i]) < 0)
				return UNRECOGNIZED;
		yylval.LITERAL_COLOR.r = 16*hexDigitToInt(input[0]) + hexDigitToInt(input[1]);
		yylval.LITERAL_COLOR.g = 16*hexDigitToInt(input[2]) + hexDigitToInt(input[3]);
		yylval.LITERAL_COLOR.b = 16*hexDigitToInt(input[4]) + hexDigitToInt(input[5]);
		input += 6;
		return LITERAL_COLOR;


	case '"': ;
		++input;
		while (input[length] != '\n' && input[length] != '\0' && input[length] != '"') ++length;

		if (input[length] != '"')
			return UNRECOGNIZED;

		yylval.LITERAL_STRING = strndup(input, length);
		input += length + 1;
		return LITERAL_STRING;


	case '$':
		++input;
		if (!isalpha(*input))
			return UNRECOGNIZED;
		
		while (isdigit(input[length]) || isalpha(input[length]) || input[length]=='_') ++length;

		char *tmp = strndup(input, length);
		yylval.VARIABLE = get_var_index(ctx, tmp);
		free(tmp);
		input += length;
		return VARIABLE;


	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		yylval.LITERAL_UINT = 0;

		while (isdigit(input[length])) ++length;
		uint power_of_ten = 1;
		for (int i=length-1; i>=0; --i) {
			yylval.LITERAL_UINT += power_of_ten * (uint)(input[i] - '0');
			power_of_ten *= 10;
		}
		input += length;

		if (*input != '.')
			return LITERAL_UINT;

		++input;
		yylval.LITERAL_FLOAT = (float) yylval.LITERAL_UINT;
		length = 0;
		while (isdigit(input[length])) ++length;
		for (uint i=0; i<length; ++i)
			yylval.LITERAL_FLOAT += pow(0.1, 1+(float) i) * (float) (input[i] - '0');
		input += length;
		return LITERAL_FLOAT;


	default:
		if (isalpha(*input)) {
			while (isdigit(input[length]) || isalpha(input[length]) || input[length]=='_') ++length;
			yylval.WORD = strndup(input, length);
			input += length;
			return WORD;
		} else {
			++input;
			return UNRECOGNIZED;
		}
	}
}

void yyerror(char const *str) {
	parser_error(ctx, "%s", str);
}


void parse(struct context *ctx_arg, const char *line) {

	if (line == NULL)
		return;

	ctx = ctx_arg;
	input = malloc(sizeof(char) * (strlen(line)+2) );
	char *original_input = input;
	at_start_of_line = 1;

	// append newline if not present
	if (line[strlen(line)-1] == '\n')
		sprintf(input, "%s", line);
	else
		sprintf(input, "%s\n", line);

	yyparse();

	free(original_input);
}

void parse_file(struct context *ctx, const char* filename) {

	FILE *fp;

	// Start counting lines anew
	ctx->line_number = 1;

	// If filename is '-', read from stdin, otherwise from the file with that name. Then, set filenr appropriately
	if (strcmp(filename, "-") == 0) {
		fp = stdin;
		ctx->file_number = 0;
	} else {
		fp = fopen(filename, "r");

		if (fp == NULL) {
			error("Cannot parse '%s': %s", filename, strerror(errno));
			++ctx->errors;
			return;
		}

		++ctx->files_size;
		ctx->files = realloc(ctx->files, ctx->files_size * sizeof(char*));
		ctx->file_number = ctx->files_size;
		ctx->files[ctx->file_number-1] = strdup(filename);
	}

	// Get input and parse it until EOF
	char *line = NULL;
	size_t len = 0;
	while (1) {
		if (getline(&line, &len, fp) == -1)
			break;

		// Parse line and add it to instruction table.
		parse(ctx, line);
	}

	// Cleanup
	ctx->file_number = 0;
	ctx->line_number = 1;
	if (line)
		free(line);
	if (fp != stdin)
		fclose(fp);
}

