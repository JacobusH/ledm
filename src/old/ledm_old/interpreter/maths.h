#ifndef INTERPRETER_MATHS_H
#define INTERPRETER_MATHS_H

#include "interpreter.h"

enum math_operation {
	LOAD_VAR = '.',
	ADDITION = '+',
	SUBTRACTION = '-',
	MULTIPLICATION = '*',
	DIVISION = '/',
	POWER = '^',
	LOWER_THAN = '<',
	GREATER_THAN = '>',
	EQUALITY = '=',
	TERNARY = '?',
	LOGICAL_NEGATION = '!',
	NEGATION = '~',
	MINIMUM = 'm',
	MAXIMUM = 'M',
	COSINE = 'c',
	SINE = 's',
	EXPONENTIAL = 'e',
	TO_UINT = 'u',
	MODULUS = '%',
	RANDOM = 'r',
	CONST_PI = 'p',
	LOGICAL_AND = '&',
	LOGICAL_OR = '|',
	TIME = 'n',
	RGB_TO_COLOR = '#',
	HSV_TO_COLOR = '@',
	INVERT_COLOR = 'I'
};


// TYPES
struct expr {
	char *formula;
	uint *vars;
};

// FUNCTIONS

struct expr create_expr_from_var(uint var);
void destroy_expr(struct expr expr);

uint expr_count_vars(struct expr expr);

struct expr expr_nullary_op(enum math_operation op);
struct expr expr_unary_op(enum math_operation op, struct expr expr1);
struct expr expr_binary_op(enum math_operation op, struct expr expr1, struct expr expr2);
struct expr expr_ternary_op(enum math_operation op, struct expr expr1, struct expr expr2, struct expr expr3);

void add_expr_eval_instr(struct context *ctx, struct expr expr, uint index);


void cmd_maths(struct context *ctx, uint *args);




#endif // INTERPRETER_MATHS_H

