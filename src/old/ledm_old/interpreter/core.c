#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include <ctype.h>

#include "interpreter.h"


struct timeval tick, tock;


void cmd_quit(struct context *ctx, uint *args) {
	ctx->quit = 1;
}


void cmd_jump(struct context *ctx, uint *args) {
	ctx->next = ctx->vars[*args].u;
}


void parse_label(struct context *ctx, struct arg *args) {

	// we want to set labels at parse-time already so that we can jump further down too

	if (!check_arg_var(ctx, args, 0, '%') || arg_count(args) != 1) {
		parser_error(ctx, "Need one uninitialized variable as label.");
		return;
	}

	ctx->vars_types[args[0].content.var] = 'u';
	ctx->vars[args[0].content.var].u = ctx->instrs_used;
}


void cmd_tick(struct context *ctx, uint *args) {
	gettimeofday(&tick, NULL);
}


void cmd_tock(struct context *ctx, uint *args) {
	gettimeofday(&tock, NULL);
	uint elapsed_usec = (((tock.tv_sec - tick.tv_sec) * 1000000) + (tock.tv_usec - tick.tv_usec));
	printf("Time since tick: %d usec\n", elapsed_usec);
}


void cmd_print_all(struct context *ctx, uint *args) {
	printf("  [instruction table]\n"
			"\tinstrs stored/allocated: %zu/%zu\n"
			"\tnext instruction:        %u\n"
			"\terror count:             %u\n"
			"\twarning count:           %u\n",
			ctx->instrs_used, ctx->instrs_size,
			ctx->next, ctx->errors, ctx->warnings);
	printf("\n");


	printf("  [variable table]\n"
			"\tvars stored/allocated:   %zu/%zu\n",
			ctx->vars_used, ctx->vars_size);

	if (ctx->vars_used > 0) {
		printf("\tvariables:\n");
		for (uint i = 0; i < ctx->vars_used; i++) {
			printf("\t%3d:  ", i);
			print_var(ctx, i);
		}
	}
	printf("\n");
}


void cmd_print_var(struct context *ctx, uint *args) {
	print_var(ctx, *args);
}


void cmd_print_string(struct context *ctx, uint *args) {
	printf("%s\n", ctx->vars[*args].s);
}


void cmd_print_vars(struct context *ctx, uint *args) {
	for (uint i = 0; i < ctx->vars_used; i++)
		if (ctx->vars_names[i])
			print_var(ctx, i);
}


struct cmd subcommands_print[] = {
	CMD_SIMPLE("all", cmd_print_all),
	CMD_SIMPLE("", cmd_print_vars),
	CMD_ARGUMENTS("string", cmd_print_string, "l"),
	CMD_ARGUMENTS("var", cmd_print_var, "v"),
	CMD_SIMPLE("vars", cmd_print_vars),
	CMD_LIST_END
};


void parse_load(struct context *ctx, struct arg *args) {

	// we want to allow plugins to introduce new commands to be parsed, so we must do plugin loading at parse time already

	if (!check_arg_var(ctx, args, 0, 's')) {
		parser_error(ctx, "Specify plugin path with a string.");
		return;
	}

	if(arg_count(args) > 2) {
		parser_error(ctx, "Too many arguments.");
		return;
	}

	plugin_entry_point_handle ep = load_plugin(ctx->vars[args[0].content.var].s);
	char *params = NULL;

	if(check_arg_var(ctx, args, 1, 's')) {
		params = ctx->vars[args[1].content.var].s;
	} else if (arg_count(args) == 2) {
		parser_error(ctx, "Specify (optional) plugin params with a string.");
		return;
	}

	if (ep)
		ep(ctx, params);
}


void add_core_cmds(struct context *ctx) {
	add_cmd(ctx, CMD_SIMPLE("quit", cmd_quit));
	add_cmd(ctx, CMD_SIMPLE("exit", cmd_quit));
	add_cmd(ctx, CMD_SIMPLE("tick", cmd_tick));
	add_cmd(ctx, CMD_SIMPLE("tock", cmd_tock));
	add_cmd(ctx, CMD_SIMPLE("q", cmd_quit));
	add_cmd(ctx, CMD_PARSER("label", parse_label));
	add_cmd(ctx, CMD_PARSER("!", parse_label));
	add_cmd(ctx, CMD_PARSER("section", parse_label));
	add_cmd(ctx, CMD_ARGUMENTS("jump", cmd_jump, "u"));
	add_cmd(ctx, CMD_ARGUMENTS("j", cmd_jump, "u"));
	add_cmd(ctx, CMD_SUBCOMMANDS("print", subcommands_print));
	add_cmd(ctx, CMD_SUBCOMMANDS("p", subcommands_print));
	add_cmd(ctx, CMD_PARSER("load", parse_load));
}

