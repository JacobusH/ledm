#define _POSIX_C_SOURCE 200809L

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "interpreter.h"
#include "interpreter/maths.h"
#include "util.h"





struct expr create_expr_from_var(uint var) {
	struct expr expr;

	expr.formula = malloc(2 * sizeof(char));
	expr.formula[0] = '.';
	expr.formula[1] = '\0';
	expr.vars = malloc(sizeof(uint));
	*expr.vars = var;

	return expr;
}


void destroy_expr(struct expr expr) {
	free(expr.formula);
	free(expr.vars);
	expr.formula = NULL;
	expr.vars = NULL;
}


uint expr_count_vars(struct expr expr) {
	if (!expr.formula || !expr.vars)
		return 0;
	uint cnt = 0;
	for (uint i=0; i<strlen(expr.formula); ++i)
		if (expr.formula[i] == '.')
			++cnt;
	return cnt;
}


struct expr expr_nullary_op(enum math_operation op) {
	struct expr expr;

	expr.formula = malloc(2 * sizeof(char));
	expr.formula[0] = op;
	expr.formula[1] = '\0';
	expr.vars = NULL;

	return expr;
}

struct expr expr_unary_op(enum math_operation op, struct expr expr1) {
	struct expr expr;
	uint form_len1 = strlen(expr1.formula);
	uint vars_cnt1 = expr_count_vars(expr1);

	// Allocate space
	expr.formula = malloc(sizeof(char) * (2 + form_len1) );
	expr.vars = malloc(sizeof(uint) * (vars_cnt1) );

	// Copy stuff over
	expr.formula[0] = op;
	memcpy(expr.formula+1, expr1.formula, sizeof(char) * form_len1);
	expr.formula[1+form_len1] = '\0';
	memcpy(expr.vars, expr1.vars, vars_cnt1*sizeof(uint));

	return expr;
}


struct expr expr_binary_op(enum math_operation op, struct expr expr1, struct expr expr2) {
	struct expr expr;
	uint form_len1 = strlen(expr1.formula);
	uint form_len2 = strlen(expr2.formula);
	uint vars_cnt1 = expr_count_vars(expr1);
	uint vars_cnt2 = expr_count_vars(expr2);

	// Allocate space
	expr.formula = malloc(sizeof(char) * (2 + form_len1 + form_len2) );
	expr.vars = malloc(sizeof(uint) * (vars_cnt1 + vars_cnt2) );

	// Copy stuff over
	expr.formula[0] = op;
	memcpy(expr.formula+1, expr1.formula, sizeof(char) * form_len1);
	memcpy(expr.formula+1+form_len1, expr2.formula, sizeof(char) * form_len2);
	expr.formula[1+form_len1+form_len2] = '\0';
	memcpy(expr.vars, expr1.vars, vars_cnt1*sizeof(uint));
	memcpy(expr.vars+vars_cnt1, expr2.vars, vars_cnt2*sizeof(uint));

	return expr;
}


struct expr expr_ternary_op(enum math_operation op, struct expr expr1, struct expr expr2, struct expr expr3) {
	struct expr expr;
	uint form_len1 = strlen(expr1.formula);
	uint form_len2 = strlen(expr2.formula);
	uint form_len3 = strlen(expr3.formula);
	uint vars_cnt1 = expr_count_vars(expr1);
	uint vars_cnt2 = expr_count_vars(expr2);
	uint vars_cnt3 = expr_count_vars(expr3);

	// Allocate space
	expr.formula = malloc(sizeof(char) * (2 + form_len1 + form_len2 + form_len3) );
	expr.vars = malloc(sizeof(uint) * (vars_cnt1 + vars_cnt2 + vars_cnt3) );

	// Copy stuff over
	expr.formula[0] = op;
	memcpy(expr.formula+1, expr1.formula, sizeof(char) * form_len1);
	memcpy(expr.formula+1+form_len1, expr2.formula, sizeof(char) * form_len2);
	memcpy(expr.formula+1+form_len1+form_len2, expr3.formula, sizeof(char) * form_len3);
	expr.formula[1+form_len1+form_len2+form_len3] = '\0';
	memcpy(expr.vars, expr1.vars, vars_cnt1*sizeof(uint));
	memcpy(expr.vars+vars_cnt1, expr2.vars, vars_cnt2*sizeof(uint));
	memcpy(expr.vars+vars_cnt1+vars_cnt2, expr3.vars, vars_cnt3*sizeof(uint));

	return expr;
}


void add_expr_eval_instr(struct context *ctx, struct expr expr, uint out_index) {

	if (strlen(expr.formula) > MAX_EXPR_SIZE) {
		parser_error(ctx, "Math expression too large! If absolutely necessary, increase MAX_EXPR_SIZE and recompile.");
		return;
	}

	uint nr_vars = expr_count_vars(expr);

	uint formula_index = new_var(ctx);
	ctx->vars_types[formula_index] = 's';
	ctx->vars[formula_index].s = strdup(expr.formula);

	uint *args = malloc( (nr_vars+2) * sizeof(uint) );
	args[0] = formula_index;
	memcpy(args+1, expr.vars, nr_vars*sizeof(uint));
	args[nr_vars+1] = out_index;

	if (expr.formula[0] == LOAD_VAR)
		ctx->vars_types[out_index] = ctx->vars_types[expr.vars[0]];
	else if (expr.formula[0] == TO_UINT)
		ctx->vars_types[out_index] = 'u';
	else if (expr.formula[0] == RGB_TO_COLOR || expr.formula[0] == HSV_TO_COLOR || expr.formula[0] == INVERT_COLOR)
		ctx->vars_types[out_index] = 'c';
	else 
		ctx->vars_types[out_index] = 'f';

	add_instr(ctx, (struct instr){cmd_maths, args});
}




void cmd_maths(struct context *ctx, uint *args) {
	char *formula = ctx->vars[args[0]].s;
	// first: index of formula, then variable number of indices of variables that get loaded into stack, then index of variable that the end result gets written to
	uint len = strlen(formula);

	static union var vars[MAX_EXPR_SIZE];
	static char types[MAX_EXPR_SIZE];

	// count number of arguments passed
	uint arg_nr = 0;
	for (uint i=0; i<len; ++i)
		if (formula[i] == '.')
			++arg_nr;
	uint next_arg = arg_nr;

	// main calculation loop: go through formula from right, when hitting on operator, calculate result with next few vars. move everything that comes after consumed vars to the left, including types.
	uint pos = len-1;
	while (1) {
		uint consumed_args = 0;

		switch (formula[pos]) {

		case LOAD_VAR:
			--next_arg;
			vars[pos] = ctx->vars[args[1+next_arg]];
			types[pos] = ctx->vars_types[args[1+next_arg]];
			if (types[pos] == 'u') {
				vars[pos].f = (float)vars[pos].u;
				types[pos] = 'f';
			}
			break;

		case ADDITION:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = vars[pos+1].f + vars[pos+2].f;
			break;

		case SUBTRACTION:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = vars[pos+1].f - vars[pos+2].f;
			break;

		case MULTIPLICATION:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = vars[pos+1].f * vars[pos+2].f;
			break;

		case DIVISION:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = vars[pos+1].f / vars[pos+2].f;
			break;

		case MODULUS:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = fmodf(vars[pos+1].f, vars[pos+2].f);
			break;

		case POWER:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = powf(vars[pos+1].f, vars[pos+2].f);
			break;

		case MAXIMUM:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = (vars[pos+1].f < vars[pos+2].f) ? vars[pos+2].f : vars[pos+1].f;
			break;

		case MINIMUM:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = (vars[pos+1].f > vars[pos+2].f) ? vars[pos+2].f : vars[pos+1].f;
			break;

		case LOWER_THAN:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = (vars[pos+1].f < vars[pos+2].f) ? 1.0 : 0.0;
			break;

		case GREATER_THAN:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = (vars[pos+1].f > vars[pos+2].f) ? 1.0 : 0.0;
			break;

		case LOGICAL_OR:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = (float)( (uint)vars[pos+1].f || (uint)vars[pos+2].f );
			break;

		case LOGICAL_AND:
			consumed_args = 2;
			if (types[pos+1] != 'f' || types[pos+2] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].u = (float)((uint)vars[pos+1].f && (uint)vars[pos+2].f);
			break;

		case EQUALITY:
			consumed_args = 2;
			types[pos] = 'f';
			if (types[pos+1] == types[pos+2]
					&& !memcmp(&vars[pos+1],&vars[pos+2], sizeof(union var))) {
				vars[pos].f = 1.0;
			} else {
				vars[pos].u = 0.0;
			}
			break;

		case RANDOM:
			consumed_args = 0;
			types[pos] = 'f';
			vars[pos].f = (float)rand()/(float)RAND_MAX;
			break;

		case CONST_PI:
			consumed_args = 0;
			types[pos] = 'f';
			vars[pos].f = PI;
			break;

		case TIME:
			consumed_args = 0;
			types[pos] = 'f';
			vars[pos].f = (float)ctx->time_ms;
			break;

		case TO_UINT:
			consumed_args = 1;
			types[pos] = 'u';
			if (types[pos+1] == 'u') {
				vars[pos].u = vars[pos+1].u;
			} else if (types[pos+1] == 'f') {
				vars[pos].u = (uint) (vars[pos+1].f + 0.5);
			} else goto type_error;
			break;

		case SINE:
			consumed_args = 1;
			types[pos] = 'f';
			if (types[pos+1] == 'f') {
				vars[pos].f = sinf( vars[pos+1].f );
			} else goto type_error;
			break;

		case COSINE:
			consumed_args = 1;
			types[pos] = 'f';
			if (types[pos+1] == 'f') {
				vars[pos].f = cosf( vars[pos+1].f );
			} else goto type_error;
			break;

		case EXPONENTIAL:
			consumed_args = 1;
			types[pos] = 'f';
			if (types[pos+1] == 'f') {
				vars[pos].f = expf( vars[pos+1].f );
			} else goto type_error;
			break;

		case TERNARY:
			consumed_args = 3;
			if (types[pos+1] != 'f' || types[pos+2] != types[pos+3])
				goto type_error;
			if (vars[pos+1].f == 0.0) {
				vars[pos] = vars[pos+3];
			} else {
				vars[pos] = vars[pos+2];
			}
			types[pos] = types[pos+2];
			break;

		case LOGICAL_NEGATION:
			consumed_args = 1;
			if (types[pos+1] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = (vars[pos+1].f==0.0)? 1.0 : 0.0;
			break;

		case NEGATION:
			consumed_args = 1;
			if (types[pos+1] != 'f')
				goto type_error;
			types[pos] = 'f';
			vars[pos].f = -vars[pos+1].f;
			break;

		case RGB_TO_COLOR:
			consumed_args = 3;
			if (types[pos+1] != 'f' || types[pos+2] != 'f' || types[pos+3] != 'f')
				goto type_error;
			types[pos] = 'c';
			vars[pos].c.r = (byte)(0.5 + 255*vars[pos+1].f);
			vars[pos].c.g = (byte)(0.5 + 255*vars[pos+2].f);
			vars[pos].c.b = (byte)(0.5 + 255*vars[pos+3].f);
			break;

		case HSV_TO_COLOR:
			consumed_args = 3;
			if (types[pos+1] != 'f' || types[pos+2] != 'f' || types[pos+3] != 'f')
				goto type_error;
			types[pos] = 'c';
			vars[pos].c = hsv2rgb_rainbow((uint)(0.5 + 255*vars[pos+1].f), (uint)(0.5 + 255*vars[pos+2].f), (uint)(0.5 + 255*vars[pos+3].f));
			break;

		case INVERT_COLOR:
			consumed_args = 1;
			if (types[pos+1] != 'c')
				goto type_error;
			types[pos] = 'c';
			vars[pos].c.r = 255-vars[pos+1].c.r;
			vars[pos].c.g = 255-vars[pos+1].c.g;
			vars[pos].c.b = 255-vars[pos+1].c.b;
			break;


		}

		// remove consumed arguments
		if (consumed_args > 0) {
			memmove(vars+pos+1, vars+pos+1+consumed_args, (len-pos-consumed_args-1)*sizeof(union var));
			memmove(types+pos+1, types+pos+1+consumed_args, (len-pos-consumed_args-1)*sizeof(char));
		}

		if (pos == 0)
			break;
		else
			pos--;
	}

	// push value and type into last argument
	ctx->vars[args[arg_nr+1]] = vars[0];
	ctx->vars_types[args[arg_nr+1]] = types[0];
	return;

type_error:
	runtime_error(ctx, "Type mismatch while evaluating maths formula!");
}

