#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>


#include "ledm.h"

void cmd_enable_serial(struct context *ctx, uint *args) {
	if (ctx->state->serial_enabled_set_by_user == 0) {
		ctx->state->serial_enabled = 1;
		free(ctx->state->serial_path);
		ctx->state->serial_path = strdup(ctx->vars[args[0]].s);
		ctx->state->serial_baudrate = ctx->vars[args[1]].u;
	}
}

void cmd_enable_opc(struct context *ctx, uint *args) {
	if (ctx->state->opc_enabled_set_by_user == 0) {
		ctx->state->opc_enabled = 1;
		free(ctx->state->opc_host);
		ctx->state->opc_host = strdup(ctx->vars[args[0]].s);
	}
}

void cmd_enable_sdl(struct context *ctx, uint *args) {
	if (ctx->state->sdl_enabled_set_by_user == 0)
		ctx->state->sdl_enabled = 1;
}

void cmd_enable_midi(struct context *ctx, uint *args) {
	if (ctx->state->midi_enabled_set_by_user == 0)
		ctx->state->midi_enabled = 1;
}

void cmd_io_init(struct context *ctx, uint *args) {
	io_init(ctx);
}

void cmd_io_poll(struct context *ctx, uint *args) {
	io_poll(ctx, NULL);
}

void cmd_io_push(struct context *ctx, uint *args) {
	io_push(ctx);
}

void set_fps(struct context *ctx, float fps) {
	ctx->state->fps = fps;
	ctx->state->usec_per_frame = 1000000/fps;
}

void wait_frame(struct context *ctx) {

	// assumes poll was the last thing to call update_time(), so that ctx->now contains start of frame!

	// Compute how much time has passed
	struct timeval now;
	gettimeofday(&now, NULL);
	int delta_usec = ((now.tv_sec - ctx->now.tv_sec) * 1000000) + (now.tv_usec - ctx->now.tv_usec);
	int usec_left_in_frame = ctx->state->usec_per_frame - delta_usec;

	// If framerate is limited, sleep until the next frame is due to be sent out
	if (ctx->state->fps != 0 && usec_left_in_frame > 0) {
		struct timespec ts;
		ts.tv_sec = usec_left_in_frame / 1000000;
		ts.tv_nsec = (usec_left_in_frame % 1000000) * 1000;
		nanosleep(&ts, NULL);
	}

	// If requested, measure and print framerate
	if (ctx->state->measure_fps) {
		gettimeofday(&now, NULL);
		float fps = 1000000.0/(float)( ((now.tv_sec - ctx->now.tv_sec) * 1000000) + (now.tv_usec - ctx->now.tv_usec) );
		printf("FPS = %f, time to calculate frame = %d usec", fps, delta_usec);
		if (ctx->state->fps != 0)
			printf(", time left = %d usec", usec_left_in_frame);
		printf("\n");
	}
}

void cmd_waitframe(struct context *ctx, uint *args) {
	wait_frame(ctx);
}

void cmd_fps(struct context *ctx, uint *args) {
	set_fps(ctx, ctx->vars[args[0]].f);
}

struct cmd subcommands_io_enable[] = {
	CMD_ARGUMENTS("serial", cmd_enable_serial, "su"),
	CMD_ARGUMENTS("opc", cmd_enable_opc, "s"),
	CMD_SIMPLE("sdl", cmd_enable_sdl),
	CMD_SIMPLE("midi", cmd_enable_midi),
	CMD_LIST_END
};

struct cmd subcommands_io[] = {
	CMD_SIMPLE("init", cmd_io_init),
	CMD_SIMPLE("poll", cmd_io_poll),
	CMD_SIMPLE("push", cmd_io_push),
	CMD_SUBCOMMANDS("enable", subcommands_io_enable),
	CMD_LIST_END
};

void add_io_cmds(struct context *ctx) {
	add_cmd(ctx, CMD_ARGUMENTS("fps", cmd_fps, "f"));
	add_cmd(ctx, CMD_SIMPLE("waitframe", cmd_waitframe));
	add_cmd(ctx, CMD_SUBCOMMANDS("io", subcommands_io));
}

void io_init(struct context *ctx) {
	if (ctx->state->initialized) {
		runtime_warning(ctx, "Tried to initialize twice.");
		return;
	}

	ctx->state->initialized = 1;

	if (ctx->state->midi_enabled)
		midi_init(ctx);
	if (ctx->state->sdl_enabled)
		sdl_init(ctx);
	if (ctx->state->serial_enabled)
		serial_init(ctx);
	if (ctx->state->opc_enabled)
		opc_init(ctx);

}

void io_poll(struct context *ctx, on_input_handle on_input) {

	// update time
	update_time(ctx);

	// poll midi and sdl if enabled
	if (ctx->state->midi_enabled)
		midi_poll(ctx, on_input);
	if (ctx->state->sdl_enabled)
		sdl_poll(ctx, on_input);

}

void io_push(struct context *ctx) {
	if (ctx->state->sdl_enabled)
		sdl_push(ctx);
	if (ctx->state->serial_enabled)
		serial_push(ctx);
	if (ctx->state->opc_enabled)
		opc_push(ctx);
}

void io_loop(struct context *ctx, on_input_handle on_input, on_frame_handle on_frame) {

	while (!ctx->quit) {

		// poll input
		io_poll(ctx, on_input);

		// call frame handler
		if (on_frame)
			on_frame(ctx);

		// sleep until the next frame is due to be sent out
		wait_frame(ctx);

		// push to output
		io_push(ctx);
	}
}

