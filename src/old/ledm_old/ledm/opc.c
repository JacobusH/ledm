#include <stdlib.h>
#include "ledm.h"
#include "util/opc.h"


void opc_init(struct context *ctx) {
	ctx->state->opc_out = opc_new_sink_socket(ctx->state->opc_host);
	ctx->state->opc_buffer = malloc(sizeof(struct color) * ctx->state->num_leds);
}


void opc_push(struct context *ctx) {
	struct state *s = ctx->state;
	for (uint i=0; i < s->num_leds; ++i)
		s->opc_buffer[i] = s->leds[i].c;
	opc_put_pixels(s->opc_out, 0, (u16)s->num_leds, s->opc_buffer);
}
