#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <alsa/asoundlib.h>

#include "ledm.h"

#define INITIAL_LEDS_SIZE 16


struct state *create_state() {
	struct state *state = malloc(sizeof(struct state));

	state->initialized = 0;

	state->load_init = 1;
	state->load_init_set_by_user = 0;
	state->init_file = NULL;

	state->exec_plugin = 0;
	state->plugin_path = NULL;
	state->plugin_params = NULL;

	state->fps = 50;
	state->measure_fps = 0;
	state->usec_per_frame = 1000000/state->fps;

	state->emcee_section_pointer = 0;
	state->emcee_section_count = 0;
	state->emcee_section_var_block = 0;
	state->emcee_current_loop = 0;

	// LED painting state
	state->leds = malloc(INITIAL_LEDS_SIZE*sizeof(struct led));
	state->num_leds = 0;
	state->size_leds = INITIAL_LEDS_SIZE;
	state->painters = NULL;
	state->num_painters = 0;
	state->patterns = NULL;
	state->num_patterns = 0;
	state->canvas_width = 0;
	state->canvas_height = 0;
	state->index_from_coords = NULL;
	state->grid_width = 0;
	state->grid_height = 0;
	state->paint_mode = OVERWRITE;

	// MIDI state
	state->midi_enabled = 0;
	state->midi_enabled_set_by_user = 0;
	state->midi_sequencer = NULL;
	state->midi_port = 0;
	state->midi_channel = -1;
	state->midi_autoconnect = NULL;
	state->midi_refs = NULL;

	// SDL state
	state->sdl_enabled = 0;
	state->sdl_enabled_set_by_user = 0;
	state->sdl_renderer = NULL;
	state->sdl_window = NULL;

	// Serial state
	state->serial_enabled = 0;
	state->serial_enabled_set_by_user = 0;
	state->serial_path = NULL;
	state->serial_baudrate = 0;
	state->serial_socket = 0;

	// OPC state
	state->opc_enabled = 0;
	state->opc_enabled_set_by_user = 0;
	state->opc_host = NULL;
	state->opc_buffer = NULL;


	return state;
}

void destroy_state(struct state *state) {

	if (state->midi_enabled) {
		if (state->midi_sequencer)
			snd_seq_close(state->midi_sequencer);
		state->midi_sequencer = NULL;
	}

	if (state->sdl_enabled) {
		if (state->sdl_renderer)
			SDL_DestroyRenderer(state->sdl_renderer);
		if (state->sdl_window)
			SDL_DestroyWindow(state->sdl_window);
		state->sdl_renderer = NULL;
		state->sdl_window = NULL;
	}

	if (state->serial_enabled) {
		if (state->serial_socket != -1)
			close(state->serial_socket);
		state->serial_socket = -1;
	}

	free(state->leds);
	free(state->painters);
	free(state->patterns);

	free(state->init_file);
	free(state->serial_path);
	free(state->opc_host);
	free(state->opc_buffer);
	free(state->midi_autoconnect);
	free(state->midi_refs);
	free(state->plugin_path);
	free(state->plugin_params);

	free(state);
}

// takes a string like "<prefix><integer>" and returns the integer. on failure, or if the integer is not between 0 and max, returns -1
int getSuffixNr(const char *str, const char *prefix, int max) {

	if (strncmp(str, prefix, strlen(prefix)) != 0)
		return -1;

	char *end;
	long value = strtol(str+strlen(prefix), &end, 10);
	if (end == str+strlen(prefix) || *end != '\0' || errno == ERANGE || value < 0 || value > (long)max)
		return -1;

	return (int)value;
}

void on_new_var(struct context *ctx, uint index, const char *name) {

	int suffix = -1;

	if (ctx->state->midi_enabled) {
		if ( (suffix=getSuffixNr(name, "midi_cc", 127)) >= 0 ) {

			ctx->state->midi_refs[suffix].cc_index = index+1;
			ctx->vars_types[index] = 'f';

		} else if ( (suffix=getSuffixNr(name, "midi_note", 127)) >= 0 ) {

			ctx->state->midi_refs[suffix].note_index = index+1;
			ctx->vars_types[index] = 'f';

		} else if ( (suffix=getSuffixNr(name, "midi_timer", 127)) >= 0 ) {

			ctx->state->midi_refs[suffix].timer_index = index+1;
			ctx->vars_types[index] = 'u';

			// timer logic requires existence of a midi_note variable!
			char notevar[20];
			snprintf(notevar, 20, "midi_note%d", suffix);
			get_var_index(ctx, notevar);
		}
	}


}

