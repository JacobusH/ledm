#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include "interpreter.h"
#include "ledm.h"

void midi_init(struct context *ctx) {

	// Allocate space for MIDI variable refs and initialize
	ctx->state->midi_refs = malloc(128 * sizeof(struct midi_ref));
	for (int i=0; i<128; ++i)
		ctx->state->midi_refs[i] = (struct midi_ref){0,0,0,0};


	// Open ALSA MIDI sequencer
	if (snd_seq_open(&(ctx->state->midi_sequencer), "default", SND_SEQ_OPEN_INPUT, 0) < 0) {
		runtime_error(ctx, "Couldn't open ALSA MIDI sequencer!");
		ctx->state->midi_sequencer = NULL;
		return;
	}

	// Set client name
	snd_seq_set_client_name(ctx->state->midi_sequencer, "ledm");

	// Open ALSA MIDI port
	ctx->state->midi_port = snd_seq_create_simple_port(ctx->state->midi_sequencer, "input",
		SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE, SND_SEQ_PORT_TYPE_APPLICATION);
	if (ctx->state->midi_port < 0) {
		runtime_error(ctx, "Couldn't create ALSA MIDI input port!");
		snd_seq_close(ctx->state->midi_sequencer);
		ctx->state->midi_port = 0;
		ctx->state->midi_sequencer = NULL;
		return;
	}

	// Attempt autoconnect if requested
	if (ctx->state->midi_autoconnect != NULL) {
		snd_seq_addr_t port;
		if (snd_seq_parse_address(ctx->state->midi_sequencer, &port, ctx->state->midi_autoconnect) < 0
				|| snd_seq_connect_from(ctx->state->midi_sequencer, 0, port.client, port.port) < 0 ) {
			warning("Couldn't autoconnect to ALSA MIDI port '%s'!", ctx->state->midi_autoconnect);
		}
	}
}

void midi_poll(struct context *ctx, on_input_handle on_input) {

	uint index;

	// Poll all pending midi events
	snd_seq_event_t* alsa_event;
	while (snd_seq_event_input_pending(ctx->state->midi_sequencer, 1) > 0) {

		snd_seq_event_input(ctx->state->midi_sequencer, &alsa_event);
		switch (alsa_event->type) {
			case SND_SEQ_EVENT_NOTEON:
				// Ignore event if channel doesn't match
				if (ctx->state->midi_channel != -1 && ctx->state->midi_channel != alsa_event->data.note.channel)
					break;

				// update note variable
				index = ctx->state->midi_refs[alsa_event->data.note.note].note_index;
				if (index)
					ctx->vars[index-1].f = ((float)alsa_event->data.note.velocity) / 127.0;

				// reset timer
				ctx->state->midi_refs[alsa_event->data.note.note].last_time = ctx->time_ms;

				break;
			case SND_SEQ_EVENT_NOTEOFF:
				// Ignore event if channel doesn't match
				if (ctx->state->midi_channel != -1 && ctx->state->midi_channel != alsa_event->data.note.channel)
					break;

				// update note variable
				index = ctx->state->midi_refs[alsa_event->data.note.note].note_index;
				if (index)
					ctx->vars[index-1].f = 0;

				break;
			case SND_SEQ_EVENT_CONTROLLER:
				// Ignore event if channel doesn't match
				if (ctx->state->midi_channel != -1 && ctx->state->midi_channel != alsa_event->data.note.channel)
					break;

				// update cc variable
				index = ctx->state->midi_refs[alsa_event->data.control.param].cc_index;
				if (index)
						ctx->vars[index-1].f = ((float)alsa_event->data.control.value) / 127.0;
				break;
		}
		if (on_input)
			on_input(ctx, alsa_event, NULL);
		snd_seq_free_event(alsa_event);
	}


	// Update timers
	for (int i=0; i<128; ++i) {
		struct midi_ref *ref = ctx->state->midi_refs + i;
		if (ref->note_index>0 && ref->timer_index>0 && ctx->vars[ref->note_index-1].f>0)
			ctx->vars[ref->timer_index-1].u = ctx->time_ms - ref->last_time;
	}
}

