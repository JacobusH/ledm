#include <stdlib.h>

#include "ledm.h"
#include "util.h"

struct color painter_solid(struct context *ctx, uint *args, float, float);
struct color painter_valnoise(struct context *ctx, uint *args, float, float);
struct color painter_huenoise(struct context *ctx, uint *args, float, float);


struct color painter_solid(struct context *ctx, uint *args, float x, float y) {

	return ctx->vars[args[0]].c;
}


struct color painter_valnoise(struct context *ctx, uint *args, float x, float y) {
	struct color out;
	//float hue = ctx->vars[args[0]].f;
	//float sat = ctx->vars[args[1]].f;
	float xoff = ctx->vars[args[2]].f;
	float yoff = ctx->vars[args[3]].f;
	float scale = ctx->vars[args[4]].f;
	float time = ctx->vars[args[5]].f;

	float noise = 0.5*(noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );

	out.r = (uint)(255*noise);
	out.g = (uint)(255*noise);
	out.b = (uint)(255*noise);

	return out;
}


struct color painter_huenoise(struct context *ctx, uint *args, float x, float y) {
	float sat = 255*ctx->vars[args[0]].f;
	float val = 255*ctx->vars[args[1]].f;
	float xoff = ctx->vars[args[2]].f;
	float yoff = ctx->vars[args[3]].f;
	float scale = ctx->vars[args[4]].f;
	float time = ctx->vars[args[5]].f;

	// float hue = 255.0*0.5*(noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );
	float hue = 250.0*0.5*(noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );

	return hsv2rgb_rainbow((uint)hue, (uint)sat, (uint) val);
}


