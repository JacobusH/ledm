#include <stdlib.h>
#include <math.h>

#include "ledm.h"
#include "util.h"


float blur(float x, float y, float radius) {

	// 1 if x<<y, 0 if x>>y, linear interpolation between y+-radius

	if (x >= y + radius)
		return 0.0;
	if (x <= y - radius)
		return 1.0;
	return 0.5 - (x-y)/(2*radius);
}

float pattern_all(struct context *ctx, uint *args, float x, float y) {
	return 1;
}

float pattern_nr(struct context *ctx, uint *args, float x, float y) {
	uint nr = ctx->vars[args[0]].u;
	if (nr >= ctx->state->num_leds)
		return 0;

	if (ctx->state->leds[nr].x != x || ctx->state->leds[nr].y != y)
		return 0;

	return 1;
}

float pattern_disc(struct context *ctx, uint *args, float x, float y) {
	float radius = ctx->vars[args[0]].f;
	float blur_radius = ctx->vars[args[1]].f;
	float circle_x = ctx->vars[args[2]].f;
	float circle_y = ctx->vars[args[3]].f;

	return blur( sqrtf( (x-circle_x)*(x-circle_x) + (y-circle_y)*(y-circle_y)),  radius, blur_radius );
}

float pattern_ring_section(struct context *ctx, uint *args, float x, float y) {

	// TODO This is a work in progress!

	float radius_min = ctx->vars[args[0]].f;
	float radius_max = ctx->vars[args[1]].f;
	float radius_blur = ctx->vars[args[2]].f;
	float phi_offset = 2*PI*ctx->vars[args[3]].f;
	float phi_halfwidth = PI*ctx->vars[args[4]].f;
	float phi_blur = PI*ctx->vars[args[5]].f;
	float circle_x = ctx->vars[args[6]].f;
	float circle_y = ctx->vars[args[7]].f;

	float distance = sqrtf( (x-circle_x)*(x-circle_x) + (y-circle_y)*(y-circle_y) );
	float phi = fmod( fabs( atan2(x - circle_x, y - circle_y) + phi_offset), 2*PI);

	return blur(radius_min, distance, radius_blur)
		*  blur(distance, radius_max, radius_blur)
		*  blur(phi, phi_halfwidth, phi_blur);
}

float pattern_rectangle(struct context *ctx, uint *args, float x, float y) {
	float xmin = ctx->vars[args[0]].f;
	float xmax = ctx->vars[args[1]].f;
	float ymin = ctx->vars[args[2]].f;
	float ymax = ctx->vars[args[3]].f;
	float rotation = ctx->vars[args[4]].f;

	float x0 = (xmax + xmin) / 2.0;
	float y0 = (ymax + ymin) / 2.0;
	float r_x = x - x0;
	float r_y = y - y0;

	x = r_x*cosf(rotation) + r_y * sinf(rotation) + x0;
	y = -r_x*sinf(rotation) + r_y * cosf(rotation) + y0;

	return (float) ( x >= xmin && x <= xmax && y >= ymin && y <= ymax );
}


float pattern_lattice(struct context *ctx, uint *args, float x, float y) {
	float xmin = ctx->vars[args[0]].f;
	float xmax = ctx->vars[args[1]].f;
	float ymin = ctx->vars[args[2]].f;
	float ymax = ctx->vars[args[3]].f;
	float stride = ctx->vars[args[4]].f;
	float point_size = ctx->vars[args[5]].f;

	return (float) ( x >= xmin && x <= xmax && fabs(fmod(x-xmin, stride)) <= point_size &&
		y >= ymin && y <= ymax && fabs(fmod(y-ymin, stride)) <= point_size);
}

float pattern_noise(struct context *ctx, uint *args, float x, float y) {
	float cutoff = ctx->vars[args[0]].f;
	float xoff = ctx->vars[args[1]].f;
	float yoff = ctx->vars[args[2]].f;
	float scale = ctx->vars[args[3]].f;
	float time = ctx->vars[args[4]].f;

	float noise = 0.5*(noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );

	return (float)(noise > cutoff);
}



