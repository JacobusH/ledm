#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <time.h>
#include <SDL2/SDL.h>

#include "ledm.h"


void sdl_init(struct context *ctx) {

	if (SDL_Init(SDL_INIT_VIDEO ) < 0)
		runtime_error(ctx, "Couldn't initialize SDL! SDL_Error: %s", SDL_GetError());

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");

	ctx->state->sdl_window = SDL_CreateWindow("ledm " VERSION, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 300, 300, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (ctx->state->sdl_window == NULL) {
		runtime_error(ctx, "Couldn't create SDL window! SDL_Error: %s", SDL_GetError());
		return;
	}

	ctx->state->sdl_renderer = SDL_CreateRenderer(ctx->state->sdl_window, -1, SDL_RENDERER_ACCELERATED);
	if (ctx->state->sdl_renderer == NULL) {
		runtime_error(ctx, "Couldn't create SDL renderer! SDL Error: %s\n", SDL_GetError());
		return;
	}

	SDL_RenderPresent(ctx->state->sdl_renderer);
}

void sdl_poll(struct context *ctx, on_input_handle on_input) {

	// Calculate offsets etc in order to change event coordinates to our coordinate system
	int window_width, window_height;
	SDL_GetWindowSize(ctx->state->sdl_window, &window_width, &window_height);
	int margin = 50;
	float scale = 1;
	float scale_w = (float)(window_width -2*margin) / (float)ctx->state->canvas_width;
	float scale_h = (float)(window_height-2*margin) / (float)ctx->state->canvas_height;
	int offset_x = margin, offset_y = margin;
	if (scale_w < scale_h) {
		scale = scale_w;
		offset_y = (int) (window_height - scale*(float)ctx->state->canvas_height )/2;
	} else {
		scale = scale_h;
		offset_x = (int) (window_width - scale*(float)ctx->state->canvas_width )/2;
	}


	SDL_Event sdl_event;
	while (SDL_PollEvent(&sdl_event) != 0) {
		if (sdl_event.type == SDL_QUIT || (sdl_event.type == SDL_KEYDOWN && sdl_event.key.keysym.sym == SDLK_q)) {
			ctx->quit = 1;
		//else if (sdl_event.type == SDL_WINDOWEVENT && sdl_event.window.event == SDL_WINDOWEVENT_RESIZED)
		} else if (sdl_event.type == SDL_FINGERUP || sdl_event.type == SDL_FINGERDOWN || sdl_event.type == SDL_FINGERMOTION) {
			sdl_event.tfinger.x = (window_width*sdl_event.tfinger.x - offset_x)/scale;
			sdl_event.tfinger.y = (-window_height*sdl_event.tfinger.y - offset_y + window_height)/scale;
		}

		if (on_input)
			on_input(ctx, NULL, &sdl_event);
	}
}

void sdl_push(struct context *ctx) {

	// Clear renderer to be sure
	SDL_RenderClear(ctx->state->sdl_renderer);

	// Get window dimensions and calculate helper variables
	int window_width, window_height;
	SDL_GetWindowSize(ctx->state->sdl_window, &window_width, &window_height);

	int margin = 50;
	float scale = 1;
	float scale_w = (float)(window_width -2*margin) / (float)ctx->state->canvas_width;
	float scale_h = (float)(window_height-2*margin) / (float)ctx->state->canvas_height;
	int offset_x = margin, offset_y = margin;
	if (scale_w < scale_h) {
		scale = scale_w;
		offset_y = (int) (window_height - scale*(float)ctx->state->canvas_height )/2;
	} else {
		scale = scale_h;
		offset_x = (int) (window_width - scale*(float)ctx->state->canvas_width )/2;
	}

	int led_size = 12;
	//if (led_size < 2) led_size = 2;

	// Draw background
	SDL_SetRenderDrawColor(ctx->state->sdl_renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(ctx->state->sdl_renderer, NULL);

	// Draw pixel grid
	SDL_Rect r;
	r.w = led_size;
	r.h = led_size;
	for (uint i = 0; i<ctx->state->num_leds; ++i) {
		r.x = offset_x - led_size/2 + scale*ctx->state->leds[i].x;
		r.y = - offset_y - led_size/2 + window_height-scale*ctx->state->leds[i].y;
		SDL_SetRenderDrawColor(ctx->state->sdl_renderer,
				ctx->state->leds[i].c.r,
				ctx->state->leds[i].c.g,
				ctx->state->leds[i].c.b,
				255);
		SDL_RenderFillRect(ctx->state->sdl_renderer, &r);
	}

	// Render!
	SDL_RenderPresent(ctx->state->sdl_renderer);
}

