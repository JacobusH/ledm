#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "ledm.h"


// implemented in painters.h
struct color painter_solid(struct context *ctx, uint *args, float, float);
struct color painter_valnoise(struct context *ctx, uint *args, float, float);
struct color painter_huenoise(struct context *ctx, uint *args, float, float);

void add_standard_painters(struct context *ctx) {
	add_painter(ctx, "solid", "c", painter_solid);
	add_painter(ctx, "valnoise", "ffffff", painter_valnoise);
	add_painter(ctx, "huenoise", "ffffff", painter_huenoise);
}


// implemented in patterns.h
float pattern_all(struct context *ctx, uint *args, float x, float y);
float pattern_nr(struct context *ctx, uint *args, float x, float y);
float pattern_disc(struct context *ctx, uint *args, float x, float y);
float pattern_ring_section(struct context *ctx, uint *args, float x, float y);
float pattern_rectangle(struct context *ctx, uint *args, float x, float y);
float pattern_lattice(struct context *ctx, uint *args, float x, float y);
float pattern_noise(struct context *ctx, uint *args, float x, float y);

void add_standard_patterns(struct context *ctx) {
	add_pattern(ctx, "all", "", pattern_all);
	add_pattern(ctx, "nr", "u", pattern_nr);
	add_pattern(ctx, "disc", "ffff", pattern_disc);
	add_pattern(ctx, "ring-section", "ffffffff", pattern_ring_section);
	add_pattern(ctx, "rectangle", "fffff", pattern_rectangle);
	add_pattern(ctx, "lattice", "ffffff", pattern_lattice);
	add_pattern(ctx, "noise", "fffff", pattern_noise);
}



void add_led(struct state *state, float x, float y) {

	if (state->num_leds == state->size_leds) {
		state->size_leds *=2;
		state->leds = realloc(state->leds, state->size_leds*sizeof(struct led));
	}
	state->leds[state->num_leds].c.r = 0;
	state->leds[state->num_leds].c.g = 0;
	state->leds[state->num_leds].c.b = 100;
	state->leds[state->num_leds].x = x;
	state->leds[state->num_leds].y = y;

	++state->num_leds;
}


void add_painter(struct context *ctx, const char *name, const char *format, struct color (*func)(struct context*, uint*, float, float)) {

	++ctx->state->num_painters;
	ctx->state->painters = realloc(ctx->state->painters, ctx->state->num_painters*sizeof(struct painter));
	ctx->state->painters[ctx->state->num_painters-1].name = name;
	ctx->state->painters[ctx->state->num_painters-1].format = format;
	ctx->state->painters[ctx->state->num_painters-1].func = func;
}


void add_pattern(struct context *ctx, const char *name, const char *format, float (*func)(struct context*, uint*, float, float)) {

	++ctx->state->num_patterns;
	ctx->state->patterns = realloc(ctx->state->patterns, ctx->state->num_patterns*sizeof(struct pattern));
	ctx->state->patterns[ctx->state->num_patterns-1].name = name;
	ctx->state->patterns[ctx->state->num_patterns-1].format = format;
	ctx->state->patterns[ctx->state->num_patterns-1].func = func;
}


void paint_at_coords(struct context *ctx, uint col, uint row, struct color color) {
	if (!ctx->state->index_from_coords)
		error("Tried to use grid drawing, but this is not supported by canvas.");
	else
		ctx->state->leds[ctx->state->index_from_coords(col,row)].c = color;
}


void cmd_paint(struct context *ctx, uint *args) {
	struct pattern *pat = &(ctx->state->patterns[args[0]]);
	uint num_pattern_arg = (uint)strlen(pat->format);
	struct painter *paint = &(ctx->state->painters[args[1]]);
	struct led *l;
	struct color col;

	for (uint i=0; i<ctx->state->num_leds; ++i) {
		l = &(ctx->state->leds[i]);

		float opacity = pat->func(ctx, args+2, l->x, l->y);
		if (opacity > 0.05) {
			col = paint->func(ctx, args+2+num_pattern_arg, l->x, l->y);

			// blend background and col
			switch (ctx->state->paint_mode) {
				case OVERWRITE:
					l->c.r = (byte) ( (float)l->c.r * (1-opacity) + (float)col.r * opacity );
					l->c.g = (byte) ( (float)l->c.g * (1-opacity) + (float)col.g * opacity );
					l->c.b = (byte) ( (float)l->c.b * (1-opacity) + (float)col.b * opacity );
					break;
				case MULTIPLY: // TODO
					l->c.r = (byte) ( ((int)l->c.r * (int)col.r)/255 );
					l->c.g = (byte) ( ((int)l->c.g * (int)col.g)/255 );
					l->c.b = (byte) ( ((int)l->c.b * (int)col.b)/255 );
					break;
				case SCREEN:
					l->c.r = (byte) (  (float)l->c.r +  opacity * (float)col.r * (1 - (float)l->c.r/255.0)  );
					l->c.g = (byte) (  (float)l->c.g +  opacity * (float)col.g * (1 - (float)l->c.g/255.0)  );
					l->c.b = (byte) (  (float)l->c.b +  opacity * (float)col.b * (1 - (float)l->c.b/255.0)  );
					break;
				case ON_BLACK:
					if (l->c.r == 0 && l->c.g == 0 && l->c.b == 0)
						l->c = col;
					break;
			}
		}
	}
}


void parser_paint(struct context *ctx, struct arg *arg_list) {

	// find separator between pattern and painter parts
	int separator_pos = -1;
	for (uint i=1; arg_list[i].type != ARG_END; ++i)
		if (check_arg_word(arg_list, i, "with"))
			separator_pos = i;
	if (separator_pos < 0) {
		parser_error(ctx, "Use the word 'with' to separate pattern and painter.");
		return;
	}

	// find pattern
	if (!check_arg_word(arg_list, 0, NULL)) {
		parser_error(ctx, "Please specify pattern.");
		return;
	}
	int pattern = -1;
	for (int i = 0; i<(int)ctx->state->num_patterns; i++)
		if (strcmp(arg_list[0].content.word, ctx->state->patterns[i].name) == 0)
			pattern = i;
	if (pattern < 0) {
		parser_error(ctx, "Unknown pattern.");
		return;
	}

	// find painter
	if (!check_arg_word(arg_list, separator_pos+1, NULL)) {
		parser_error(ctx, "Please specify painter.");
		return;
	}
	int painter = -1;
	for (int i = 0; i<(int)ctx->state->num_painters; i++)
		if (strcmp(arg_list[separator_pos+1].content.word, ctx->state->painters[i].name) == 0)
			painter = i;
	if (painter < 0) {
		parser_error(ctx, "Unknown painter.");
		return;
	}

	// create separate pattern and painter arg lists
	uint num_pattern_arg = separator_pos - 1;
	uint num_painter_arg = arg_count(arg_list) - separator_pos - 2;
	arg_list[separator_pos].type = ARG_END;
	struct arg *pattern_arg_list = arg_list + 1;
	struct arg *painter_arg_list = arg_list + separator_pos + 2;

	// Define args pointers
	uint *painter_args = NULL;
	uint *pattern_args = NULL;

	if ( parse_arg_list(ctx, pattern_arg_list, ctx->state->patterns[pattern].format, &pattern_args) != SUCCESS ) {
		parser_error(ctx, "Could not parse pattern arguments.");
		goto cleanup;
	}
	
	if ( parse_arg_list(ctx, painter_arg_list, ctx->state->painters[painter].format, &painter_args) != SUCCESS ) {
		parser_error(ctx, "Could not parse painter arguments.");
		goto cleanup;
	}

	// undo splitting of arg list so that it will get freed properly
	arg_list[separator_pos].type = ARG_WORD;
	

	// Build argument pointer
	uint *args = malloc(sizeof(uint) * (2+num_pattern_arg+num_painter_arg) );
	args[0] = pattern;
	args[1] = painter;
	for (uint i=0; i<num_pattern_arg; ++i)
		args[2+i] = pattern_args[i];
	for (uint i=0; i<num_painter_arg; ++i)
		args[2+num_pattern_arg+i] = painter_args[i];

	// Add draw instruction
	add_instr(ctx, (struct instr){cmd_paint, args});

cleanup:
	free(painter_args);
	free(pattern_args);
}


void cmd_paint_mode(struct context *ctx, uint *args) {
	switch (*args) {
		default:
		case 0: ctx->state->paint_mode = OVERWRITE; break;
		case 1: ctx->state->paint_mode = MULTIPLY;  break;
		case 2: ctx->state->paint_mode = SCREEN;    break;
		case 3: ctx->state->paint_mode = ON_BLACK;  break;
	}
}


void add_paint_cmds(struct context *ctx) {
	add_cmd(ctx, CMD_PARSER("paint", parser_paint));
	add_cmd(ctx, CMD_ARGUMENTS("paintmode", cmd_paint_mode, "[overwrite|multiply|screen|on_black]"));
}
