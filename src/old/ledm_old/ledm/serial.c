#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <string.h>
#include <sys/ioctl.h>

#include "ledm.h"

#define CRTSCTS  020000000000

void serial_init(struct context *ctx) {

	struct termios toptions;

	//serial->fd = open(port, O_WRONLY | O_NOCTTY | O_NDELAY );  O_RDWR
	ctx->state->serial_socket = open(ctx->state->serial_path, O_WRONLY | O_NOCTTY );

	if (ctx->state->serial_socket == -1) {
		runtime_error(ctx, "Unable to open serial tty: %s", strerror(errno));
		return;
	}
	if (tcgetattr(ctx->state->serial_socket, &toptions) < 0) {
		runtime_error(ctx, "Couldn't get tty attributes");
		return;
	}

	speed_t brate = ctx->state->serial_baudrate; // let you override switch below if needed
	switch(ctx->state->serial_baudrate) {
	case 4800:   brate=B4800;   break;
	case 9600:   brate=B9600;   break;
	#ifdef B14400
	case 14400:  brate=B14400;  break;
	#endif
	case 19200:  brate=B19200;  break;
	#ifdef B28800
	case 28800:  brate=B28800;  break;
	#endif
	case 38400:  brate=B38400;  break;
	case 57600:  brate=B57600;  break;
	case 115200: brate=B115200; break;
	#ifdef B921600
	case 921600: brate=B921600; break;
	#endif
	#ifdef B2000000
	case 2000000: brate=B2000000; break;
	#endif
	#ifdef B2500000
	case 2500000: brate=B2500000; break;
	#endif
	#ifdef B3000000
	case 3000000: brate=B3000000; break;
	#endif
	#ifdef B3500000
	case 3500000: brate=B3500000; break;
	#endif
	#ifdef B4000000
	case 4000000: brate=B4000000; break;
	#endif
	//default: TODO: error message, then exit
	}
	cfsetispeed(&toptions, brate);
	cfsetospeed(&toptions, brate);

	// 8N1
	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	// no flow control
	toptions.c_cflag &= ~CRTSCTS;

	//toptions.c_cflag &= ~HUPCL; // disable hang-up-on-close to avoid reset

	toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
	toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

	toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
	toptions.c_oflag &= ~OPOST; // make raw

	// see: http://unixwiz.net/techtips/termios-vmin-vtime.html
	toptions.c_cc[VMIN]  = 0;
	toptions.c_cc[VTIME] = 0;
	//toptions.c_cc[VTIME] = 20;

	tcsetattr(ctx->state->serial_socket, TCSANOW, &toptions);
	if(tcsetattr(ctx->state->serial_socket, TCSAFLUSH, &toptions) < 0) {
		runtime_error(ctx, "Couldn't set tty attributes");
		return;
	}
}

/*
void cmd_serial_write(struct context *ctx, uint *args) {
	if (!ctx->state->serial_socket) {
		runtime_error(ctx, "Serial not initialized.");
		return;
	}
	int n = write(ctx->state->serial_socket, &ctx->vars[*args].b, 1);
	if (n != 1)
		runtime_error(ctx, "Failed to write byte: %s", strerror(errno));
}*/

void serial_push(struct context *ctx) {
	uint16_t num_LEDs = (uint16_t) ctx->state->num_leds;
	if (!ctx->state->serial_socket) {
		runtime_error(ctx, "Serial line not initialized.");
		return;
	}
	const byte header[4] = { 0xDE, 0xAD, 0xBE, 0xEF };
	uint n = write(ctx->state->serial_socket, header, sizeof(header));
	n += write(ctx->state->serial_socket, &num_LEDs, 2);
	if (n != sizeof(header)+2)
		runtime_error(ctx, "Failed to write header: %s", strerror(errno));
	uint LEDs_written = 0;
	byte buffer[3];
	while (LEDs_written < ctx->state->num_leds) {
		buffer[0] = ctx->state->leds[LEDs_written].c.r;
		buffer[1] = ctx->state->leds[LEDs_written].c.g;
		buffer[2] = ctx->state->leds[LEDs_written].c.b;

		n = write(ctx->state->serial_socket, buffer, 3);//paint_data.num_LEDs*3);
		if (n != 3) {
			runtime_error(ctx, "Failed to write data: %s", strerror(errno));
			return;
		} else {
			LEDs_written++;
		}
		if (LEDs_written % 3 == 0) {
			nanosleep((const struct timespec[]){{0, 1000}}, NULL);
		}
	}
}
