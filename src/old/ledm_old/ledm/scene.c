#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "ledm.h"

void cmd_scene_control(struct context *ctx, uint *args) {

	// set section pointer from argument
	if (ctx->vars_types[*args] != '%' && ctx->vars_types[*args] != 'u')
		parser_warning(ctx, "Cast variable type of scene control pointer to 'u'.");
	ctx->vars_types[*args] = 'u';
	ctx->state->scene_section_pointer = *args;
	ctx->state->scene_current_loop = 0;
}

/*void parser_scene_section_list(struct context *ctx, struct arg *arg_list) {

	// let 'end' point at the last non-whitespace character
	char *end = str + strlen(str) - 1;
	while (end >= str && isspace((unsigned char)*end)) end--;

	// parameter_start is supposed to always point at start of currently parsed parameter
	char *parameter_start = str;

	// go through all parameters we expect based on format
	while (1) {

		// calculate position of first non-whitespace character
		parameter_start = parameter_start + strspn(parameter_start, WHITESPACE);

		// if no word is left, we have not been supplied with enough parameters
		if (parameter_start > end || strlen(parameter_start) == 0)
			break;

		// set parameter_end to next whitespace or '\0' that appears
		char *parameter_end = parameter_start;
		while (!isspace(*parameter_end) && *parameter_end != '\0') parameter_end++;

		*parameter_end = '\0';

		// parse parameter_start (which ranges to parameter_end now) based on expected type and write result into *out
		uint var_index = new_var(ctx);
		// set name
		asprintf(&(ctx->vars_names[var_index]), "scene_%s", parameter_start);
		ctx->vars_types[var_index] = 'u';

		if (ctx->state->scene_section_count == 0)
			ctx->state->scene_section_var_block = var_index;
		ctx->state->scene_section_count++;

		// set pointer to beginning of next parameter
		parameter_start = parameter_end+1;
	}

}*/

void parser_scene_section(struct context *ctx, struct arg *arg_list) {

	// we want to set section labels at parse-time already so that we can jump to instructions later in a file, too

	// remove trailing whitespace
	char *end = str + strlen(str) - 1;
	while (end >= str && isspace((unsigned char)*end)) end--;
	end[1] = '\0';

	// calculate position of first non-whitespace character
	size_t start = strspn(str, WHITESPACE);

	char *name = NULL;
	asprintf(&name, "scene_%s", str+start);
	uint index = get_var_index(ctx, name);
	ctx->vars[index].u = ctx->instrs_used;
	free(name);
}

void cmd_scene_proceed(struct context *ctx, uint *args) {

	// Sleep until the next frame is due to be sent out
	wait_frame(ctx);

	// call output commands
	io_push(ctx);

	// poll commands that require it
	io_poll(ctx, NULL);

	// loop or jump to next section based on section pointer (if its value lies inside the section array)
	if (ctx->vars[ctx->state->scene_section_pointer].u == 0) {
		if (ctx->state->scene_current_loop)
			ctx->next = ctx->state->scene_current_loop-1;
	} else if (ctx->vars[ctx->state->scene_section_pointer].u <= ctx->state->scene_section_count) {
		ctx->next = ctx->vars[ ctx->state->scene_section_var_block + ctx->vars[ctx->state->scene_section_pointer].u - 1 ].u;
	}
}

void cmd_scene_loop(struct context *ctx, uint *args) {
	ctx->state->scene_current_loop = 1 + ctx->next;
	ctx->vars[ctx->state->scene_section_pointer].u = 0;
}

void parser_scene_wait(struct context *ctx, struct arg *arg_list) {
	add_instr(ctx, (struct instr){cmd_scene_loop, NULL});
	add_instr(ctx, (struct instr){cmd_scene_proceed, NULL});
}

struct cmd subcommands_scene[] = {
	CMD_ARGUMENTS("selector", cmd_scene_selector, "u"),
	CMD_SIMPLE("continue", cmd_scene_continue),
	CMD_SIMPLE("loop", cmd_scene_loop),
	CMD_SIMPLE("wait", cmd_scene_wait),
	CMD_ARGUMENTS("", cmd_scene_control, "u"),
	CMD_LIST_END

	CMD_ARGUMENTS("control", cmd_scene_control, "v"),
//S	CMD_PARSER("section-list", parser_scene_section_list),
	CMD_PARSER("section", parser_scene_section),
	CMD_PARSER("wait", parser_scene_wait),
	CMD_SIMPLE("proceed", cmd_scene_proceed),
	CMD_SIMPLE("loop", cmd_scene_loop),
	CMD_LIST_END
};

void add_scene_cmds(struct context *ctx) {
	add_cmd(ctx, CMD_SUBCOMMANDS("scene", subcommands_scene));
}

