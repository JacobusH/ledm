#define _XOPEN_SOURCE 700
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <time.h>
#include <fcntl.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "ledm.h"



void handler(int sig) {
	void *array[20];
	size_t size;

	// Get backtrace
	size = backtrace(array, 10);

	// Print to stderr
	fprintf(stderr, "Caught a Segmentation fault! Backtrace:\n\n");
	backtrace_symbols_fd(array, size, STDERR_FILENO);
	exit(1);
}

int main(int argc, char **argv) {
	int return_value = 0;

	// Register signal handler
	signal(SIGSEGV, handler);

	// Locate configuration folder
	char *config_path = NULL;
	char *config_home = getenv("XDG_CONFIG_HOME");
	if (config_home != NULL) {
		asprintf(&config_path, "%s/%s/", config_home, "ledm");
	} else {
config_home = getenv("HOME");
		if (config_home != NULL) {
			asprintf(&config_path, "%s/%s/%s/", config_home, ".config", "ledm");
        } else {
			warning("Cannot find configuration folder since both HOME and XDG_CONFIG_HOME are not set.");
		}
	}

	// Create configuration folder if necessary
	if (config_path != NULL)
		mkdir(config_path, 0777);

	// Seed random number generation
	srand((uint)time(NULL));

	// Create interpreter context
	struct context *ctx = create_context();

	// Add commands to context
	add_core_cmds(ctx);
	add_paint_cmds(ctx);
	add_standard_painters(ctx);
	add_standard_patterns(ctx);
	add_io_cmds(ctx);
	//add_emcee_cmds(ctx);

	// Handle command-line arguments
	static struct option long_options[] = {
		{"serial", required_argument, NULL, 's'},
		{"no-serial", no_argument, NULL, 'S'},
		{"opc", required_argument, NULL, 'o'},
		{"no-opc", no_argument, NULL, 'O'},
		{"sdl", no_argument, NULL, 'g'},
		{"no-sdl", no_argument, NULL, 'G'},
		{"midi", optional_argument, NULL, 'm'},
		{"no-midi", no_argument, NULL, 'M'},
		{"init", required_argument, NULL, 'i'},
		{"no-init", no_argument, NULL, 'I'},
		{"exec", required_argument, NULL, 'x'},
		{"fps", required_argument, NULL, 'f'},
		{"measure-fps", no_argument, NULL, 'F'},
		{"help", no_argument, NULL, 'h'},
		{"version", no_argument, NULL, 'v'},
		{0, 0, 0, 0}
	};

	const char* usage =
		"Usage: ledm [options] [files]\n"
		"Run ledm scripts. Starts interactive session if no file is specified, otherwise parses files in the order they are given and then executes. If a file is set to -, parses from stdin instead until encountering an EOF. \n"
		"\n"
		"Options:\n"
		"  -i, --init <file>        Load <file> instead of default init file.\n"
		"  -I, --no-init            Don't load init file.\n"
		"  -s, --serial <tty>@<br>  Enable serial output to <tty> with baudrate <br>.\n"
		"  -S, --no-serial          Disable serial output.\n"
		"  -o, --opc <host>:<port>  Enable Open Pixel Control output.\n"
		"  -O, --no-opc             Disable Open Pixel Control output.\n"
		"  -g, --sdl                Enable SDL output.\n"
		"  -G, --no-sdl             Disable SDL output.\n"
		"  -m, --midi               Enable MIDI input.\n"
		"  -M, --no-midi            Disable MIDI input.\n"
		"  -c <client>:<port>       Enables MIDI input and autoconnects to <client>:<port>.\n"
		"  -x, --exec <file>        Run plugin <file> and quit.\n"
		"  -p <params>              Specifies parameters to be passed along to plugin.\n"
		"  -f, --fps <fps>          Specifies default framerate. Zero turns off framerate limiting.\n"
		"  -F, --measure-fps        Measure the framerate.\n"
		"  -h, --help               Print this help message and exit.\n"
		"  -v, --version            Print version information and exit.\n"
		"\n";

	float fps = -1.0;
	while (1) {
		int option_index = 0;
		int c = getopt_long(argc, argv, "o:Os:SgGmMc:i:Ix:p:f:Fhv", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
			case 's':
				ctx->state->serial_enabled = 1;
				ctx->state->serial_enabled_set_by_user = 1;
				//TODO: parse string for settings!
				break;
			case 'S':
				ctx->state->serial_enabled = 0;
				ctx->state->serial_enabled_set_by_user = 1;
				break;
			case 'o':
				ctx->state->opc_enabled = 1;
				ctx->state->opc_enabled_set_by_user = 1;
				ctx->state->opc_host = strdup(optarg);
				break;
			case 'O':
				ctx->state->opc_enabled = 0;
				ctx->state->opc_enabled_set_by_user = 1;
				break;
			case 'g':
				ctx->state->sdl_enabled = 1;
				ctx->state->sdl_enabled_set_by_user = 1;
				break;
			case 'G':
				ctx->state->sdl_enabled = 0;
				ctx->state->sdl_enabled_set_by_user = 1;
				break;
			case 'm':
				ctx->state->midi_enabled = 1;
				ctx->state->midi_enabled_set_by_user = 1;
				break;
			case 'M':
				ctx->state->midi_enabled = 0;
				ctx->state->midi_enabled_set_by_user = 1;
				break;
			case 'c':
				ctx->state->midi_enabled = 1;
				ctx->state->midi_enabled_set_by_user = 1;
				free(ctx->state->midi_autoconnect);
				ctx->state->midi_autoconnect = strdup(optarg);
				break;
			case 'i':
				ctx->state->load_init = 1;
				ctx->state->load_init_set_by_user = 1;
				free(ctx->state->init_file);
				ctx->state->init_file = strdup(optarg);
				break;
			case 'I':
				ctx->state->load_init = 0;
				ctx->state->load_init_set_by_user = 1;
				break;
			case 'x':
				ctx->state->exec_plugin = 1;
				ctx->state->plugin_path = strdup(optarg);
				break;
			case 'p':
				ctx->state->plugin_params = strdup(optarg);
				break;
			case 'f':
				fps = atof(optarg);
				break;
			case 'F':
				ctx->state->measure_fps = 1;
				break;
			case 'h':
				printf(usage);
				goto cleanup;
			case 'v':
				printf("ledm " VERSION "\n");
				goto cleanup;
			case '?':
			default:
				printf("Could not parse command line parameters!\n\n");
				printf(usage);
				return_value = 1;
				goto cleanup;
		}
	}

	// Print program name and version if in interactive session
	if (optind == argc)
		printf("ledm " VERSION "\n");

	// Parsing of init file
	if (ctx->state->load_init) {

		// Compute default init file path if necessary
		if (!ctx->state->init_file && config_path)
			asprintf(&ctx->state->init_file, "%sinit", config_path);

		// Parse init file if it is accessible
		if (access(ctx->state->init_file, R_OK) != -1) {
			parse_file(ctx, ctx->state->init_file);

			// Abort if there were errors in init file
			if (ctx->errors) {
				printf("Found %d error(s) while parsing init file. Exiting...\n", ctx->errors);
				return_value = 1;
				goto cleanup;
			}

			execute_context(ctx);
		} else if (ctx->state->load_init_set_by_user) { // user set file
			printf("Could not read init file!\n");
			return_value = 1;
			goto cleanup;
		}
	}


	// Initialize serial, midi, sdl components (unless this already happened in the init file), overwrite fps that might have been set in init file
	if (!ctx->state->initialized)
		io_init(ctx);
	if (fps >= 0)
		set_fps(ctx, fps);


	// Parsing and execution
	if (ctx->state->exec_plugin) { // if set to execute plugin

		plugin_entry_point_handle plugin = load_plugin(ctx->state->plugin_path);
		if (plugin)
			plugin(ctx, ctx->state->plugin_params);

	} else if (optind == argc) { // if in an interactive session since no files specified

		// Load readline history from file
		char *history_file = NULL;
		if (config_path != NULL) {
			asprintf(&history_file, "%shistory", config_path);

			// Create history file if it does not exist
			FILE* fd_tmp = fopen(history_file, "ab+");
			if (fd_tmp)
				fclose(fd_tmp);

			int n = read_history(history_file);
			if (n)
				warning("Cannot read history file: %s", strerror(n));
		}

		// Draw prompt and parse input line by line
		char prompt[20];
		while (!ctx->quit) {

			// Display prompt and read input
			printf("\033[1m");
			snprintf(prompt, 20, "[%2d] ", ctx->line_number);
			char *line = readline(prompt);
			printf("\033[0m");

			// Unless EOF was encountered, add to history, parse and execute
			if (line) {
				add_history(line);
				parse(ctx, line);
				execute_context(ctx);
				free(line);
			} else {
				printf("\n");
				break;
			}
		}

		// Be polite
		printf("Bye!\n");

		// Write readline history to file
		if (config_path != NULL) {
			int n = write_history(history_file);
			if (n)
				warning("Cannot write to history file: %s", strerror(n));
			history_truncate_file(history_file, 500);
		}
		free(history_file);


	} else { // if not in an interactive session

		// Parse all files
		for (int i=optind; i<argc; ++i)
			parse_file(ctx, argv[i]);

		// Execute context if no errors were encountered
		if (ctx->errors) {
			printf("Found %d error(s) while parsing. Exiting...\n", ctx->errors);
			return_value = 1;
			goto cleanup;
		} else {
			execute_context(ctx);
		}
	}


cleanup:
	destroy_context(ctx);
	free(config_path);

	return return_value;
}

