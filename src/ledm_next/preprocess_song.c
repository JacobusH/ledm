#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <string.h>

#include "ledm.h"


char *stage1(struct song*); // adds GLSL header and main function
char *stage2(char*); // processes @include's
struct source *stage3(char*); // separates out @painter, @pattern, @paint, @<ident>
char *stage4(struct source*); // processes @painter, @pattern, @paint, @<ident>


const char *header =
"#version 310 es\n"
"\n"
"layout (local_size_x = 1) in;\n"
"layout (rgba8, binding = 0) uniform writeonly highp image2D ledm_out_image;\n"
"layout (rgba32f, binding = 1) uniform readonly highp image2D ledm_in_positions;\n"
"\n"
"#define ledm_index gl_GlobalInvocationID.x\n"
"vec4 ledm_pos = imageLoad(ledm_in_positions, ivec2(ledm_index, 0));\n"
"#define ledm_output(X) imageStore(ledm_out_image, ivec2(gl_GlobalInvocationID.x, 0), X);\n\n\n"
;

const char *before_main = "\nvoid main() {\n";
const char *after_main = "\n}\n";

char *stage1(struct song* song) {
	
	struct source *shader = create_source(header);
	if (song->global)
		shader = source_union(shader, song->global);

	int error = 0;
	if (source_append(shader, before_main) != LEDM_SUCCESS)
		error = 1;

	if (song->preamble)
		shader = source_union(shader, song->preamble);

	//TODO add main, section logic and sections

	if (source_append(shader, after_main) != LEDM_SUCCESS)
		error = 1;

	if (error && shader) {
		destroy_source(shader);
		return NULL;
	}
	return source_join(shader);
};


char *stage2(char *str) {
	return strdup(str);
};

struct source *stage3(char *str) {
	return create_source(str);
};

char *stage4(struct source *src) {
	return source_join(src);
};


char *preprocess_song(struct song* song) {
	char *s1, *s2, *s4;
	struct source *s3;

	s1 = stage1(song);
	s2 = stage2(s1);
	s3 = stage3(s2);
	s4 = stage4(s3);

	free(s1);
	free(s2);
	destroy_source(s3);

	return s4;
};
