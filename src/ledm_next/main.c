#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include "libledm.h"
#include "ledm.h"


int main(int argc, char **argv) {
	int return_value = 0;
	int emit_only = 0;

	// Create libledm state
	struct ledm_config *cfg = ledm_config_create();
	ledm_config_load_default(cfg);

	// Handle command-line arguments
	static struct option long_options[] = {
		{"emit-only", no_argument, NULL, 'e'},
		{"serial", required_argument, NULL, 's'},
		{"no-serial", no_argument, NULL, 'S'},
		{"opc", required_argument, NULL, 'o'},
		{"no-opc", no_argument, NULL, 'O'},
		{"sdl", no_argument, NULL, 'g'},
		{"no-sdl", no_argument, NULL, 'G'},
		{"midi", optional_argument, NULL, 'm'},
		{"no-midi", no_argument, NULL, 'M'},
		{"fps", required_argument, NULL, 'f'},
		{"help", no_argument, NULL, 'h'},
		{"version", no_argument, NULL, 'v'},
		{0, 0, 0, 0}
	};

	const char* usage =
		"Usage: ledm [options] [file]\n"
		"Run ledm scripts. \n"
		"\n"
		"Options:\n"
		"  -e, --emit-only          Only emit preprocessed shader.\n"
		"  -s, --serial <tty>@<br>  Enable serial output to <tty> with baudrate <br>.\n"
		"  -S, --no-serial          Disable serial output.\n"
		"  -o, --opc <host>:<port>  Enable Open Pixel Control output.\n"
		"  -O, --no-opc             Disable Open Pixel Control output.\n"
		"  -g, --sdl                Enable SDL output.\n"
		"  -G, --no-sdl             Disable SDL output.\n"
		"  -m, --midi               Enable MIDI input.\n"
		"  -M, --no-midi            Disable MIDI input.\n"
		"  -c <client>:<port>       Enables MIDI input and autoconnects to <client>:<port>.\n"
		"  -p <params>              Specifies parameters to be passed along to plugin.\n"
		"  -f, --fps <fps>          Specifies default framerate. Zero turns off framerate limiting.\n"
		"  -h, --help               Print this help message and exit.\n"
		"  -v, --version            Print version information and exit.\n"
		"\n";


	while (1) {
		int option_index = 0;
		int c = getopt_long(argc, argv, "eo:Os:SgGmMc:p:f:hv", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
			case 'e': emit_only = 1; break;
			case 's': ledm_config_set(cfg, "io_serial", optarg); break;
			case 'S': ledm_config_set(cfg, "io_serial", "false"); break;
			case 'o': ledm_config_set(cfg, "io_opc", optarg); break;
			case 'O': ledm_config_set(cfg, "io_opc", NULL); break;
			case 'g': ledm_config_set(cfg, "io_sdl", "true"); break;
			case 'G': ledm_config_set(cfg, "io_sdl", "false"); break;
			case 'm': ledm_config_set(cfg, "io_midi", "true"); break;
			case 'M': ledm_config_set(cfg, "io_midi", "false"); break;
			case 'c':
				ledm_config_set(cfg, "io_midi", "true");
				ledm_config_set(cfg, "io_midi_autoconnect", optarg);
				break;
			case 'p': // TODO
				break;
			case 'f': ledm_config_set(cfg, "io_fps", optarg); break;
			case 'h':
				printf(usage);
				ledm_config_destroy(cfg);
				return 1;
			case 'v':
				printf("ledm " VERSION "\n");
				ledm_config_destroy(cfg);
				return 1;
			case '?':
			default:
				printf("Could not parse command line parameters!\n\n");
				printf(usage);
				ledm_config_destroy(cfg);
				return 1;
		}
	}

	if (optind + 1 != argc) {
		logmsg(CRITICAL, "Please specifiy exactly one input file!");
		ledm_config_destroy(cfg);
		return 1;
	}

	// Initialize libledm state and interpreter context
	struct ledm_state *state = ledm_initialize(cfg);
	verbosity = ledm_config_get_verbosity(cfg);

	struct song* song = load_song(argv[optind]);
	
	if (song) {
		song_print_info(song);
		if (emit_only) {
			char *shader = preprocess_song(song);
			if (shader) {
				puts(shader);
			} else {
				logmsg(CRITICAL, "Failed to preprocess song, exiting...");
				return_value = 1;
			}
			free(shader);
		} else {
			if (init_openGL() == LEDM_SUCCESS) {
				play_song(state, song);
			} else {
				logmsg(CRITICAL, "Failed to init openGL, exiting...");
				return_value = 1;
			}
		}
	} else {
		logmsg(CRITICAL, "Failed to load song, exiting...");
		return_value = 1;
	}

	ledm_destroy(state);
	ledm_config_destroy(cfg);

	return return_value;
}

