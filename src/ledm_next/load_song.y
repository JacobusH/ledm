%{
#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#include "ledm.h"

#define WHITESPACE " \f\r\t\v"



int yylex();
void yyerror(char const *);


static struct song* output = NULL;
static FILE *file = NULL;
static char *line = NULL;
static size_t line_len = 0;
static size_t line_nr = 0;
static char *input = NULL;

%}


%define api.value.type union
%token <double> NUMBER
%token <char*> STRING
%token <struct source*> SOURCE_BLOCK
%token COMMENT
%token SECTION
%token EVENTS
%token GLOBAL
%token PREAMBLE
%token FILE_VERSION
%token AUDIO
%token FIRST_SECTION
%token BEATS_PER_MINUTE
%token BEATS_PER_MEASURE
%token UNRECOGNIZED
%token END 0 "end of file"


%destructor { free($$); } STRING
%destructor { destroy_source($$); } SOURCE_BLOCK

%%

input: /* empty */ | input cmd ;

cmd: '\n';
cmd: COMMENT '\n';
cmd: FILE_VERSION STRING '\n'  {
	free(output->version);
	output->version = $2;
};
cmd: AUDIO STRING '\n' {
	free(output->audio_file);
	output->audio_file = $2;
	output->audio_offset = 0;
};
cmd: AUDIO STRING NUMBER '\n' {
	free(output->audio_file);
	output->audio_file = $2;
	output->audio_offset = $3;
};
cmd: BEATS_PER_MINUTE NUMBER '\n' {
	output->beats_per_minute = $2;
};
cmd: BEATS_PER_MEASURE NUMBER '\n' {
	output->beats_per_measure = $2;
};
cmd: SECTION STRING SOURCE_BLOCK '\n' {
	if (song_add_section(output, $2, $3) != LEDM_SUCCESS) {
		// song takes ownership of $3 only on success
		destroy_source($3);
		logmsg(ERROR, "Failed to allocate memory for source code!");
	};
	free($2);
};
cmd: GLOBAL SOURCE_BLOCK '\n' {
	if (output->global)
		destroy_source(output->global);
	output->global = $2; // takes ownership!
};
cmd: PREAMBLE SOURCE_BLOCK '\n' {
	if (output->preamble)
		destroy_source(output->preamble);
	output->preamble = $2; // takes ownership!
};


%%



void yyerror(char const *str) {
	logmsg(ERROR, "%s (line %d)", str, line_nr);
	destroy_song(output);
	output = NULL;
}



int startMatches(const char *str1, const char *str2) {
	if( strncmp(str1, str2, strlen(str2)) == 0 )
		return 1;
	return 0;
};

int yylex() {

	// Get new line of input if nothing left in input buffer
	if (!input || strlen(input)==0) {
		if (getline(&line, &line_len, file) == -1)
			return EOF;
		input = line;
		line_nr += 1;
	}

	input += strspn(input, WHITESPACE);
	size_t length = 0;
	
	switch (*input) {
	
	case '\n':
	case '\0':
		return *(input++);
	case '#': 
		while (input[length] != '\n' && input[length] != '\0') ++length;
		input += length;
		return COMMENT;
	case '"': ;
		++input;
		while (input[length] != '\n' && input[length] != '\0' && input[length] != '"') ++length;

		if (input[length] != '"')
			return UNRECOGNIZED;

		yylval.STRING = strndup(input, length);
		input += length + 1;
		return STRING;
	
	case '+': case '-': case '0': case '1': case '2': case '3':
	case '4': case '5': case '6': case '7': case '8': case '9':
		yylval.NUMBER = 0;
		double sign = 1;
		if (*input == '-') {
			sign = -1;
			++input;
		} else if (*input == '+') {
			++input;
		}

		while (isdigit(input[length])) ++length;
		double power_of_ten = 1;
		for (int i=length-1; i>=0; --i) {
			yylval.NUMBER += power_of_ten * (input[i] - '0');
			power_of_ten *= 10;
		}
		input += length;

		if (*input != '.') {
			yylval.NUMBER *= sign;
			return NUMBER;
		}

		++input;
		length = 0;
		while (isdigit(input[length])) ++length;
		for (uint i=0; i<length; ++i)
			yylval.NUMBER += pow(0.1, 1+i) * (double) (input[i] - '0');
		input += length;
		yylval.NUMBER *= sign;
		return NUMBER;
	case '{':
		++input;
		struct source *src = create_source("");
		int multiline_comment = 0;
		int singleline_comment = 0;
		int brackets = 0;
		while(1) {
			if (input == NULL)
				return EOF;
			if (*input == '\n') {
				if (getline(&line, &line_len, file) == -1)
					return EOF;
				input = line;
				line_nr += 1;
			}
			singleline_comment = 0;
			length = 0;
			while (    input[length] != '\n'
					&& brackets >= 0) {
				if (startMatches(input+length, "/*"))
					multiline_comment = 1;
				if (startMatches(input+length, "*/"))
					multiline_comment = 0;
				if (startMatches(input+length, "//"))
					singleline_comment = 1;
				if (input[length] == '{' && !multiline_comment && !singleline_comment)
					++brackets;
				if (input[length] == '}' && !multiline_comment && !singleline_comment)
					--brackets;
				++length;
			}
			if (brackets < 0)
				input[length-1] = '\0';
			if(source_append(src, input) != LEDM_SUCCESS)
				logmsg(ERROR, "Failed to allocate memory for source code!");
			input += length;
			if (brackets < 0) {
				input += 1;
				break;
			}
			
		}
		
		yylval.SOURCE_BLOCK = src;
		return SOURCE_BLOCK;
	}
	
	if (startMatches(input, "section")) {
		input += 7;
		return SECTION;
	} else if (startMatches(input, "events")) {
		input += 6;
		return EVENTS;
	} else if (startMatches(input, "global")) {
		input += 6;
		return GLOBAL;
	} else if (startMatches(input, "preamble")) {
		input += 8;
		return PREAMBLE;
	} else if (startMatches(input, "version")) {
		input += 7;
		return FILE_VERSION;
	} else if (startMatches(input, "audio")) {
		input += 5;
		return AUDIO;
	} else if (startMatches(input, "first_section")) {
		input += 13;
		return FIRST_SECTION;
	} else if (startMatches(input, "beats_per_minute")) {
		input += 16;
		return BEATS_PER_MINUTE;
	} else if (startMatches(input, "beats_per_measure")) {
		input += 17;
		return BEATS_PER_MEASURE;
	};
	
	
	++input;
	return UNRECOGNIZED;
}



struct song* load_song(const char* filename) {

	file = fopen(filename, "r");
	if (!file) {
		logmsg(ERROR, "Cannot access '%s': %s", filename, strerror(errno));
		return NULL;
	}

	output = create_song();
	if (!output) return NULL;

	output->filename = strdup(filename);

	yyparse();

	line_nr = 0;
	input = NULL;

	fclose(file);
	file = NULL;
	return output;
}

