#include <stdio.h>
#include <stdarg.h>

#include "ledm.h"


enum ledm_severity verbosity = WARNING;

void logmsg(enum ledm_severity severity, const char *format, ...) {

	if (severity < verbosity)
		return;

	switch (severity) {
		default:
		case MUTE: return;
		case CRITICAL: fprintf(stderr, "\033[31;1mCritical\033[0m: "); break;
		case ERROR: fprintf(stderr, "\033[31;1mError\033[0m: "); break;
		case WARNING: fprintf(stderr, "\033[35;1mWarning\033[0m: "); break;
		case INFO: fprintf(stderr, "Info: "); break;
		case DEBUG: fprintf(stderr, "Debug: "); break;
	}

	va_list args;
	va_start (args, format);
	vfprintf (stderr, format, args);
	va_end (args);

	fprintf(stderr, "\n");
};
