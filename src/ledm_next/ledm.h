#ifndef LEDM_H
#define LEDM_H

#include <stdlib.h>
#include "libledm.h"


extern enum ledm_severity verbosity;



struct source {
	char *content;
	struct source *next;
};

struct source* create_source(const char*);
void destroy_source(struct source*);
char* source_join(struct source*);
int source_append(struct source*, const char*);
int source_prepend(struct source*, const char*);
struct source* source_union(struct source*, struct source*);


struct song {
	char  *filename;
	char  *version;
	char  *audio_file;
	char  *first_section;
	double audio_offset;
	double beats_per_minute;
	double beats_per_measure;
	
	
	struct source *global;
	struct source *preamble;
	
	size_t   nr_sections;
	struct source **sections;
	char **section_names;
};

struct song* create_song();
void destroy_song(struct song*);
void song_print_info(struct song* song);
int song_add_section(struct song*, const char *name, struct source* src);




struct song* load_song(const char* filename);
char *preprocess_song(struct song* song);
int play_song(struct ledm_state* state, struct song* song);

int init_openGL();
void logmsg(enum ledm_severity, const char *format, ...);


#endif // LEDM_H

