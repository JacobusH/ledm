#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ledm.h"




struct song* create_song() {
	struct song *out = malloc(sizeof(struct song));
	if (out) {
		out->filename = NULL;
		out->version = NULL;
		out->audio_file = NULL;
		out->first_section = NULL;
		out->audio_offset = 0;
		out->beats_per_minute = 120;
		out->beats_per_measure = 4;
		
		out->global = NULL;
		out->preamble = NULL;
		
		out->nr_sections = 0;
		out->sections = NULL;
		out->section_names = NULL;
	}
	return out;
};

void destroy_song(struct song *song) {
	free(song->filename);
	free(song->version);
	free(song->audio_file);
	free(song->first_section);
	if (song->global)
		destroy_source(song->global);
	if (song->preamble)
		destroy_source(song->preamble);
	
	for (size_t i=0; i<song->nr_sections; ++i) {
		destroy_source(song->sections[i]);
		free(song->section_names[i]);
	}
	free(song->sections);
	free(song->section_names);
	free(song);
};


void song_print_info(struct song *song) {
	if (song->filename)
		printf("     Song File:  %s\n", song->filename);
	if (song->version)
		printf("  LEDM Version:  %s\n", song->version);
	if (song->audio_file) {
		printf("    Audio File:  %s\n", song->audio_file);
		printf("  Audio Offset:  %f\n", song->audio_offset);
	}
	printf("  Beats/Minute:  %f\n", song->beats_per_minute);
	printf(" Beats/Measure:  %f\n", song->beats_per_measure);
	printf(" Section Count:  %ld\n", song->nr_sections);
};



	size_t   nr_sections;
	struct source **sections;
	char **section_names;

int song_add_section(struct song *song, const char *name, struct source* src) {
	size_t new_nr = song->nr_sections + 1;
	char *new_name = strdup(name);
	if (!new_name)
		return LEDM_FAILURE;
	
	struct source **new_sections = realloc(song->sections, new_nr*sizeof(struct source*));
	char **new_section_names = realloc(song->section_names, new_nr*sizeof(char*));
	if (new_sections && new_section_names) {
		song->nr_sections = new_nr;
		new_sections[new_nr-1] = src;
		new_section_names[new_nr-1] = new_name;
	} else {
		free(new_name);
	}
	if (new_sections)
		song->sections = new_sections;
	if (new_section_names)
		song->section_names = new_section_names;

	return LEDM_SUCCESS;
};

