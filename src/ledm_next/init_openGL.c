#include <string.h>
#include <stdio.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES3/gl31.h>

#include "ledm.h"


void checkErrors() {
	GLenum e = glGetError();
	char *string = NULL;
	switch (e) {
	case GL_NO_ERROR:  string = "No error"; break;
	case GL_INVALID_ENUM:  string = "Invalid enum"; break;
	case GL_INVALID_VALUE: string = "Invalid value"; break;
	case GL_INVALID_OPERATION: string = "Invalid operation"; break;
	//case GL_STACK_OVERFLOW: string = "Stack overflow";
	//case GL_STACK_UNDERFLOW:   string = "Stack underflow";
	case GL_OUT_OF_MEMORY: string = "Out of memory"; break;
	default:   string = "Unknown error";
	}
	
	//if (e != GL_NO_ERROR) {
		fprintf(stderr, "OpenGL error: %s (%d)\n", string, e);
	//}
}

int init_openGL() {
	const char *client_extensions = eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS);
	if (!client_extensions) {
		logmsg(ERROR, "No extensions found.");
		return LEDM_FAILURE;
	}
	if (!strstr(client_extensions, "EGL_MESA_platform_surfaceless")) {
		logmsg(ERROR, "Extension EGL_MESA_platform_surfaceless not found.");
		return LEDM_FAILURE;
	}
	EGLDisplay display = eglGetPlatformDisplay(EGL_PLATFORM_SURFACELESS_MESA, EGL_DEFAULT_DISPLAY, NULL);
	if (display == EGL_NO_DISPLAY) {
		logmsg(ERROR, "No display found.");
		return LEDM_FAILURE;
	}
	EGLint major, minor;
	if (!eglInitialize(display, &major, &minor)) {
		logmsg(ERROR, "Failed to initialize EGL.");
		return LEDM_FAILURE;
	}
	if (!eglBindAPI(EGL_OPENGL_ES_API)) {
		logmsg(ERROR, "No OpenGL ES implementation found.");
		return LEDM_FAILURE;
	}
	EGLint attributes[] = {
		EGL_CONTEXT_MAJOR_VERSION, 3,
		EGL_CONTEXT_MINOR_VERSION, 1,
		EGL_NONE,
	};
	EGLContext context = eglCreateContext(display, EGL_NO_CONFIG_KHR, EGL_NO_CONTEXT, attributes);
	if (context == EGL_NO_CONTEXT) {
		logmsg(ERROR, "Could not create context.");
		return LEDM_FAILURE;
	}
	eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, context);
// 	if (!eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, context)) {
// 		logmsg(ERROR, "Could not make the context current.");
// 		checkErrors();
// 		return LEDM_FAILURE;
// 	}
	
	return LEDM_SUCCESS;
};

