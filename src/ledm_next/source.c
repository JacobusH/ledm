#define _POSIX_C_SOURCE 200809L

#include "stdlib.h"
#include "string.h"

#include "ledm.h"



struct source* create_source(const char *str) {
	struct source *out = malloc(sizeof(struct source));
	if (out) {
		out->content = strdup(str);
		out->next = NULL;
		if (!out->content) {
			free(out);
			return NULL;
		}
	}
	return out;
};


void destroy_source(struct source *src) {
	if (!src)
		return;
	if (src->next)
		destroy_source(src->next);
	free(src->content);
	free(src);
};


char* source_join(struct source *src) {
	if (!src)
		return NULL;

	// Count length first
	size_t len = 0;
	struct source* node = src;
	do {
		len += strlen(node->content);
	} while ( (node=node->next) != NULL );

	// Allocate space
	char *out = malloc( (len+1) * sizeof(char));
	if (!out)
		return NULL;

	// Copy contents
	char *current = out;
	node = src;
	do {
		strcpy(current, node->content);
		current += strlen(node->content);
	} while ( (node=node->next) != NULL );

	return out;
};


int source_append(struct source *src, const char *str) {
	if (!src || !str)
		return LEDM_FAILURE;

	struct source* node = src;
	while(node->next) {
		node = node->next;
	};
	node->next = create_source(str);
	if (!node->next)
		return LEDM_FAILURE;
	return LEDM_SUCCESS;
};


int source_prepend(struct source *src, const char *str) {
	if (!src || !str)
		return LEDM_FAILURE;

	struct source* node = create_source(str);
	if (!node)
		return LEDM_FAILURE;

	node->next = src->next;
	src->next = node;
	char *temp = node->content;
	node->content = src->content;
	src->content = temp;

	return LEDM_SUCCESS;
};


struct source* source_union(struct source *src1, struct source *src2) {
	if (!src1 || !src2)
		return NULL;

	struct source *out = NULL;
	char *str1 = source_join(src1);
	char *str2 = source_join(src2);
	
	if (!str1 || !str2)
		goto done;

	out = create_source(str1);
	if (out && source_append(out, str2) != LEDM_SUCCESS) {
		destroy_source(out);
		out = NULL;
	}

done:
	free(str1);
	free(str2);
	return out;
};

