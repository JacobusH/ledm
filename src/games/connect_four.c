#include <stdlib.h>
#include <SDL2/SDL.h>

#include "libledm.h"
#include "libledm_utils.h"

// Compile-time configuration
#define COLOR_PLAYER1 (struct ledm_rgba){255,255,0,255}
#define COLOR_PLAYER2 (struct ledm_rgba){0,255,255,255}
#define COLOR_BLANK (struct ledm_rgba){20,20,20,255}
float fps = 15;


enum cell_type {
	EMPTY,
	PL1,
	PL2
};

enum game_state {
	ONGOING,
	WIN,
	DRAW
};

enum react {
	ANIMATE,
	NOANIMATE
};



// Game state data
int width;
int height;
int bheight;
int plyr_turn;
int uninitialized = 1;
int cursor;
int move_y;
int countdown;
int plyr_turn;
int colrange = 100;
enum cell_type *board = NULL;
struct ledm_rgba* buffer = NULL;
struct ledm_rgba plyr_col;
enum game_state state;
enum react moved;


void initialize_game() {
	bheight = height-1;
	uninitialized = 0;
	cursor = 0;
	state = ONGOING;
	moved = NOANIMATE;
	countdown = 0;
	plyr_turn = 1;
	plyr_col = COLOR_PLAYER1;

	// Initialize board
	for (int r=0; r<height; r++)
		for (int c=0; c<width; c++)
			board[r*width + c] = EMPTY;
}


int check_board(){
	// check for WIN
	for (int r=0; r<bheight; r++){
		for (int c=0; c<width; c++){
			// horizontal check
			if (c<width-3 && (*(board + r*width + c)!=EMPTY) && (*(board + r*width + c)==*(board + r*width + c+1)) && (*(board + r*width + c+1)==*(board + r*width + c+2))  && (*(board + r*width + c+2)==*(board + r*width + c+3)))
				return 1;
			// diagonal-down check
			if (c<width-3 && r>=3 && (*(board + r*width + c)!=EMPTY) && (*(board + r*width + c)==*(board + (r-1)*width + c+1)) && (*(board + (r-1)*width + c+1)==*(board + (r-2)*width + c+2))  && (*(board + (r-2)*width + c+2)==*(board + (r-3)*width + c+3)))
				return 1;
			// vertical check
			if (r<bheight-3 && (*(board + r*width + c)!=EMPTY) && (*(board + r*width + c)==*(board + (r+1)*width + c)) && (*(board + (r+1)*width + c)==*(board + (r+2)*width + c))  && (*(board + (r+2)*width + c)==*(board + (r+3)*width + c)))
				return 1;
			// diagonal-up check
			if (r>=3 && c>=3 && (*(board + r*width + c)!=EMPTY) && (*(board + r*width + c)==*(board + (r-1)*width + c-1)) && (*(board + (r-1)*width + c-1)==*(board + (r-2)*width + c-2))  && (*(board + (r-2)*width + c-2)==*(board + (r-3)*width + c-3)))
				return 1;
		}
	}

	// check if game can go on (no DRAW)
	for (int r=0; r<bheight; r++)
		for (int c=0; c<width; c++)
			if (board[r*width + c] == EMPTY)
				return 0;

	// else DRAW
	return 2;
}

void make_move(){
	countdown=bheight;
	move_y=0;
	while(board[cursor+ width*move_y]!=EMPTY && countdown!=0) {
		move_y++;
		countdown--;
	}

	if (countdown!=0){
		moved = ANIMATE;
		board[ + move_y*width + cursor] = (plyr_turn==1) ? PL1 : PL2;
		state = check_board();
	}
}

void animate_end(struct ledm_state *s) {
	if (state == DRAW){
		plyr_col.r = 255;
		plyr_col.g = 255;
		plyr_col.b = 255;
	}

	struct ledm_rgba animcolor;
	for (int i=1; i<8; i++) {
		for (int x=0; x<width; ++x) {
			for (int y=0; y<height; ++y) {
				animcolor.r=plyr_col.r - ((plyr_col.r>colrange)?colrange:0) + (rand()%colrange);
				animcolor.g=plyr_col.g - ((plyr_col.g>colrange)?colrange:0) + (rand()%colrange);
				animcolor.b=plyr_col.b - ((plyr_col.b>colrange)?colrange:0) + (rand()%colrange);
				animcolor.a=255;
				ledm_paint_grid(s, x, y, animcolor);
			}
		}

		ledm_io_push(s);

		ledm_sleep_us(300*1000);
	}
}

void print_board(struct ledm_state *s) {

	for (int x=0; x<width; ++x) {
		for (int y=0; y<height; ++y) {
			switch (board[x+width*y]) {
				case EMPTY: ledm_paint_grid(s, x, y, COLOR_BLANK); break;
				case PL1: ledm_paint_grid(s, x, y, COLOR_PLAYER1); break;
				case PL2: ledm_paint_grid(s, x, y, COLOR_PLAYER2); break;
			}
		}
	}

	if (moved==ANIMATE && countdown!=0){
		ledm_paint_grid(s, cursor, move_y, COLOR_BLANK);
		ledm_paint_grid(s, cursor, move_y+countdown, plyr_col);
		countdown--;
	}else{
		ledm_paint_grid(s, cursor, (height-1), plyr_col);
	}
}

void on_input(struct ledm_state *s, void *state, struct snd_seq_event *midi_event, union SDL_Event *sdl_event) {
	if (sdl_event && sdl_event->type != SDL_KEYDOWN && moved != ANIMATE) {
		switch (sdl_event->key.keysym.sym) {
			case SDLK_LEFT:
			case SDLK_a:
				cursor = (cursor==0) ? 0 : cursor-1;
				break;
			case SDLK_RIGHT:
			case SDLK_d:
				cursor = (cursor>=(width-1)) ? (width-1) : cursor+1;
				break;
			case SDLK_SPACE:
			case SDLK_s:
			case SDLK_DOWN:
			case SDLK_RETURN:
				make_move();
				break;
			case SDLK_q:
				ledm_io_loop_quit(s);
				break;
		}
	}
		
	// exit possible during animation
	if (sdl_event && sdl_event->type != SDL_KEYDOWN && moved == ANIMATE && sdl_event->key.keysym.sym==SDLK_q)
		ledm_io_loop_quit(s);
}

void on_frame(struct ledm_state *s, void *context) {

	if (uninitialized) {
		initialize_game();
	}
	
	if(moved==ANIMATE && countdown==0){
		moved = NOANIMATE;
		if (state==ONGOING) {
			// switch whose turn it is
			plyr_turn = (plyr_turn == 1) ? 2 : 1;
			plyr_col = (plyr_turn == 1) ? COLOR_PLAYER1 : COLOR_PLAYER2;
		}
	}

	print_board(s);
	
	if (state!=ONGOING && moved!=ANIMATE) {
		animate_end(s);
		initialize_game();
	}
}



int main() {

	int returnvalue = 0;

	struct ledm_config *cfg = ledm_config_create();
	ledm_config_load_default(cfg);
	struct ledm_state *s = ledm_initialize(cfg);

	ledm_io_set_fps(s, fps);
	width = ledm_canvas_get_grid_width(s);
	height = ledm_canvas_get_grid_height(s);
	board = (enum cell_type *) malloc(width * height * sizeof(int));
	buffer = malloc(width*height*sizeof(struct ledm_rgba));

	if (!ledm_canvas_grid_enabled(s)) {
		puts("This game requires that grid drawing is supported!");
		returnvalue = 1;
	} else {
		ledm_io_loop(s, NULL, on_input, on_frame);
	}

	free(board);
	free(buffer);
	ledm_destroy(s);
	ledm_config_destroy(cfg);
	return returnvalue;
}

