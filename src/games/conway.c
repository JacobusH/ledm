#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <alsa/asoundlib.h>

#include "libledm.h"



#define MAX_FINGERS 10

struct finger {
	SDL_FingerID id;
	float x;
	float y;
	float hue;
};

int *lattice = NULL;
int *lattice_next = NULL;

struct finger fingers[MAX_FINGERS];
uint number_fingers = 0;

float dt = 50;
int width;
int height;


void updateFingerPos(SDL_FingerID id, float x, float y) {

	uint match = number_fingers;
	for (uint i=0; i<number_fingers; ++i)
		if (fingers[i].id == id)
			match = i;

	if (match == number_fingers) {
		if (number_fingers < MAX_FINGERS) {
			++number_fingers;
			fingers[match].id = id;
			fingers[match].hue = 0.01 * (float)(rand() % 100);
		} else {
			return;
		}
	}

	fingers[match].x = x;
	fingers[match].y = y;

}

void removeFinger(SDL_FingerID id) {

	uint next_index = 0;
	for (uint i=0; i<number_fingers; ++i) {

		// if this particle is dead, move on
		if (fingers[i].id == id)
			continue;

		if (next_index != i)
			fingers[next_index] = fingers[i];

		next_index++;
	}

	number_fingers = next_index;
}



void on_input(struct ledm_state *state, void *context, struct snd_seq_event *midi_event, union SDL_Event *sdl_event) {

	if (sdl_event && sdl_event->type == SDL_FINGERDOWN) {
		updateFingerPos(sdl_event->tfinger.fingerId, sdl_event->tfinger.x, sdl_event->tfinger.y);
	} else if (sdl_event && sdl_event->type == SDL_FINGERMOTION) {
		updateFingerPos(sdl_event->tfinger.fingerId, sdl_event->tfinger.x, sdl_event->tfinger.y);
	} else if (sdl_event && sdl_event->type == SDL_FINGERUP) {
		removeFinger(sdl_event->tfinger.fingerId);
	}


}


enum dir {NORTH, EAST, SOUTH, WEST, NORTHEAST, NORTHWEST, SOUTHEAST, SOUTHWEST};
int get_neighbour(int x, int y, enum dir dir) {
	int rx = x;
	int ry = y;
	switch (dir) {
		case NORTH: ry = (height+ry+1)%height; break;
		case EAST:  rx = (width+rx+1)%width;  break;
		case SOUTH: ry = (height+ry-1)%height; break;
		case WEST:  rx = (width+rx-1)%width;  break;
		case NORTHEAST: rx = (width+rx+1)%width; ry = (height+ry+1)%height; break;
		case NORTHWEST: rx = (width+rx-1)%width; ry = (height+ry+1)%height; break;
		case SOUTHEAST: rx = (width+rx+1)%width; ry = (height+ry-1)%height; break;
		case SOUTHWEST: rx = (width+rx-1)%width; ry = (height+ry-1)%height; break;
	}
	return lattice[rx+width*ry];
};

int count_neighbours(int x, int y) {
	return get_neighbour(x,y, NORTH)
	     + get_neighbour(x,y, EAST)
	     + get_neighbour(x,y, SOUTH)
	     + get_neighbour(x,y, WEST)
	     + get_neighbour(x,y, NORTHEAST)
	     + get_neighbour(x,y, NORTHWEST)
	     + get_neighbour(x,y, SOUTHEAST)
	     + get_neighbour(x,y, SOUTHWEST);
};

void on_frame(struct ledm_state *state, void *context) {

	// Spawn life with your fingers
	for(uint i=0; i<number_fingers; ++i) {
		int x = (int)(width+fingers[i].x+0.5) % width;
		int y = (int)(2*height-fingers[i].y-0.5) % height;
		lattice[x+width*y] = 1;
	}

	// Apocalypse if 8 fingers
	if (number_fingers >= 8)
		for (int x=0; x<width; ++x)
			for (int y=0; y<height; ++y)
				lattice[x+width*y] = 0;

	for (int x=0; x<width; ++x) {
		for (int y=0; y<height; ++y) {
			int neighbours = count_neighbours(x,y);

			// Draw current step
			if (lattice[x+width*y]) {
				switch (neighbours) {
					default:
					case 0: ledm_paint_grid(state, x, y,  (struct ledm_rgba){50,0,0,255}); break;
					case 1: ledm_paint_grid(state, x, y,  (struct ledm_rgba){100,30,0,255}); break;
					case 2: ledm_paint_grid(state, x, y,  (struct ledm_rgba){150,40,0,255}); break;
					case 3: ledm_paint_grid(state, x, y,  (struct ledm_rgba){200,70,0,255}); break;
					case 4: ledm_paint_grid(state, x, y,  (struct ledm_rgba){250,100,0,255}); break;
					case 5: ledm_paint_grid(state, x, y,  (struct ledm_rgba){250,200,0,255}); break;
					case 6: ledm_paint_grid(state, x, y,  (struct ledm_rgba){250,200,100,255}); break;
					case 7: ledm_paint_grid(state, x, y,  (struct ledm_rgba){250,250,250,255}); break;
					case 8: ledm_paint_grid(state, x, y,  (struct ledm_rgba){0,255,255,255}); break;
				}
			} else {
				ledm_paint_grid(state, x, y,  (struct ledm_rgba){0,0,0,255});
			}

			// Calculate next step unless paused, as indicated by fingers down
			if (!number_fingers) {
				if (lattice[x+width*y]) { // If cell lives
					if (neighbours == 2 || neighbours == 3)
						lattice_next[x+width*y] = 1;
					else
						lattice_next[x+width*y] = 0;
				} else { // If cell is dead
					if (neighbours == 3)
						lattice_next[x+width*y] = 1;
					else
						lattice_next[x+width*y] = 0;
				}
			} else {
				lattice_next[x+width*y] = lattice[x+width*y];
			}
		}
	}

	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			lattice[x+width*y] = lattice_next[x+width*y];
}


int main() {

	int returnvalue = 0;
	struct ledm_config *cfg = ledm_config_create();
	ledm_config_load_default(cfg);
	struct ledm_state *state = ledm_initialize(cfg);


	ledm_io_set_fps(state, 7);
	dt = 1.0/7.0;
	width = ledm_canvas_get_grid_width(state);
	height = ledm_canvas_get_grid_height(state);
	lattice = malloc(sizeof(int) * width * height);
	lattice_next = malloc(sizeof(int) * width * height);


	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			lattice[x + width*y] = rand() % 2;


	if (ledm_canvas_grid_enabled(state)) {
		ledm_io_loop(state, NULL, on_input, on_frame);
	} else {
		puts("This game requires that grid drawing is supported!");
		returnvalue = 1;
	}


	free(lattice);
	free(lattice_next);
	ledm_destroy(state);
	ledm_config_destroy(cfg);
	return returnvalue;
}

