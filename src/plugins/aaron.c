#include <stdio.h>
#include <math.h>

#include "libledm.h"
#include "ledm.h"








static void add_cloud(struct ledm_state *state, int leds, float radius, float x, float y) {

	for (int i = 0; i<leds; ++i) 
		ledm_canvas_add_led(state, 
			x + radius*sin(2*3.14159*(float)i/(float)leds), 
			y + radius*cos(2*3.14159*(float)i/(float)leds));
}


static int layout_clouds(struct ledm_state *state, struct ledm_config *cfg) {

	int count = ledm_config_get_int(cfg, "hw_layout_clouds_count", 4, state);
	int leds = ledm_config_get_int(cfg, "hw_layout_clouds_leds", 12, state);


	for (int i = 0; i<count; ++i)
		add_cloud(state, leds, 3, 3+10*i, 3);


	return LEDM_SUCCESS;
}





float fmodulo( float value, float m) {
    float mod = fmodf(value,m);
    if (mod <  0) {
        mod += m;
    }
    return mod;
}

static float pattern_checkerboard(union ledm_var *params, float x, float y) {
	float xperiod = params[0].f;
	float xoffset = params[1].f;
	float yperiod = params[2].f;
	float yoffset = params[3].f;

	return (float) (
		(fmodulo((x-xoffset)/xperiod, 1) < 0.5 && fmodulo((y-yoffset)/yperiod+0.5, 1) < 0.5) ||
		(fmodulo((x-xoffset)/xperiod+0.5, 1) < 0.5 && fmodulo((y-yoffset)/yperiod, 1) < 0.5) );
}

void plugin_entry_point_libledm(struct ledm_state *state, struct ledm_config *cfg) {
	ledm_register_pattern(state,  "checkerboard", "ffff", pattern_checkerboard);
	ledm_register_layout(state, "clouds", layout_clouds);
}





void cmd_example_moo(struct context *ctx, uint *args) {
	puts("\n"
	"            ^__^              \n"
	"  MOOOOO!   (oo)\\_______     \n"
	"            (__)\\       )\\  \n"
	"                ||----w | *   \n"
	"                ||     ||     \n");
}

void cmd_example_echo(struct context *ctx, uint *args) {
	printf("%s\n", ctx->vars[args[0]].s);
}

void cmd_example_help(struct context *ctx, uint *args) {
	puts("Usage of 'example':\n"
	"    example moo            Have you moo'ed today?\n"
	"    example echo <string>  Echos the string back at you.\n"
	"    example help           Show this help message.\n");
}

struct cmd subcommands_example[] = {
	CMD_SIMPLE("", cmd_example_help),
	CMD_SIMPLE("moo", cmd_example_moo),
	CMD_ARGUMENTS("echo", cmd_example_echo, "s"),
	CMD_SIMPLE("help", cmd_example_help),
	CMD_LIST_END
};

void cmd_argexample(struct context *ctx, uint *args) {
	printf("f=%f, wuhan=%f, berlin=%f\n", ctx->vars[args[0]].f, ctx->vars[args[1]].f, ctx->vars[args[2]].f);
}

void plugin_entry_point_interpreter(struct context *ctx, struct ledm_config *cfg) {
	add_cmd(ctx, CMD_SUBCOMMANDS("example", subcommands_example));
	add_cmd(ctx, CMD_ARGUMENTS("a", cmd_argexample, "f{wuhan=2,berlin=1}f"));
}

