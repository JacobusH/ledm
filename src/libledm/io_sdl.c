#include <stdlib.h>
#include <SDL2/SDL.h>

#include "libledm_internal.h"



static void ledm_io_sdl_update_geometry(struct ledm_state *state) {

	float canvas_width = ledm_canvas_get_xmax(state) - ledm_canvas_get_xmin(state);
	float canvas_height = ledm_canvas_get_ymax(state) - ledm_canvas_get_ymin(state);

	SDL_GetWindowSize(state->sdl_window, &state->sdl_window_width, &state->sdl_window_height);

	float scale_w = (float)(state->sdl_window_width  -2*state->sdl_window_margin) / canvas_width;
	float scale_h = (float)(state->sdl_window_height -2*state->sdl_window_margin) / canvas_height;

	state->sdl_window_offset_x = state->sdl_window_margin;
	state->sdl_window_offset_y = state->sdl_window_margin;

	if (scale_w < scale_h) {
		state->sdl_scale = scale_w;
		state->sdl_window_offset_y = (int) (state->sdl_window_height - state->sdl_scale*canvas_height )/2;
	} else {
		state->sdl_scale = scale_h;
		state->sdl_window_offset_x = (int) (state->sdl_window_width - state->sdl_scale*canvas_width )/2;
	}
}


int ledm_io_sdl_init(struct ledm_state *state, struct ledm_config *cfg) {

	state->sdl_renderer = NULL;
	state->sdl_window = NULL;

	if (!ledm_config_get_bool(cfg, "io_sdl", 1, state))
		return LEDM_SUCCESS;

	state->sdl_window_margin = ledm_config_get_int(cfg, "io_sdl_window_margin", 50, state);
	state->sdl_led_size = ledm_config_get_int(cfg, "io_sdl_led_size", 12, state);

	if (SDL_Init(SDL_INIT_VIDEO ) < 0) {
		ledm_log(state, ERROR, "Couldn't initialize SDL! SDL_Error: %s", SDL_GetError());
		return LEDM_FAILURE;
	}

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");

	state->sdl_window = SDL_CreateWindow("libledm " VERSION, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 500, 500, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (state->sdl_window == NULL) {
		ledm_log(state, ERROR, "Couldn't create SDL window! SDL_Error: %s", SDL_GetError());
		return LEDM_FAILURE;
	}

	state->sdl_renderer = SDL_CreateRenderer(state->sdl_window, -1, SDL_RENDERER_ACCELERATED);
	if (state->sdl_renderer == NULL) {
		ledm_log(state, ERROR, "Couldn't create SDL renderer! SDL Error: %s\n", SDL_GetError());
		SDL_DestroyWindow(state->sdl_window);
		state->sdl_window = NULL;
		return LEDM_FAILURE;
	}

	SDL_RenderPresent(state->sdl_renderer);

	ledm_io_sdl_update_geometry(state);

	return LEDM_SUCCESS;
};


int ledm_io_sdl_poll(struct ledm_state *state, void *context, ledm_on_input on_input) {

	if (!state->sdl_renderer)
		return LEDM_SUCCESS;

	SDL_Event sdl_event;
	while (SDL_PollEvent(&sdl_event) != 0) {
		if (sdl_event.type == SDL_QUIT || (sdl_event.type == SDL_KEYDOWN && sdl_event.key.keysym.sym == SDLK_q)) {
			state->quit_io_loop = 1;
		//else if (sdl_event.type == SDL_WINDOWEVENT && sdl_event.window.event == SDL_WINDOWEVENT_RESIZED)
		} else if (sdl_event.type == SDL_FINGERUP || sdl_event.type == SDL_FINGERDOWN || sdl_event.type == SDL_FINGERMOTION) {
			sdl_event.tfinger.x = (state->sdl_window_width*sdl_event.tfinger.x - state->sdl_window_offset_x)/state->sdl_scale;
			sdl_event.tfinger.y = (-state->sdl_window_height*sdl_event.tfinger.y - state->sdl_window_offset_y + state->sdl_window_height)/state->sdl_scale;
		}

		if (on_input)
			on_input(state, context, NULL, &sdl_event);
	}

	// Update geometry helper variables
	ledm_io_sdl_update_geometry(state);

	return LEDM_SUCCESS;
};


int ledm_io_sdl_push(struct ledm_state *state) {

	if (!state->sdl_renderer)
		return LEDM_SUCCESS;

	// Clear renderer to be sure
	SDL_RenderClear(state->sdl_renderer);

	// Draw background
	SDL_SetRenderDrawColor(state->sdl_renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(state->sdl_renderer, NULL);

	// Draw pixel grid
	SDL_Rect r;
	r.w = state->sdl_led_size;
	r.h = state->sdl_led_size;
	for (uint i = 0; i<state->num_leds; ++i) {
		r.x = state->sdl_window_offset_x - state->sdl_led_size/2 + state->sdl_scale*state->leds[i].x;
		r.y = - state->sdl_window_offset_y - state->sdl_led_size/2 + state->sdl_window_height-state->sdl_scale*state->leds[i].y;
		SDL_SetRenderDrawColor(state->sdl_renderer,
				state->led_colors[i].r,
				state->led_colors[i].g,
				state->led_colors[i].b,
				255);
		SDL_RenderFillRect(state->sdl_renderer, &r);
	}

	// Render!
	SDL_RenderPresent(state->sdl_renderer);

	return LEDM_SUCCESS;
};


int ledm_io_sdl_deinit(struct ledm_state *state) {

	if (state->sdl_renderer)
		SDL_DestroyRenderer(state->sdl_renderer);

	if (state->sdl_window)
		SDL_DestroyWindow(state->sdl_window);

	state->sdl_renderer = NULL;
	state->sdl_window = NULL;
	return LEDM_SUCCESS;
};


