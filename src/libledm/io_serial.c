#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <string.h>
#include <sys/ioctl.h>

#include "libledm_internal.h"
#include "libledm_utils.h"


#define CRTSCTS  020000000000

int ledm_io_serial_init(struct ledm_state *state, struct ledm_config *cfg) {

	struct termios toptions;

	state->serial_path = ledm_config_get(cfg, "io_serial", NULL);
	state->serial_baudrate = ledm_config_get_int(cfg, "io_serial_baudrate", 115200, state);
	state->serial_socket = -1;

	if (!state->serial_path)
		return LEDM_SUCCESS;

	state->serial_socket = open(state->serial_path, O_WRONLY | O_NOCTTY );

	if (state->serial_socket == -1) {
		ledm_log(state, ERROR, "io_serial: Unable to open serial tty: %s", strerror(errno));
		return LEDM_FAILURE;
	}
	if (tcgetattr(state->serial_socket, &toptions) < 0) {
		ledm_log(state, ERROR, "io_serial: Couldn't get tty attributes");
		return LEDM_FAILURE;
	}

	speed_t brate = state->serial_baudrate; // let you override switch below if needed
	switch(state->serial_baudrate) {
	case 4800:   brate=B4800;   break;
	case 9600:   brate=B9600;   break;
	#ifdef B14400
	case 14400:  brate=B14400;  break;
	#endif
	case 19200:  brate=B19200;  break;
	#ifdef B28800
	case 28800:  brate=B28800;  break;
	#endif
	case 38400:  brate=B38400;  break;
	case 57600:  brate=B57600;  break;
	case 115200: brate=B115200; break;
	#ifdef B921600
	case 921600: brate=B921600; break;
	#endif
	#ifdef B2000000
	case 2000000: brate=B2000000; break;
	#endif
	#ifdef B2500000
	case 2500000: brate=B2500000; break;
	#endif
	#ifdef B3000000
	case 3000000: brate=B3000000; break;
	#endif
	#ifdef B3500000
	case 3500000: brate=B3500000; break;
	#endif
	#ifdef B4000000
	case 4000000: brate=B4000000; break;
	#endif
	//default: TODO: error message, then exit
	}
	cfsetispeed(&toptions, brate);
	cfsetospeed(&toptions, brate);

	// 8N1
	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	// no flow control
	toptions.c_cflag &= ~CRTSCTS;

	//toptions.c_cflag &= ~HUPCL; // disable hang-up-on-close to avoid reset

	toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
	toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

	toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
	toptions.c_oflag &= ~OPOST; // make raw

	// see: http://unixwiz.net/techtips/termios-vmin-vtime.html
	toptions.c_cc[VMIN]  = 0;
	toptions.c_cc[VTIME] = 0;
	//toptions.c_cc[VTIME] = 20;

	tcsetattr(state->serial_socket, TCSANOW, &toptions);
	if(tcsetattr(state->serial_socket, TCSAFLUSH, &toptions) < 0) {
		ledm_log(state, ERROR, "io_serial: Couldn't set tty attributes");
		return LEDM_FAILURE;
	}
	
	return LEDM_SUCCESS;
};


int ledm_io_serial_push(struct ledm_state *state) {

	if (!state->serial_path)
		return LEDM_SUCCESS;


	uint16_t num_LEDs = (uint16_t) state->num_leds;
	const uint8_t header[4] = { 0xDE, 0xAD, 0xBE, 0xEF };
	uint n = write(state->serial_socket, header, sizeof(header));
	n += write(state->serial_socket, &num_LEDs, 2);
	if (n != sizeof(header)+2)
		ledm_log(state, ERROR, "io_serial: Failed to write header: %s", strerror(errno));
	uint LEDs_written = 0;
	uint8_t buffer[3];
	while (LEDs_written < state->num_leds) {
		buffer[0] = state->led_colors[LEDs_written].r;
		buffer[1] = state->led_colors[LEDs_written].g;
		buffer[2] = state->led_colors[LEDs_written].b;

		n = write(state->serial_socket, buffer, 3);//paint_data.num_LEDs*3);
		if (n != 3) {
			ledm_log(state, ERROR, "io_serial: Failed to write data: %s", strerror(errno));
			return LEDM_FAILURE;
		} else {
			LEDs_written++;
		}
		if (LEDs_written % 3 == 0) {
			ledm_sleep_us(1);
		}
	}

	return LEDM_SUCCESS;
};


int ledm_io_serial_deinit(struct ledm_state *state) {

	free(state->serial_path);

	if (state->serial_socket != -1)
		close(state->serial_socket);

	state->serial_path = NULL;
	state->serial_socket = -1;

	return LEDM_SUCCESS;
};


