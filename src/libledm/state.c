#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include "libledm_internal.h"




struct ledm_state *ledm_initialize(struct ledm_config *cfg) {

	struct ledm_state *state = malloc(sizeof(struct ledm_state));

	if (!state)
		return NULL;

	state->mix_mode = OVERWRITE;
	state->quit_io_loop = 0;
	state->grid_height = 0;
	state->grid_width = 0;
	state->grid_trafo = NULL;

	// Need verbosity first to have correct error logging
	state->verbosity = ledm_config_get_verbosity(cfg);

	ledm_log(state, INFO, "Initializing libledm v" LIBLEDM_VERSION "...");

	state->fps = ledm_config_get_float(cfg, "io_fps", 50, state);

	// Initialize registers
	state->leds = NULL;
	state->led_colors = NULL;
	state->num_leds = 0;
	state->grid_width = 0;
	state->grid_height = 0;
	state->grid_trafo = NULL;
	state->painters = NULL;
	state->num_painters = 0;
	state->patterns = NULL;
	state->num_patterns = 0;
	state->particles = NULL;
	state->num_particles = 0;
	state->layouts = NULL;
	state->num_layouts = 0;

	// Register builtins
	ledm_register_builtin_painters(state);
	ledm_register_builtin_patterns(state);
	ledm_register_builtin_particles(state);
	ledm_register_builtin_layouts(state);


	// Load plugins
	char *file = NULL;
	for (int i=0; ledm_config_get_nth(cfg, "plugin_libledm", i, &file) == LEDM_SUCCESS; ++i) {
		void* handle = dlopen(file, RTLD_LAZY);
		if (handle == NULL) {
			ledm_log(state, ERROR, "Could not open libledm plugin: %s", dlerror());
			continue;
		}

		void (*f)(struct ledm_state*, struct ledm_config*) = dlsym(handle, "plugin_entry_point_libledm");
		if (f)
			f(state, cfg);
		else
			ledm_log(state, ERROR, "Could not find plugin_entry_point_libledm in plugin: %s", dlerror());
	}
	free(file);


	// Set layout
	char *layout = ledm_config_get(cfg, "hw_layout", "rectangle");
	ledm_canvas_set_layout(state, layout, cfg);
	free(layout);

	// Initialize IO modules
	ledm_io_serial_init(state, cfg);
	ledm_io_opc_init(state, cfg);
	ledm_io_sdl_init(state, cfg);
	ledm_io_midi_init(state, cfg);


	ledm_log(state, DEBUG, "Initialization complete.");
	return state;
};


void ledm_destroy(struct ledm_state *state) {


	ledm_io_serial_deinit(state);
	ledm_io_opc_deinit(state);
	ledm_io_sdl_deinit(state);
	ledm_io_midi_deinit(state);


	for (uint i=0; i<state->num_painters; ++i) {
		free(state->painters[i].name);
		free(state->painters[i].format);
	}
	for (uint i=0; i<state->num_patterns; ++i) {
		free(state->patterns[i].name);
		free(state->patterns[i].format);
	}
	for (uint i=0; i<state->num_particles; ++i) {
		free(state->particles[i].name);
		// TODO
	}
	for (uint i=0; i<state->num_layouts; ++i) {
		free(state->layouts[i].name);
	}

	free(state->painters);
	free(state->patterns);
	free(state->particles);
	free(state->layouts);
	free(state->leds);

	ledm_log(state, INFO, "libledm deinitialized.");
	free(state);

};
