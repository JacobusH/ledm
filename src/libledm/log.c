#include <stdio.h>
#include <stdarg.h>

#include "libledm_internal.h"


void ledm_log(struct ledm_state *state, enum ledm_severity severity, const char *format, ...) {

	if ( (state && severity < state->verbosity) || (!state && severity < ERROR) )
		return;

	switch (severity) {
		default:
		case MUTE: return;
		case CRITICAL: fprintf(stderr, "[libledm] Critical: "); break;
		case ERROR: fprintf(stderr, "[libledm] Error: "); break;
		case WARNING: fprintf(stderr, "[libledm] Warning: "); break;
		case INFO: fprintf(stderr, "[libledm] Info: "); break;
		case DEBUG: fprintf(stderr, "[libledm] Debug: "); break;
	}

	va_list args;
	va_start (args, format);
	vfprintf (stderr, format, args);
	va_end (args);

	fprintf(stderr, "\n");
};


void ledm_log_bare(struct ledm_state *state, enum ledm_severity severity, const char *format, ...) {

	if ( (state && severity < state->verbosity) || (!state && severity < ERROR) )
		return;

	va_list args;
	va_start (args, format);
	vfprintf (stderr, format, args);
	va_end (args);

	fprintf(stderr, "\n");
};
