#include "libledm.h"
#include "libledm_utils.h"

// following conversion maps adapted from hsv2rgb.cpp from the FastLED library!

#define HSV_SECTION_6 (0x20)
#define HSV_SECTION_3 (0x40)
struct ledm_rgba ledm_hsv2rgb_spectrum(uint hue, uint sat, uint val) {
	// takes hue between 0 and 191, sat and value between 0 and 255

	struct ledm_rgba rgba;
	rgba.a = 255;

	// The brightness floor is minimum number that all of
	// R, G, and B will be set to.
	uint8_t invsat = 255 - sat;
	uint8_t brightness_floor = (val * invsat) / 256;

	// The color amplitude is the maximum amount of R, G, and B
	// that will be added on top of the brightness_floor to
	// create the specific hue desired.
	uint8_t color_amplitude = val - brightness_floor;

	// Figure out which section of the hue wheel we're in,
	// and how far offset we are withing that section
	uint8_t section = ((uint8_t)hue) / HSV_SECTION_3; // 0..2
	uint8_t offset = ((uint8_t)hue) % HSV_SECTION_3;  // 0..63

	uint8_t rampup = offset; // 0..63
	uint8_t rampdown = (HSV_SECTION_3 - 1) - offset; // 63..0

	// We now scale rampup and rampdown to a 0-255 range -- at least
	// in theory, but here's where architecture-specific decsions
	// come in to play:
	// To scale them up to 0-255, we'd want to multiply by 4.
	// But in the very next step, we multiply the ramps by other
	// values and then divide the resulting product by 256.
	// So which is faster?
	//   ((ramp * 4) * othervalue) / 256
	// or
	//   ((ramp	) * othervalue) /  64
	// It depends on your processor architecture.
	// On 8-bit AVR, the "/ 256" is just a one-cycle register move,
	// but the "/ 64" might be a multicycle shift process. So on AVR
	// it's faster do multiply the ramp values by four, and then
	// divide by 256.
	// On ARM, the "/ 256" and "/ 64" are one cycle each, so it's
	// faster to NOT multiply the ramp values by four, and just to
	// divide the resulting product by 64 (instead of 256).
	// Moral of the story: trust your profiler, not your insticts.

	// Since there's an AVR assembly version elsewhere, we'll
	// assume what we're on an architecture where any number of
	// bit shifts has roughly the same cost, and we'll remove the
	// redundant math at the source level:

	//  // scale up to 255 range
	//  //rampup *= 4; // 0..252
	//  //rampdown *= 4; // 0..252

	// compute color-amplitude-scaled-down versions of rampup and rampdown
	uint8_t rampup_amp_adj   = (rampup   * color_amplitude) / (256 / 4);
	uint8_t rampdown_amp_adj = (rampdown * color_amplitude) / (256 / 4);

	// add brightness_floor offset to everything
	uint8_t rampup_adj_with_floor   = rampup_amp_adj   + brightness_floor;
	uint8_t rampdown_adj_with_floor = rampdown_amp_adj + brightness_floor;


	if( section ) {
		if( section == 1) {
			// section 1: 0x40..0x7F
			rgba.r = brightness_floor;
			rgba.g = rampdown_adj_with_floor;
			rgba.b = rampup_adj_with_floor;
		} else {
			// section 2; 0x80..0xBF
			rgba.r = rampup_adj_with_floor;
			rgba.g = brightness_floor;
			rgba.b = rampdown_adj_with_floor;
		}
	} else {
		// section 0: 0x00..0x3F
		rgba.r = rampdown_adj_with_floor;
		rgba.g = rampup_adj_with_floor;
		rgba.b = brightness_floor;
	}
	return rgba;
}

#define K255 255
#define K171 171
#define K170 170
#define K85  85
uint8_t scale8( uint8_t i, uint8_t scale) {
	return (((unsigned short)i) * (1+(unsigned short)(scale))) >> 8;
}
uint8_t scale8_video( uint8_t i, uint8_t scale) {
	return (((int)i * (int)scale) >> 8) + ((i&&scale)?1:0);
}
struct ledm_rgba ledm_hsv2rgb_rainbow(uint _hue, uint _sat, uint _val) {
	struct ledm_rgba rgba;
	rgba.a = 255;

	uint8_t hue = _hue;
	uint8_t sat = _sat;
	uint8_t val = _val;

	uint8_t offset = hue & 0x1F; // 0..31

	// offset8 = offset * 8
	uint8_t offset8 = offset << 3;

	uint8_t third = scale8( offset8, (256 / 3)); // max = 85

	if( ! (hue & 0x80) ) {
		// 0XX
		if( ! (hue & 0x40) ) {
			// 00X
			//section 0-1
			if( ! (hue & 0x20) ) {
				// 000
				//case 0: // R -> O
				rgba.r = K255 - third;
				rgba.g = third;
				rgba.b = 0;
			} else {
				// 001
				//case 1: // O -> Y
				rgba.r = K171;
				rgba.g = K85 + third ;
				rgba.b = 0;
			}
		} else {
			//01X
			// section 2-3
			if( !  (hue & 0x20) ) {
				// 010
				//case 2: // Y -> G
				//uint8_t twothirds = (third << 1);
				uint8_t twothirds = scale8( offset8, ((256 * 2) / 3)); // max=170
				rgba.r = K171 - twothirds;
				rgba.g = K170 + third;
				rgba.b = 0;
			} else {
				// 011
				// case 3: // G -> A
				rgba.r = 0;
				rgba.g = K255 - third;
				rgba.b = third;
			}
		}
	} else {
		// section 4-7
		// 1XX
		if( ! (hue & 0x40) ) {
			// 10X
			if( ! ( hue & 0x20) ) {
				// 100
				//case 4: // A -> B
				rgba.r = 0;
				//uint8_t twothirds = (third << 1);
				uint8_t twothirds = scale8( offset8, ((256 * 2) / 3)); // max=170
				rgba.g = K171 - twothirds; //K170?
				rgba.b = K85  + twothirds;
			} else {
				// 101
				//case 5: // B -> P
				rgba.r = third;
				rgba.g = 0;
				rgba.b = K255 - third;
			}
		} else {
			if( !  (hue & 0x20)  ) {
				// 110
				//case 6: // P -- K
				rgba.r = K85 + third;
				rgba.g = 0;
				rgba.b = K171 - third;
			} else {
				// 111
				//case 7: // K -> R
				rgba.r = K170 + third;
				rgba.g = 0;
				rgba.b = K85 - third;
			}
		}
	}

	// Scale down colors if we're desaturated at all
	// and add the brightness_floor to r, g, and b.
	if( sat != 255 ) {
		if( sat == 0) {
			rgba.r = 255; rgba.b = 255; rgba.g = 255;
		} else {
			//nscale8x3_video( r, g, b, sat);
			if( rgba.r ) rgba.r = scale8( rgba.r, sat);
			if( rgba.g ) rgba.g = scale8( rgba.g, sat);
			if( rgba.b ) rgba.b = scale8( rgba.b, sat);

			uint8_t desat = 255 - sat;
			desat = scale8( desat, desat);

			uint8_t brightness_floor = desat;
			rgba.r += brightness_floor;
			rgba.g += brightness_floor;
			rgba.b += brightness_floor;
		}
	}

	// Now scale everything down if we're at value < 255.
	if( val != 255 ) {

		val = scale8_video( val, val);
		if( val == 0 ) {
			rgba.r=0; rgba.g=0; rgba.b=0;
		} else {
			// nscale8x3_video( r, g, b, val);
			if( rgba.r ) rgba.r = scale8( rgba.r, val);
			if( rgba.g ) rgba.g = scale8( rgba.g, val);
			if( rgba.b ) rgba.b = scale8( rgba.b, val);
		}
	}
	return rgba;
} 
