#include <math.h>
#include "libledm_internal.h"
#include "libledm_utils.h"



static struct ledm_rgba painter_solid(union ledm_var *params, float x, float y) {

	return params[0].c;
}


static struct ledm_rgba painter_valnoise(union ledm_var *params, float x, float y) {
	struct ledm_rgba out;
	//float hue = params[0].f;
	//float sat = params[1].f;
	float xoff = params[2].f;
	float yoff = params[3].f;
	float scale = params[4].f;
	float time = params[5].f;

	float noise = 0.5*(ledm_noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );

	out.r = (uint)(255*noise);
	out.g = (uint)(255*noise);
	out.b = (uint)(255*noise);

	return out;
}


static struct ledm_rgba painter_huenoise(union ledm_var *params, float x, float y) {
	float sat = 255*params[0].f;
	float val = 255*params[1].f;
	float xoff = params[2].f;
	float yoff = params[3].f;
	float scale = params[4].f;
	float time = params[5].f;

	// float hue = 255.0*0.5*(noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );
	float hue = 250.0*0.5*(ledm_noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );

	return ledm_hsv2rgb_rainbow((uint)hue, (uint)sat, (uint) val);
}


static struct ledm_rgba painter_radial_rainbow(union ledm_var *params, float x, float y) {
	float x0 = params[0].f;
	float y0 = params[1].f;
	float offset = params[2].f;
	float sat = 255*params[3].f;
	float val = 255*params[4].f;

	// float hue = 255.0*0.5*(noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );
	float hue = 255.0*(offset+atan2(y-y0,x-x0))/(2*3.14159);

	return ledm_hsv2rgb_rainbow((uint)hue, (uint)sat, (uint) val);
}


int ledm_register_builtin_painters(struct ledm_state *state) {

	return 
		ledm_register_painter(state, "color", "c", painter_solid) ||
		ledm_register_painter(state, "valnoise", "{hue=0,sat=1,xoff=0,yoff=0,scale=1,time=0}", painter_valnoise) ||
		ledm_register_painter(state, "radial_rainbow", "{x=0,y=0,offset=0,sat=1,val=1}", painter_radial_rainbow) ||
		ledm_register_painter(state, "huenoise", "{sat=1,val=1,xoff=0,yoff=0,scale=1,time=0}", painter_huenoise);
};
