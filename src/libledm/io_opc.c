#include "stdlib.h"
#include "util_opc.h"

#include "libledm_internal.h"


// TODO: eventually either rewrite the opc code so that we don't need a buffer, or have a buffer managed by ledm_initialize and ledm_destroy that gets resized together with the leds array so that we don't segfault if adding leds after initializing io_opc

int ledm_io_opc_init(struct ledm_state *state, struct ledm_config *cfg) {


	state->opc_host = ledm_config_get(cfg, "io_opc", NULL);

	if (!state->opc_host)
		return LEDM_SUCCESS;

	state->opc_out = opc_new_sink_socket(state->opc_host);
	
	return LEDM_SUCCESS;
};


int ledm_io_opc_push(struct ledm_state *state) {

	if (!state->opc_host)
		return LEDM_SUCCESS;

	opc_put_pixels(state->opc_out, 0, (u16)state->num_leds, state->led_colors);
	
	return LEDM_SUCCESS;
};


int ledm_io_opc_deinit(struct ledm_state *state) {

	free(state->opc_host);

	state->opc_host = NULL;

	return LEDM_SUCCESS;
};


