#ifndef LIBLEDM_UTILS_H
#define LIBLEDM_UTILS_H

#include "libledm.h"


/*
 * noise1234 Perlin noise by Stefan Gustavson
 * Downloaded from github.com/stegu/perlin-noise/
 *
 * For more information, see util_noise1234.c
 *
 */
extern float ledm_noise1( float x );
extern float ledm_noise2( float x, float y );
extern float ledm_noise3( float x, float y, float z );
extern float ledm_noise4( float x, float y, float z, float w );
extern float ledm_pnoise1( float x, int px );
extern float ledm_pnoise2( float x, float y, int px, int py );
extern float ledm_pnoise3( float x, float y, float z, int px, int py, int pz );
extern float ledm_pnoise4( float x, float y, float z, float w, int px, int py, int pz, int pw );




/*
 * Adapted from hsv2rgb.cpp from the FastLED library!
 *
 */
struct ledm_rgba ledm_hsv2rgb_spectrum(uint hue, uint sat, uint val);
struct ledm_rgba ledm_hsv2rgb_rainbow(uint hue, uint sat, uint val);




// Timing utilities - implemented in util_time.c

unsigned long ledm_time_us();

uint ledm_time_ms();

void ledm_sleep_us(uint dt);


#endif // LIBLEDM_UTILS_H
