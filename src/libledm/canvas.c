#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "libledm_internal.h"



struct ledm_rgb* ledm_get_color_buffer(struct ledm_state* state) {
	return state->led_colors;
};

int ledm_canvas_add_led(struct ledm_state *state, float x, float y) {

	struct ledm_led *p1 = realloc(state->leds, (state->num_leds+1)*sizeof(struct ledm_led));
	struct ledm_rgb *p2 = realloc(state->led_colors, (state->num_leds+1)*sizeof(struct ledm_rgb));

	if (!p1 || !p2) {
		ledm_log(state, ERROR, "Failed to allocate space for LED.");
		return LEDM_OUT_OF_MEMORY;
	}

	++state->num_leds;
	state->leds = p1;
	state->led_colors = p2;
	state->leds[state->num_leds-1].x = x;
	state->leds[state->num_leds-1].y = y;
	state->leds[state->num_leds-1].z = 0;
	state->leds[state->num_leds-1].w = 0;
	state->led_colors[state->num_leds-1] = (struct ledm_rgb){0,0,0};

	return LEDM_SUCCESS;
};


int ledm_canvas_reset(struct ledm_state *state) {

	state->num_leds = 0;
	free(state->leds);
	free(state->led_colors);
	state->leds = NULL;
	state->led_colors = NULL;

	return LEDM_SUCCESS;
};


int ledm_canvas_set_layout(struct ledm_state *state, const char *name, struct ledm_config *cfg) {

	for (uint i=0; i<state->num_layouts; ++i) {
		if (!strcmp(state->layouts[i].name, name)) {
			ledm_log(state, DEBUG, "Setting layout to '%s'.", name);
			return state->layouts[i].layout(state, cfg);
		}
	}

	ledm_log(state, ERROR, "Failed to set layout: layout '%' unknown.");
	return LEDM_UNKNOWN_LAYOUT;
};


float ledm_canvas_get_xmin(struct ledm_state *state) {
	float xmin = INFINITY;
	for (uint i=0; i<state->num_leds; ++i)
		if (state->leds[i].x < xmin)
			xmin = state->leds[i].x;
	return xmin;
};


float ledm_canvas_get_xmax(struct ledm_state *state) {
	float xmax = -INFINITY;
	for (uint i=0; i<state->num_leds; ++i)
		if (state->leds[i].x > xmax)
			xmax = state->leds[i].x;
	return xmax;
};


float ledm_canvas_get_ymin(struct ledm_state *state) {
	float ymin = INFINITY;
	for (uint i=0; i<state->num_leds; ++i)
		if (state->leds[i].y < ymin)
			ymin = state->leds[i].y;
	return ymin;
};


float ledm_canvas_get_ymax(struct ledm_state *state) {
	float ymax = -INFINITY;
	for (uint i=0; i<state->num_leds; ++i)
		if (state->leds[i].y > ymax)
			ymax = state->leds[i].y;
	return ymax;
};


uint ledm_canvas_get_grid_width(struct ledm_state *state) {
	return state->grid_width;
};


uint ledm_canvas_get_grid_height(struct ledm_state *state) {
	return state->grid_height;
};


int ledm_canvas_grid_enabled(struct ledm_state *state) {
	return state->grid_trafo != NULL;
};


void ledm_canvas_set_grid(struct ledm_state *state, ledm_grid_trafo trafo, uint width, uint height) {
	state->grid_width = width;
	state->grid_height = height;
	state->grid_trafo = trafo;
};




