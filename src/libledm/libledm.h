#ifndef LIBLEDM_H
#define LIBLEDM_H

#include <stdint.h>



#define LIBLEDM_VERSION "0.0.1"

#define LEDM_SUCCESS               0
#define LEDM_FAILURE               1
#define LEDM_OUT_OF_MEMORY         2
#define LEDM_FILE_ERROR            3
#define LEDM_PARSE_FAILURE         20
#define LEDM_UNKNOWN               40
#define LEDM_UNKNOWN_PATTERN       41
#define LEDM_UNKNOWN_PAINTER       42
#define LEDM_UNKNOWN_PARTICLE      43
#define LEDM_UNKNOWN_LAYOUT        44
#define LEDM_NEEDS_GRID_LAYOUT     60



// Types

struct snd_seq_event;
union SDL_Event;
struct ledm_state;
struct ledm_config;

struct ledm_rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

struct ledm_rgba {
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};

typedef unsigned int uint;

union ledm_var {
	char *s;
	struct ledm_rgba c;
	double f;
	uint u;
};

enum ledm_severity {
	DEBUG,
	INFO,
	WARNING,
	ERROR,
	CRITICAL,
	MUTE
};

enum ledm_mix_mode {
	OVERWRITE,
	MULTIPLY,
	SCREEN,
	ON_BLACK
};

typedef void (*ledm_on_input)(struct ledm_state*, void *context, struct snd_seq_event *midi_event, union SDL_Event *sdl_event);

typedef void (*ledm_on_frame)(struct ledm_state*, void *context);

typedef int (*ledm_layout)(struct ledm_state*, struct ledm_config*);

typedef struct ledm_rgba (*ledm_painter)(union ledm_var *params, float x, float y);

typedef float (*ledm_pattern)(union ledm_var *params, float x, float y);

typedef uint (*ledm_grid_trafo)(uint x, uint y, uint width, uint height);




// Creating, loading, destroying and accessing ledm_config - implemented in config.c

struct ledm_config *ledm_config_create();

void ledm_config_destroy(struct ledm_config*);

void ledm_config_load_default(struct ledm_config*);

int ledm_config_load_file(struct ledm_config*, char *filename);

void ledm_config_print(struct ledm_config*);

char *ledm_config_get_path();

int ledm_config_set(struct ledm_config*, const char *key, const char *val);

int ledm_config_get_nth(struct ledm_config*, const char *key, int n, char **param);

char *ledm_config_get(struct ledm_config*, const char *key, const char *fallback);

int ledm_config_get_int(struct ledm_config*, const char *key, int fallback, struct ledm_state*);

float ledm_config_get_float(struct ledm_config*, const char *key, float fallback, struct ledm_state*);

int ledm_config_get_bool(struct ledm_config*, const char *key, int fallback, struct ledm_state*);

enum ledm_severity ledm_config_get_verbosity(struct ledm_config*);


// Initializing and deinitializing libledm - implemented in state.c

struct ledm_state *ledm_initialize(struct ledm_config*);

void ledm_destroy(struct ledm_state*);




// Canvas stuff - implemented in canvas.c

struct ledm_rgb* ledm_get_color_buffer(struct ledm_state*);

int ledm_canvas_add_led(struct ledm_state*, float x, float y);

int ledm_canvas_reset(struct ledm_state*);

int ledm_canvas_set_layout(struct ledm_state*, const char *name, struct ledm_config *cfg);

float ledm_canvas_get_xmin(struct ledm_state*);

float ledm_canvas_get_xmax(struct ledm_state*);

float ledm_canvas_get_ymin(struct ledm_state*);

float ledm_canvas_get_ymax(struct ledm_state*);

uint ledm_canvas_get_grid_width(struct ledm_state*);

uint ledm_canvas_get_grid_height(struct ledm_state*);

int ledm_canvas_grid_enabled(struct ledm_state*);

void ledm_canvas_set_grid(struct ledm_state*, ledm_grid_trafo trafo, uint width, uint height);



// Registering layouts, patterns, painters, particles - implemented in register.c

int ledm_register_layout(struct ledm_state*, const char *name, ledm_layout layout);

int ledm_register_painter(struct ledm_state*, const char *name, const char *format, ledm_painter painter);

int ledm_get_painter_number(struct ledm_state*, const char *painter);

const char *ledm_get_painter_format(struct ledm_state*, const char *painter);

int ledm_register_pattern(struct ledm_state*, const char *name, const char *format, ledm_pattern pattern);

int ledm_get_pattern_number(struct ledm_state*, const char *pattern);

const char *ledm_get_pattern_format(struct ledm_state*, const char *pattern);

// TODO ledm_register_particle_type




// Actual painting - implemented in paint.c

void ledm_mix_color(struct ledm_rgb *background, const struct ledm_rgba *foreground, enum ledm_mix_mode mode);

int ledm_paint_grid(struct ledm_state*, uint x, uint y, struct ledm_rgba color);

int ledm_paint(struct ledm_state*, int pattern, union ledm_var *pattern_params, int painter, union ledm_var *painter_params);

int ledm_paint_function(struct ledm_state*, void *context, struct ledm_rgba (*function)(void *context, float x, float y));

enum ledm_mix_mode ledm_paint_get_mode(struct ledm_state*);

void ledm_paint_set_mode(struct ledm_state*, enum ledm_mix_mode);




// TODO functions to manipulate particles




// Input and output - implemented in io.c

void ledm_io_poll(struct ledm_state*, void *context, ledm_on_input on_input);

void ledm_io_push(struct ledm_state*);

void ledm_io_wait_frame(struct ledm_state*);

void ledm_io_loop(struct ledm_state*, void *context, ledm_on_input on_input, ledm_on_frame on_frame);

void ledm_io_loop_quit(struct ledm_state*);

float ledm_io_get_fps(struct ledm_state*);

void ledm_io_set_fps(struct ledm_state*, float fps); // value <= 0 turns off frame rate limiting




// Error reporting and logging - implemented in log.h

void ledm_log(struct ledm_state*, enum ledm_severity, const char *format, ...);

void ledm_log_bare(struct ledm_state*, enum ledm_severity, const char *format, ...);





#endif // LIBLEDM_H
