#include <stdlib.h>
#include <alsa/asoundlib.h>

#include "libledm_internal.h"



int ledm_io_midi_init(struct ledm_state *state, struct ledm_config *cfg) {

	state->midi_sequencer = NULL;
	state->midi_port = 0;
	state->midi_channel = 0;

	if (!ledm_config_get_bool(cfg, "io_midi", 1, state))
		return LEDM_SUCCESS;

	// Open ALSA MIDI sequencer
	if (snd_seq_open(&(state->midi_sequencer), "default", SND_SEQ_OPEN_INPUT, 0) < 0) {
		ledm_log(state, ERROR, "Couldn't open ALSA MIDI sequencer!");
		state->midi_sequencer = NULL;
		return LEDM_FAILURE;
	}

	// Set client name
	snd_seq_set_client_name(state->midi_sequencer, "libledm");

	// Open ALSA MIDI port
	state->midi_port = snd_seq_create_simple_port(state->midi_sequencer, "input",
		SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE, SND_SEQ_PORT_TYPE_APPLICATION);
	if (state->midi_port < 0) {
		ledm_log(state, ERROR, "Couldn't create ALSA MIDI input port!");
		snd_seq_close(state->midi_sequencer);
		state->midi_port = 0;
		state->midi_sequencer = NULL;
		return LEDM_FAILURE;
	}


	// Attempt autoconnections
	char *client = NULL;
	for (int i=0; ledm_config_get_nth(cfg, "midi_autoconnect", i, &client) == LEDM_SUCCESS; ++i) {
		snd_seq_addr_t port;
		if (snd_seq_parse_address(state->midi_sequencer, &port, client) < 0
				|| snd_seq_connect_from(state->midi_sequencer, 0, port.client, port.port) < 0 ) {
			ledm_log(state, WARNING, "Couldn't autoconnect to ALSA MIDI port '%s'!", client);
		} else {
			ledm_log(state, WARNING, "Connected to ALSA MIDI port '%s'!", client);
		}
		free(client);
		client = NULL;
	}
	
	return LEDM_SUCCESS;
};


int ledm_io_midi_poll(struct ledm_state *state, void *context, ledm_on_input on_input) {
	if (!state->midi_sequencer)
		return LEDM_SUCCESS;

	// Poll all pending midi events
	snd_seq_event_t* alsa_event;
	while (snd_seq_event_input_pending(state->midi_sequencer, 1) > 0) {
		snd_seq_event_input(state->midi_sequencer, &alsa_event);
		if (on_input)
			on_input(state, context, alsa_event, NULL);
		snd_seq_free_event(alsa_event);
	}

	return LEDM_SUCCESS;
};


int ledm_io_midi_deinit(struct ledm_state *state) {

	if (state->midi_sequencer)
		snd_seq_close(state->midi_sequencer);

	state->midi_sequencer = NULL;
	state->midi_port = 0;
	state->midi_channel = 0;

	return LEDM_SUCCESS;
};


