#include <stdlib.h>

#include "libledm.h"



static uint layout_rectangle_trafo(uint x, uint y, uint width, uint height) {

	if (x%2 ==0)
		return y + 2*height*(x/2);
	else
		return -1 -y + 2*height*(1+x/2);
}


static int layout_rectangle(struct ledm_state *state, struct ledm_config *cfg) {

	int width = ledm_config_get_int(cfg, "hw_layout_rectangle_width", 25, state);
	int height = ledm_config_get_int(cfg, "hw_layout_rectangle_height", 16, state);

	ledm_canvas_set_grid(state, layout_rectangle_trafo, width, height);

// 	for (uint j = 0; j<height; ++j) {
// 		for (uint i = 0; i<width; ++i) {
// 			if (j%2 == 0)
// 				add_led(ctx->state, (float)i, (float)j);
// 			else
// 				add_led(ctx->state, (float)(width-1-i), (float)j);
// 
// 		}
// 	}
	for (int col = 0; col<width; ++col) {
		for (int row = 0; row<height; ++row) {
			if (col%2 == 0)
				ledm_canvas_add_led(state, (float)col, (float)row);
			else
				ledm_canvas_add_led(state, (float)col, (float)(height-1-row));

		}
	}

	return LEDM_SUCCESS;
}



int ledm_register_builtin_layouts(struct ledm_state *state) {

	return ledm_register_layout(state, "rectangle", layout_rectangle);
}
