#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include "libledm_utils.h"

unsigned long ledm_time_us() {

	static struct timeval *start = NULL;
	if (!start) {
		start = malloc(sizeof(struct timeval));
		gettimeofday(start, NULL);
	}

	struct timeval now;
	gettimeofday(&now, NULL);

	return ((now.tv_sec - start->tv_sec) * 1000000) + (now.tv_usec - start->tv_usec);
};

uint ledm_time_ms() {
	return (uint) (ledm_time_us()/1000);
}

void ledm_sleep_us(uint dt) {
	struct timespec ts;
	ts.tv_sec = dt / 1000000;
	ts.tv_nsec = (dt % 1000000) * 1000;
	nanosleep(&ts, NULL);
};
