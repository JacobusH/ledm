#include <math.h>
#include <stdio.h>

#include "libledm_internal.h"
#include "libledm_utils.h"


#define PI    3.14159265358979323846


static float blur(float x, float y, float radius) {

	// 1 if x<<y, 0 if x>>y, linear interpolation between y+-radius

	if (x >= y + radius)
		return 0.0;
	if (x <= y - radius)
		return 1.0;
	return 0.5 - (x-y)/(2*radius);
}

static float pattern_all(union ledm_var *params, float x, float y) {
	return 1;
}

static float pattern_allish(union ledm_var *params, float x, float y) {
	return params[0].f;
}

static float pattern_disc(union ledm_var *params, float x, float y) {
	float radius = params[0].f;
	float blur_radius = params[1].f;
	float circle_x = params[2].f;
	float circle_y = params[3].f;
	//printf("radius=%f,blur=%f,x=%f,y=%f\n",radius,blur_radius,circle_x,circle_y);

	return blur( sqrtf( (x-circle_x)*(x-circle_x) + (y-circle_y)*(y-circle_y)),  radius, blur_radius );
}

static float pattern_ripple(union ledm_var *params, float x, float y) {
	float x_0 = params[0].f;
	float y_0 = params[1].f;
	float scale = params[2].f;
	float time = params[3].f;
	//printf("radius=%f,blur=%f,x=%f,y=%f\n",radius,blur_radius,circle_x,circle_y);

	float distance = sqrtf( (x_0 - x)*(x_0 - x) + (y_0 - y)*(y_0 - y) );
	return fabs(sin(distance/scale - time) / ((distance/scale) + 1) );
}

static float pattern_ring_section(union ledm_var *params, float x, float y) {

	// TODO This is a work in progress!

	float radius_min = params[0].f;
	float radius_max = params[1].f;
	float radius_blur = params[2].f;
	float phi_offset = 2*PI*params[3].f;
	float phi_halfwidth = PI*params[4].f;
	float phi_blur = PI*params[5].f;
	float circle_x = params[6].f;
	float circle_y = params[7].f;

	float distance = sqrtf( (x-circle_x)*(x-circle_x) + (y-circle_y)*(y-circle_y) );
	float phi = fmod( fabs( atan2(x - circle_x, y - circle_y) + phi_offset), 2*PI);

	return blur(radius_min, distance, radius_blur)
		*  blur(distance, radius_max, radius_blur)
		*  blur(phi, phi_halfwidth, phi_blur);
}

static float pattern_rectangle(union ledm_var *params, float x, float y) {
	float xmin = params[0].f;
	float xmax = params[1].f;
	float ymin = params[2].f;
	float ymax = params[3].f;
	float rotation = params[4].f;

	float x0 = (xmax + xmin) / 2.0;
	float y0 = (ymax + ymin) / 2.0;
	float r_x = x - x0;
	float r_y = y - y0;

	x = r_x*cosf(rotation) + r_y * sinf(rotation) + x0;
	y = -r_x*sinf(rotation) + r_y * cosf(rotation) + y0;

	return (float) ( x >= xmin && x <= xmax && y >= ymin && y <= ymax );
}

static float pattern_lattice(union ledm_var *params, float x, float y) {
	float xmin = params[0].f;
	float xmax = params[1].f;
	float ymin = params[2].f;
	float ymax = params[3].f;
	float stride = params[4].f;
	float point_size = params[5].f;

	return (float) ( x >= xmin && x <= xmax && fabs(fmod(x-xmin, stride)) <= point_size &&
		y >= ymin && y <= ymax && fabs(fmod(y-ymin, stride)) <= point_size);
}

static float pattern_noise(union ledm_var *params, float x, float y) {
	float cutoff = params[0].f;
	float xoff = params[1].f;
	float yoff = params[2].f;
	float scale = params[3].f;
	float time = params[4].f;

	if (cutoff >=0.98)
		return 0;

	float noise = 0.5*(ledm_noise3(scale*(x-xoff),scale*(y-yoff), scale*time) + 1.0 );

	return (float)(noise > cutoff);
}



int ledm_register_builtin_patterns(struct ledm_state *state) {

	return
		ledm_register_pattern(state,  "all", "", pattern_all) ||
		ledm_register_pattern(state,  "allish", "f", pattern_allish) ||
		ledm_register_pattern(state,  "disc", "{radius=1,blur=0,x=0,y=0}", pattern_disc) ||
		ledm_register_pattern(state,  "ripple", "{x_0=0,y_0=0,scale=1,time=0}", pattern_ripple) ||
		ledm_register_pattern(state,  "ring-section", "ffffffff", pattern_ring_section) ||
		ledm_register_pattern(state,  "rectangle", "{xmin=0,xmax=0,ymin=0,ymax=0,rotation=0}", pattern_rectangle) ||
		ledm_register_pattern(state,  "lattice", "{xmin=0,xmax=0,ymin=0,ymax=0,stride=1,point_size=0.5}", pattern_lattice) ||
		ledm_register_pattern(state,  "noise", "{cutoff=0.5,xoff=0,yoff=0,scale=1,time=0}", pattern_noise);
};
