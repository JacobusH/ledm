#include <stdio.h>

#include "libledm_internal.h"
#include "libledm_utils.h"



void ledm_io_poll(struct ledm_state *state, void *context, ledm_on_input on_input) {

	ledm_io_sdl_poll(state, context, on_input);
	ledm_io_midi_poll(state, context, on_input);
};

void ledm_io_push(struct ledm_state *state) {

	// TODO: write to out buffer

	ledm_io_serial_push(state);
	ledm_io_opc_push(state);
	ledm_io_sdl_push(state);
};

void ledm_io_wait_frame(struct ledm_state *state) {

	static unsigned long last_frame = 0; // when the last frame was sent out
	unsigned long before_sleep = ledm_time_us();

	int usec_left_in_frame = last_frame + (1000000/state->fps) - before_sleep;

	// If framerate is limited, sleep until the next frame is due to be sent out
	if (state->fps != 0 && usec_left_in_frame > 0)
		ledm_sleep_us(usec_left_in_frame);

	// Old code to measure frame rate
	if (0) {
		unsigned long after_sleep = ledm_time_us();
		float fps = 1000000.0/(float)( after_sleep-last_frame );
		printf("FPS = %f, time to calculate frame = %lu usec", fps, before_sleep-last_frame);
		if (state->fps != 0)
			printf(", time left = %d usec", usec_left_in_frame);
		printf("\n");
	}

	last_frame = ledm_time_us();
};

void ledm_io_loop(struct ledm_state *state, void *context, ledm_on_input on_input, ledm_on_frame on_frame) {

	state->quit_io_loop = 0;

	while (!state->quit_io_loop) {

		// poll input
		ledm_io_poll(state, context, on_input);

		// call frame handler
		if (on_frame)
			on_frame(state, context);

		// sleep until the next frame is due to be sent out
		ledm_io_wait_frame(state);

		// push to output
		ledm_io_push(state);
	}
};

void ledm_io_loop_quit(struct ledm_state *state) {
	state->quit_io_loop = 1;
};

void ledm_io_set_fps(struct ledm_state *state, float fps) {
	state->fps = fps;
};

float ledm_io_get_fps(struct ledm_state *state) {
	return state->fps;
};



