#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "libledm_internal.h"




struct ledm_config *ledm_config_create() {

	struct ledm_config *cfg = malloc(sizeof(struct ledm_config));

	cfg->param_count = 0;
	cfg->param_key = NULL;
	cfg->param_val = NULL;

	return cfg;
};

void ledm_config_destroy(struct ledm_config *cfg) {

	for (int i=0; i<cfg->param_count; ++i) {
		free(cfg->param_key[i]);
		free(cfg->param_val[i]);
	}

	free(cfg);
};

void ledm_config_load_default(struct ledm_config *cfg) {

	char *config_folder = ledm_config_get_path();
	char *config_file = NULL;

	if (!config_folder)
		return;

	asprintf(&config_file, "%s/%s", config_folder, "config");

	ledm_config_load_file(cfg, config_file);
};


char *ledm_config_get_path() {

	char *config_path = NULL;
	char *config_home = getenv("XDG_CONFIG_HOME");
	if (config_home != NULL) {
		asprintf(&config_path, "%s/%s/", config_home, "ledm");
	} else {
		config_home = getenv("HOME");
		if (config_home != NULL) {
			asprintf(&config_path, "%s/%s/%s/", config_home, ".config", "ledm");
        }
	}

	// Create configuration folder if necessary
	if (config_path != NULL)
		mkdir(config_path, 0777);

	return config_path;
}

static void trim_whitespace(char *s) {
	int i, j;

	// Move i to first non-whitespace position
	for(i=0; s[i]==' ' || s[i]=='\t'; i++);

	// Shift rest of string to beginning
	for(j=0;s[i];i++)
		s[j++]=s[i];
	s[j] = '\0';

	// Terminate string at last non-whitespace
	for (j--; s[j]==' ' || s[j]=='\t'; j--)
		s[j] = '\0';
}

static int get_key_value_pair(FILE *fp, char **key, char **val) {

	char key_buffer[128] = "";
	char val_buffer[128] = "";
	*key = NULL;
	*val = NULL;

	int cnt = fscanf(fp, "%127[^=\n]=%127[^\n]%*[^\n]", key_buffer, val_buffer);

	if (cnt == 0 || cnt == EOF)
		return cnt;

	trim_whitespace(key_buffer);
	trim_whitespace(val_buffer);
	*key = strdup(key_buffer);
	*val = strdup(val_buffer);

	return cnt;
}

int ledm_config_load_file(struct ledm_config *cfg, char *filename) {

	FILE *fp = fopen(filename, "r");
	char *key = NULL;
	char *val = NULL;
	int cnt;

	if (fp == NULL)
		return 1;

	while ( (cnt = get_key_value_pair(fp, &key, &val)) != EOF ) {

		// Move to next line if nothing was matched
		if(cnt == 0) {
			char c;
			while ( (c = getc(fp)) != EOF && c != '\n') {};
			continue;
		}

		//printf("ledm_config_load_file: parsed cnt=%d key='%s' val='%s'\n", cnt, key, val);

		if (cnt == 2)
			ledm_config_set(cfg, key, val);
		
		free(key);
		free(val);
	}

	fclose(fp);
	return 0;
};

void ledm_config_print(struct ledm_config *cfg) {

	for (int i=0; i<cfg->param_count; ++i)
		if (cfg->param_val[i])
			printf("%s = %s\n", cfg->param_key[i], cfg->param_val[i]);
		else
			printf("%s\n", cfg->param_key[i]);
};


int ledm_config_set(struct ledm_config *cfg, const char *key, const char *val) {
	if (!key)
		return LEDM_FAILURE;

	cfg->param_count++;
	cfg->param_key = realloc(cfg->param_key, cfg->param_count*sizeof(char*));
	cfg->param_val = realloc(cfg->param_val, cfg->param_count*sizeof(char*));
	cfg->param_key[cfg->param_count-1] = strdup(key);
	if (val)
		cfg->param_val[cfg->param_count-1] = strdup(val);
	else
		cfg->param_val[cfg->param_count-1] = NULL;
	return LEDM_SUCCESS;
};


static int get_nth_index(struct ledm_config *cfg, const char *key, int n) {

	int start, sign;

	if (n >= 0) {
		start = 0;
		sign = 1;
	} else {
		start = cfg->param_count-1;
		sign = -1;
		n = -n-1;
	}

	for (int i=0; i<cfg->param_count; ++i)
		if (!strcmp(cfg->param_key[start + sign*i], key))
			if ((n--) == 0)
				return start+sign*i;

	return -1;
};

static int string_to_int(const char *str, int *target) {
	if (str)
		*target = (int) strtol(str, NULL, 10);
	else
		return LEDM_FAILURE;
	return LEDM_SUCCESS;
};

static int string_to_float(const char *str, float *target) {
	if (str)
		*target = strtof(str, NULL);
	else
		return LEDM_FAILURE;
	return LEDM_SUCCESS;
};

static int string_to_bool(const char *str, int *target) {
	if (str)
		*target = (
			!strcmp(str, "") ||
			!strcmp(str, "on") ||
			!strcmp(str, "true") ||
			!strcmp(str, "1") ||
			!strcmp(str, "enabled") );
	else
		*target = 0;
	return LEDM_SUCCESS;
};


int ledm_config_get_nth(struct ledm_config *cfg, const char *key, int n, char **param) {
	int index = get_nth_index(cfg, key, n);
	if (index < 0)
		return LEDM_FAILURE;
	free(*param);
	if (cfg->param_val[index] 
			&& strcmp(cfg->param_val[index], "disabled")
			&& strcmp(cfg->param_val[index], "off")
			&& strcmp(cfg->param_val[index], "false")
			&& strcmp(cfg->param_val[index], "0")
			&& strcmp(cfg->param_val[index], "null")
	   )
		*param = strdup(cfg->param_val[index]);
	else
		*param = NULL;
	return LEDM_SUCCESS;
};

char *ledm_config_get(struct ledm_config *cfg, const char *key, const char *fallback) {
	char *val = NULL;
	ledm_config_get_nth(cfg, key, -1, &val);
	if (!val && fallback)
		val = strdup(fallback);
	return val;
};

int ledm_config_get_int(struct ledm_config *cfg, const char *key, int fallback, struct ledm_state *state) {
	int val = fallback;
	char *str = NULL;
	if (ledm_config_get_nth(cfg, key, -1, &str) == LEDM_SUCCESS) {
		if (string_to_int(str, &val) == LEDM_FAILURE)
			ledm_log(state, ERROR, "Failed to parse value for key '%s', falling back to default '%d'.", key, val);
		free(str);
	}
	return val;
};

float ledm_config_get_float(struct ledm_config *cfg, const char *key, float fallback, struct ledm_state *state) {
	float val = fallback;
	char *str = NULL;
	if (ledm_config_get_nth(cfg, key, -1, &str) == LEDM_SUCCESS) {
		if (string_to_float(str, &val) == LEDM_FAILURE)
			ledm_log(state, ERROR, "Failed to parse value for key '%s', falling back to default '%f'.", key, val);
		free(str);
	}
	return val;
};

int ledm_config_get_bool(struct ledm_config *cfg, const char *key, int fallback, struct ledm_state *state) {
	int val = fallback;
	char *str = NULL;
	if (ledm_config_get_nth(cfg, key, -1, &str) == LEDM_SUCCESS) {
		if (string_to_bool(str, &val) == LEDM_FAILURE) {
			if (val)
				ledm_log(state, ERROR, "Failed to parse value for key '%s', falling back to default 'true'.", key);
			else
				ledm_log(state, ERROR, "Failed to parse value for key '%s', falling back to default 'false'.", key);
		}
		free(str);
	}
	return val;
};

enum ledm_severity ledm_config_get_verbosity(struct ledm_config* cfg) {
	char *verb = ledm_config_get(cfg, "verbosity", "warning");
	if (!strcmp(verb, "debug"))
		return DEBUG;
	else if (!strcmp(verb, "info"))
		return INFO;
	else if (!strcmp(verb, "warning"))
		return WARNING;
	else if (!strcmp(verb, "error"))
		return ERROR;
	else if (!strcmp(verb, "critical"))
		return CRITICAL;
	else if (!strcmp(verb, "mute"))
		return MUTE;
	else {
		return WARNING;
	}
	free(verb);
};

