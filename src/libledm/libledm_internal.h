#ifndef LIBLEDM_INTERNAL_H
#define LIBLEDM_INTERNAL_H

#include "libledm.h"


struct ledm_led {
	float x;
	float y;
	float z;
	float w;
};

struct ledm_layout_entry {
	char *name;
	ledm_layout layout;
};

struct ledm_painter_entry {
	char *name;
	char *format;
	ledm_painter painter;
};

struct ledm_pattern_entry {
	char *name;
	char *format;
	ledm_pattern pattern;
};

struct ledm_particle_entry {
	char *name;
	int placeholder;
};


struct ledm_state {

	enum ledm_severity verbosity;
	enum ledm_mix_mode mix_mode;
	float fps;
	int quit_io_loop;
	int io_measure_fps;

	// LED buffers
	struct ledm_led *leds;
	struct ledm_rgb *led_colors;
	uint num_leds;


	// Canvas and grid geometry
	uint grid_width;
	uint grid_height;
	ledm_grid_trafo grid_trafo;
	// TODO some rotation/orientation state


	// Registry of drawing tools
	struct ledm_painter_entry *painters;
	uint num_painters;
	struct ledm_pattern_entry *patterns;
	uint num_patterns;
	struct ledm_particle_entry *particles;
	uint num_particles;
	struct ledm_layout_entry *layouts;
	uint num_layouts;


	// Serial output
	char *serial_path;
	int serial_baudrate;
	int serial_socket;


	// Open Pixel Control output
	char *opc_host;
	int8_t opc_out;


	// SDL input and output
	struct SDL_Renderer* sdl_renderer;
	struct SDL_Window* sdl_window;
	int sdl_window_margin;
	int sdl_led_size;
	int sdl_window_width;
	int sdl_window_height;
	int sdl_window_offset_x;
	int sdl_window_offset_y;
	float sdl_scale;


	// MIDI input
	struct _snd_seq* midi_sequencer;
	int midi_port;
	int midi_channel;
};

struct ledm_config {
	int    param_count;
	char **param_key;
	char **param_val;
};



// IO modules - implemented in serial.c, opc.c, sdl.c, midi.c

int ledm_io_serial_init(struct ledm_state*, struct ledm_config*);

int ledm_io_serial_push(struct ledm_state*);

int ledm_io_serial_deinit(struct ledm_state*);


int ledm_io_opc_init(struct ledm_state*, struct ledm_config*);

int ledm_io_opc_push(struct ledm_state*);

int ledm_io_opc_deinit(struct ledm_state*);


int ledm_io_sdl_init(struct ledm_state*, struct ledm_config*);

int ledm_io_sdl_poll(struct ledm_state*, void *context, ledm_on_input on_input);

int ledm_io_sdl_push(struct ledm_state*);

int ledm_io_sdl_deinit(struct ledm_state*);


int ledm_io_midi_init(struct ledm_state*, struct ledm_config*);

int ledm_io_midi_poll(struct ledm_state*, void *context, ledm_on_input on_input);

int ledm_io_midi_deinit(struct ledm_state*);



// Registration of builtin objects of type <X> - implemented in <X>.c

int ledm_register_builtin_painters(struct ledm_state*);

int ledm_register_builtin_patterns(struct ledm_state*);

int ledm_register_builtin_particles(struct ledm_state*);

int ledm_register_builtin_layouts(struct ledm_state*);


#endif // LIBLEDM_INTERNAL_H
