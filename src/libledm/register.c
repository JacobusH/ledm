#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <string.h>

#include "libledm_internal.h"

int ledm_register_layout(struct ledm_state *state, const char *name, ledm_layout layout) {

	struct ledm_layout_entry *p = realloc(state->layouts, (state->num_layouts+1)*sizeof(struct ledm_layout_entry));

	if (!p) {
		ledm_log(state, ERROR, "Failed to allocate space for layout register.");
		return LEDM_OUT_OF_MEMORY;
	}

	ledm_log(state, DEBUG, "Added layout '%s' to register.", name);
	++state->num_layouts;
	state->layouts = p;
	state->layouts[state->num_layouts-1].name = strdup(name);
	state->layouts[state->num_layouts-1].layout = layout;

	return LEDM_SUCCESS;
};

int ledm_register_painter(struct ledm_state *state, const char *name, const char *format, ledm_painter painter) {

	struct ledm_painter_entry *p = realloc(state->painters, (state->num_painters+1)*sizeof(struct ledm_painter_entry));

	if (!p) {
		ledm_log(state, ERROR, "Failed to allocate space for painter register.");
		return LEDM_OUT_OF_MEMORY;
	}

	ledm_log(state, DEBUG, "Added painter '%s' to register.", name);
	++state->num_painters;
	state->painters = p;
	state->painters[state->num_painters-1].name = strdup(name);
	state->painters[state->num_painters-1].format = strdup(format);
	state->painters[state->num_painters-1].painter = painter;

	return LEDM_SUCCESS;
};

int ledm_get_painter_number(struct ledm_state *state, const char *painter) {
	if (!painter)
		return -1;
	for (uint i=0; i<state->num_painters; ++i)
		if (!strcmp(painter, state->painters[i].name))
			return i;
	return -1;
};

const char *ledm_get_painter_format(struct ledm_state *state, const char *painter) {
	int i = ledm_get_painter_number(state, painter);
	if (i < 0)
		return NULL;
	else
		return state->painters[i].format;
};

int ledm_register_pattern(struct ledm_state *state, const char *name, const char *format, ledm_pattern pattern) {

	struct ledm_pattern_entry *p = realloc(state->patterns, (state->num_patterns+1)*sizeof(struct ledm_pattern_entry));

	if (!p) {
		ledm_log(state, ERROR, "Failed to allocate space for pattern register.");
		return LEDM_OUT_OF_MEMORY;
	}

	ledm_log(state, DEBUG, "Added pattern '%s' to register.", name);
	++state->num_patterns;
	state->patterns = p;
	state->patterns[state->num_patterns-1].name = strdup(name);
	state->patterns[state->num_patterns-1].format = strdup(format);
	state->patterns[state->num_patterns-1].pattern = pattern;

	return LEDM_SUCCESS;
};

int ledm_get_pattern_number(struct ledm_state *state, const char *pattern) {
	if (!pattern)
		return -1;
	for (uint i=0; i<state->num_patterns; ++i)
		if (!strcmp(pattern, state->patterns[i].name))
			return i;
	return -1;
};

const char *ledm_get_pattern_format(struct ledm_state *state, const char *pattern) {
	int i = ledm_get_pattern_number(state, pattern);
	if (i < 0)
		return NULL;
	else
		return state->patterns[i].format;
};
