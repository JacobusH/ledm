#include "libledm_internal.h"


void ledm_mix_color(struct ledm_rgb *b, const struct ledm_rgba *f, enum ledm_mix_mode mode) {

	switch (mode) {

	case OVERWRITE:
		b->r = (uint8_t) ( b->r * (1-f->a/255.0f) + f->r * f->a/255.0f );
		b->g = (uint8_t) ( b->g * (1-f->a/255.0f) + f->g * f->a/255.0f );
		b->b = (uint8_t) ( b->b * (1-f->a/255.0f) + f->b * f->a/255.0f );
		break;

	case MULTIPLY: // TODO
		b->r = (uint8_t) ( ((int)b->r * (int)f->r)/255 );
		b->g = (uint8_t) ( ((int)b->g * (int)f->g)/255 );
		b->b = (uint8_t) ( ((int)b->b * (int)f->b)/255 );
		break;

	case SCREEN:
		b->r = (uint8_t) (  b->r +  (f->a/255.0f) * f->r * (1 - b->r/255.0f)  );
		b->g = (uint8_t) (  b->g +  (f->a/255.0f) * f->g * (1 - b->g/255.0f)  );
		b->b = (uint8_t) (  b->b +  (f->a/255.0f) * f->b * (1 - b->b/255.0f)  );
		break;

	case ON_BLACK:
		if (b->r == 0 && b->g == 0 && b->b == 0) {
			b->r = (uint8_t) ( (f->r*f->a)/255.0f );
			b->g = (uint8_t) ( (f->g*f->a)/255.0f );
			b->b = (uint8_t) ( (f->b*f->a)/255.0f );
		}
		break;
	}
};

int ledm_paint_grid(struct ledm_state *state, uint x, uint y, struct ledm_rgba color) {

	if (!state->grid_trafo){
		ledm_log(state, ERROR, "Tried to use grid painting without a set grid");
		return LEDM_NEEDS_GRID_LAYOUT;
	}

	struct ledm_rgb *led = & state->led_colors[state->grid_trafo(x,y,state->grid_width,state->grid_height)];

	ledm_mix_color(led, &color, state->mix_mode);

	return LEDM_SUCCESS;
}

int ledm_paint(struct ledm_state *state, int pattern, union ledm_var *pattern_params, int painter, union ledm_var *painter_params) {

	struct ledm_pattern_entry *pattern_entry = state->patterns + pattern;
	struct ledm_painter_entry *painter_entry = state->painters + painter;

	for (uint i=0; i<state->num_leds; ++i) {
		struct ledm_led *l = state->leds + i;
		struct ledm_rgb *c = state->led_colors + i;

		float opacity = pattern_entry->pattern(pattern_params, l->x, l->y);
		if (opacity > 0.05) {
			struct ledm_rgba f = painter_entry->painter(painter_params, l->x, l->y);
			f.a = (uint8_t) (opacity*f.a);
			ledm_mix_color(c, &f, state->mix_mode);
		}
	}

	return LEDM_SUCCESS;
};

int ledm_paint_function(struct ledm_state *state, void *context, struct ledm_rgba (*function)(void *context, float x, float y)) {

	for (uint i=0; i<state->num_leds; ++i) {
		struct ledm_led *l = state->leds + i;
		struct ledm_rgb *c = state->led_colors + i;
		struct ledm_rgba f = function(context, l->x, l->y);
		ledm_mix_color(c, &f, state->mix_mode);
	}

	return LEDM_SUCCESS;
};

enum ledm_mix_mode ledm_paint_get_mode(struct ledm_state *state) {
	return state->mix_mode;
};

void ledm_paint_set_mode(struct ledm_state *state, enum ledm_mix_mode mode) {
	state->mix_mode = mode;
};

