#include "FastLED.h"

// source:
// https://github.com/FastLED/FastLED/wiki/Interrupt-problems

#define NUM_LEDS 400
#define DATA_PIN 7

CRGB leds[NUM_LEDS];

const uint8_t header[4] = { 0xDE, 0xAD, 0xBE, 0xEF };

void setup() {
	FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);

	Serial.begin(115200);
	//Serial.begin(2500000);
	leds[0].r = 1;
	leds[0].g = 1;
	leds[0].b = 1;
	leds[1].r = 1;
	leds[1].g = 2;
	leds[1].b = 2;
	leds[2].r = 2;
	leds[2].g = 3;
	leds[2].b = 2;
	leds[3].r = 1;
	leds[3].g = 3;
	leds[3].b = 1;
	FastLED.show();
}

void loop() {
	// we're going to read led data directly from serial, after we get our header
	while(true) {
		while(Serial.available() == 0){} // wait for new serial data
		uint8_t b = Serial.read();
		bool looksLikeHeader = false;
		if(b == header[0]) {
			looksLikeHeader = true;
			for(int i = 1; looksLikeHeader && (i < sizeof(header)); i++) {
				while(Serial.available() == 0){} // wait for new serial data
				b = Serial.read();
				if(b != header[i]) {
					// whoops, not a match, this no longer looks like a header.
					looksLikeHeader = false;
				}
			}
		}

		if(looksLikeHeader) {
			uint16_t incoming_LEDs = 0;
			Serial.readBytes((uint8_t*)&incoming_LEDs, 2);
			// hey, we read all the header bytes!  Yay!  Now read the frame data
			int bytesRead = 0;
			while(bytesRead < (incoming_LEDs *3)) {
				//while(Serial.available() == 0){} // wait for new serial data
				//leds[bytesRead] = Serial.read();
				//bytesRead++;
				bytesRead += Serial.readBytes(((uint8_t*)leds) + bytesRead, (incoming_LEDs*3)-bytesRead);
			}
			// all bytes are read into led buffer. Now we can break out of while loop
			break;
		}
	}

	// now show the led data
	FastLED.show();
	//Serial.println('R');

	// finally, flush out any data in the serial buffer, as it may have been interrupted oddly by writing out led data:
	while(Serial.available() > 0) { Serial.read(); }

}

