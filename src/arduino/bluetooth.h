#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include <bluefruit.h>

// BLE Service
extern BLEDis  bledis;
extern BLEUart bleuart;

extern analyze_data_t analyze_data;

extern cmd_t subcommands_analyze[];

void setup();
void startAdv();
void connect_callback(uint16_t conn_handle);

#endif