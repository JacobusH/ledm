#include <stdlib.h>
#include <stdio.h>

#include "libledm.h"
#include "libledm_utils.h"




int main() {

	char *path = ledm_config_get_path();
	printf("libledm config path: %s\n", path);

	struct ledm_config *cfg = ledm_config_create();
	ledm_config_load_default(cfg);
	puts("libledm configuration:");
	ledm_config_print(cfg);


	struct ledm_state *state = ledm_initialize(cfg);
	ledm_sleep_us(50000);
	ledm_io_poll(state, NULL, NULL);

	ledm_paint_grid(state, 5, 5, (struct ledm_rgba){200,200,0,255});
	ledm_io_poll(state, NULL, NULL);
	ledm_io_push(state);

	ledm_paint_grid(state, 5, 5, (struct ledm_rgba){0,200,0,255});
	ledm_sleep_us(500000);
	ledm_io_poll(state, NULL, NULL);
	ledm_io_push(state);

	ledm_paint_grid(state, 5, 5, (struct ledm_rgba){200,0,0,255});
	ledm_sleep_us(500000);
	ledm_io_poll(state, NULL, NULL);
	ledm_io_push(state);

	ledm_paint_grid(state, 5, 5, (struct ledm_rgba){0,200,0,255});
	ledm_sleep_us(500000);
	ledm_io_poll(state, NULL, NULL);
	ledm_io_push(state);

	ledm_paint_grid(state, 5, 5, (struct ledm_rgba){200,0,0,255});
	ledm_sleep_us(500000);
	ledm_io_poll(state, NULL, NULL);
	ledm_io_push(state);



	ledm_destroy(state);
	ledm_config_destroy(cfg);
	free(path);
	return 0;
}






