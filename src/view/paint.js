var canvas = document.getElementById('tutorial');
var ctx = canvas.getContext('2d');
var paintStruc = {"bytesPerLight": 3, "spaceBetween": 10
    , "rowLen": 16, "colLen": 25, "width": 10, "height": 10};
// paintStructure = {(rgb), px, # lights, # lights, x-px, y-px}
var addrPattern;

canvas.width = paintStruc.spaceBetween + paintStruc.rowLen * (paintStruc.width + paintStruc.spaceBetween);
canvas.height = paintStruc.spaceBetween + paintStruc.colLen * (paintStruc.height + paintStruc.spaceBetween);

function paint(data) {
    // init the addressable pattern 
    if(!addrPattern) {
        makeAddrPattern();
    }

    // get the 1200 bytes of data from socket
    var paintBuffer = [];
    var dv = new DataView(data);
    for(var i = 0; i < data.byteLength; i+=paintStruc.bytesPerLight) {
        var r, g, b; 
        r = dv.getUint8(i);
        g = dv.getUint8(i+1);
        b = dv.getUint8(i+2);
        paintBuffer[i]   = r;
        paintBuffer[i+1] = g;
        paintBuffer[i+2] = b;
    }

    // snake pattern
    var curAddr = 0;
    for(var i = 0; i < paintStruc.rowLen; i++) {
        for(var j = 0; j < paintStruc.colLen; j++) {
            if(i % 2 == 0) {
                this.addrPattern[i][j] = curAddr++;
                paintRec(paintBuffer[0], paintBuffer[1], paintBuffer[2], i, j);
            }
            else {
                this.addrPattern[i][paintStruc.colLen - j] = curAddr++;
                paintRec(paintBuffer[0], paintBuffer[1], paintBuffer[2], i, paintStruc.colLen - j);
            }
        }
    }

    // console.log("buffer", paintBuffer[0], paintBuffer[1], paintBuffer[2]);
    // for(var i = 0; i < paintStruc.rowLen; i++) {
    //     for(var j = 0; j < paintStruc.colLen; j++) {
    //         paintRec(paintBuffer[0], paintBuffer[1], paintBuffer[2], i, j);
    //     }
    // }
}

function makeAddrPattern() {
    addrPattern = new Array(paintStruc.rowLen); 
    for (var i = 0; i < paintStruc.colLen; i++) { 
        addrPattern[i] = new Array(paintStruc.rowLen); 
    } 
  
    // snake pattern
    var curAddr = 0;
    for(var i = 0; i < paintStruc.rowLen; i++) {
        for(var j = 0; j < paintStruc.colLen; j++) {
            if(i % 2 == 0) {
                this.addrPattern[i][j] = curAddr++;
            }
            else {
                this.addrPattern[i][paintStruc.colLen - j] = curAddr++;
            }
        }
    }
    console.log(this.addrPattern)
}

function paintRec(r, g, b, rowIdx, colIdx) {
    var w = paintStruc.width;
    var h = paintStruc.height;
    var spaceBetween = paintStruc.spaceBetween;
    var x = spaceBetween + rowIdx * (w + spaceBetween);
    var y = spaceBetween + colIdx * (h + spaceBetween);

    ctx.beginPath();
    ctx.rect(x, y, w, h);
    // ctx.fillStyle = "red";
    // ctx.fillStyle = `rgb(${r}, ${g}, ${b})`;
    // ctx.fillStyle = "rgb("+r+","+g+","+b+")";
    // ctx.fillStyle = "rgba(0, 98, 255, 1)";
    ctx.fillStyle = `rgba(${r}, ${g}, ${b}, 1)`;;
    ctx.fill();
    // console.log('rbg', ctx.fillStyle);
}

function drawTemplate(type) {
    if(type === "square") {
        var rectangle = new Path2D();
        rectangle.rect(10, 10, 50, 50);
    }
    else if(type == "circle") {
        var circle = new Path2D();
        circle.moveTo(125, 35);
        circle.arc(100, 35, 25, 0, 2 * Math.PI);
    }
    else if(type == "heart") {
        ctx.beginPath();
        ctx.moveTo(75, 40);
        ctx.bezierCurveTo(75, 37, 70, 25, 50, 25);
        ctx.bezierCurveTo(20, 25, 20, 62.5, 20, 62.5);
        ctx.bezierCurveTo(20, 80, 40, 102, 75, 120);
        ctx.bezierCurveTo(110, 102, 130, 80, 130, 62.5);
        ctx.bezierCurveTo(130, 62.5, 130, 25, 100, 25);
        ctx.bezierCurveTo(85, 25, 75, 37, 75, 40);
        ctx.fill();
    }
    else {

    }

    var circle = new Path2D();
    circle.moveTo(125, 35);
    circle.arc(100, 35, 25, 0, 2 * Math.PI);

    ctx.stroke(rectangle);
    ctx.fill(circle);
}

function draw() {
    var canvas = document.getElementById('tutorial');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');

        // ctx.fillStyle = 'rgb(200, 0, 0)';
        // ctx.fillRect(10, 10, 50, 50); // x y width height
        // ctx.fillStyle = 'rgba(0, 0, 200, 0.5)';
        // ctx.fillRect(30, 30, 50, 50);

        simpleDrawRecs(ctx);
        simpleDrawTriangle(ctx);
        simpleDrawSmiley(ctx);
      }
  }

  function simpleDrawRecs(ctx) {
    ctx.fillRect(25, 25, 100, 100);
    ctx.clearRect(45, 45, 60, 60);
    ctx.strokeRect(50, 50, 50, 50);
    ctx.strokeRect(70, 70, 10, 10);
  }

  function simpleDrawTriangle(ctx) {
    ctx.beginPath();
    ctx.moveTo(75, 50);
    ctx.lineTo(100, 75);
    ctx.lineTo(100, 25);
    // ctx.fill();
    ctx.closePath();
    ctx.stroke();
  }

  function simpleDrawSmiley(ctx) {
    ctx.beginPath();
    ctx.arc(75, 75, 50, 0, Math.PI * 2, true); // Outer circle
    ctx.moveTo(110, 75);
    ctx.arc(75, 75, 35, 0, Math.PI, false);  // Mouth (clockwise)
    ctx.moveTo(65, 65);
    ctx.arc(60, 65, 5, 0, Math.PI * 2, true);  // Left eye
    ctx.moveTo(95, 65);
    ctx.arc(90, 65, 5, 0, Math.PI * 2, true);  // Right eye
    ctx.stroke();
  }