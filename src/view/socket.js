function createSocket() {

    // Get references to elements on the page.
    var form = document.getElementById('message-form');
    var messageField = document.getElementById('message');
    var messagesList = document.getElementById('messages');
    var socketStatus = document.getElementById('status');
    var closeBtn = document.getElementById('close');
  
    // Create a new WebSocket.
    // var socket = new WebSocket('ws://echo.websocket.org');
    var socket = new WebSocket('ws://localhost:8000', ['binary', 'base64']);
    socket.binaryType = 'arraybuffer'; // blob | arraybuffer
  
    // Handle any errors that occur.
    socket.onerror = function(error) {
      console.log('WebSocket Error: ', error);
    };
  
    // Show a connected message when the WebSocket is opened.
    socket.onopen = function(event) {
      socketStatus.innerHTML = 'Connected to: ' + event.currentTarget.URL;
      socketStatus.className = 'open';
    };
  
  
    // Handle messages sent by the server.
    socket.onmessage = function(event) {
        // just paint the buffer immediately?
        paint(event.data);

        // recTxt(event);
    };
  
  
    // Show a disconnected message when the WebSocket is closed.
    socket.onclose = function(event) {
      socketStatus.innerHTML = 'Disconnected from WebSocket.';
      socketStatus.className = 'closed';
    };
  
  
    // Send a message when the form is submitted.
    form.onsubmit = function(e) {
      e.preventDefault();
  
      // Retrieve the message from the textarea.
      var message = messageField.value;
  
      // Send the message through the WebSocket.
      socket.send(message);
  
      // Add the message to the messages list.
      messagesList.innerHTML += '<li class="sent"><span>Sent:</span>' + message +
                                '</li>';
  
      // Clear out the message field.
      messageField.value = '';
  
      return false;
    };
  
  
    // Close the WebSocket connection when the close button is clicked.
    closeBtn.onclick = function(e) {
      e.preventDefault();
  
      // Close the WebSocket.
      socket.close();
  
      return false;
    };
  
  };
  
  
  function recTxt(event) {
      var message = event.data;
      messagesList.innerHTML += '<li class="received"><span>Received:</span>' +
                                 message + '</li>';
  }
  
  function recBinary(event) {
      // 2      U16      framebuffer-width
      // 2      U16      framebuffer-height
      // 16 PIXEL_FORMAT server-pixel-format
      // 4      U32      name-length
  
      var data = event.data;
      var dv = new DataView(data);
      var width = dv.getUint16(0);
      var height = dv.getUint16(2); // parameter is byte offset from start of buffer
      var format = getPixelFormat(dv);
      var len = dv.getUint32(20);
      console.log('We have width: ' + width +
          'px, height: ' +
          height + 'px, name length: ' + len);
  }
  
  function getEndianness() {
      var a = new ArrayBuffer(4);
      var b = new Uint8Array(a);
      var c = new Uint32Array(a);
      b[0] = 0xa1;
      b[1] = 0xb2;
      b[2] = 0xc3;
      b[3] = 0xd4;
      if (c[0] === 0xd4c3b2a1) {
        return BlobReader.ENDIANNESS.LITTLE_ENDIAN;
      }
      if (c[0] === 0xa1b2c3d4) {
        return BlobReader.ENDIANNESS.BIG_ENDIAN;
      } else {
        throw new Error('Unrecognized endianness');
      }
    }
    
  
  
  
  // //// Using dataview and array buffer
  // // create an ArrayBuffer with a size in bytes
  // var buffer = new ArrayBuffer(16);
  
  // // Create a couple of views
  // var view1 = new DataView(buffer);
  // var view2 = new DataView(buffer,12,4); //from byte 12 for the next 4 bytes
  // view1.setInt8(12, 42); // put 42 in slot 12
  
  // console.log(view2.getInt8(0));
  // // expected output: 42