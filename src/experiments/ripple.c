#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <alsa/asoundlib.h>
#include "ledm.h"
#include "util.h"



#define MAX_FINGERS 10


const char* ledm_version = VERSION;

struct finger {
	SDL_FingerID id;
	float x;
	float y;
	float hue;
};

struct oscillator {
	float a;
	float v_a;
	float b;
	float v_b;
};

float f_a = 5;
float c_a = 8;
float d_a = 0.9;

float f_b = 5;
float c_b = 8;
float d_b = 0.3;


struct oscillator *lattice = NULL;

struct finger fingers[MAX_FINGERS];
uint number_fingers = 0;

float dt = 50;
int width;
int height;


void updateFingerPos(struct context *ctx, SDL_FingerID id, float x, float y) {

	uint match = number_fingers;
	for (uint i=0; i<number_fingers; ++i)
		if (fingers[i].id == id)
			match = i;

	if (match == number_fingers) {
		if (number_fingers < MAX_FINGERS) {
			++number_fingers;
			fingers[match].id = id;
			fingers[match].hue = 0.01 * (float)(rand() % 100);
		} else {
			return;
		}
	}

	fingers[match].x = x;
	fingers[match].y = y;

}

void removeFinger(SDL_FingerID id) {

	uint next_index = 0;
	for (uint i=0; i<number_fingers; ++i) {

		// if this particle is dead, move on
		if (fingers[i].id == id)
			continue;

		if (next_index != i)
			fingers[next_index] = fingers[i];

		next_index++;
	}

	number_fingers = next_index;
}



void on_input(struct context *ctx, struct snd_seq_event *midi_event, union SDL_Event *sdl_event) {

	if (sdl_event && sdl_event->type == SDL_FINGERDOWN) {
		updateFingerPos(ctx, sdl_event->tfinger.fingerId, sdl_event->tfinger.x, sdl_event->tfinger.y);
	} else if (sdl_event && sdl_event->type == SDL_FINGERMOTION) {
		updateFingerPos(ctx, sdl_event->tfinger.fingerId, sdl_event->tfinger.x, sdl_event->tfinger.y);
	} else if (sdl_event && sdl_event->type == SDL_FINGERUP) {
		removeFinger(sdl_event->tfinger.fingerId);
	}


}

struct color get_osc_color(int x, int y) {
	struct oscillator *osc = lattice + x + width * y;

	return hsv2rgb_spectrum((int) 255*(osc->b), (int) 150, (int) 255*fabs(osc->a) );
}

enum dir {NORTH, EAST, SOUTH, WEST};
struct oscillator *get_neighbour(int x, int y, enum dir dir) {
	int rx = x;
	int ry = y;
	switch (dir) {
		case NORTH: ry = (height+ry+1)%height; break;
		case EAST:  rx = (width+rx+1)%width;  break;
		case SOUTH: ry = (height+ry-1)%height; break;
		case WEST:  rx = (width+rx-1)%width;  break;
	}
	//printf("%3d|%3d -> %3d|%3d \n",x,y,rx,ry);
	return lattice+rx+width*ry;
};

void update_velocity(int x, int y) {
	struct oscillator *osc = lattice + x + width * y;

	osc->v_a -= dt*f_a*d_a*osc->v_a/PI + dt*f_a*f_a*osc->a/(4*PI*PI);
	if (y != height-1)
		osc->v_a += dt*f_a*f_a/(4*PI*PI)*c_a*( get_neighbour(x,y,NORTH)->a - osc->a );
	if (x != width-1)
		osc->v_a += dt*f_a*f_a/(4*PI*PI)*c_a*( get_neighbour(x,y,EAST)->a - osc->a );
	if (y != 0)
		osc->v_a += dt*f_a*f_a/(4*PI*PI)*c_a*( get_neighbour(x,y,SOUTH)->a - osc->a );
	if (x != 0)
		osc->v_a += dt*f_a*f_a/(4*PI*PI)*c_a*( get_neighbour(x,y,WEST)->a - osc->a );


	osc->v_b -= dt*f_b*d_b*osc->v_b/PI + dt*f_b*f_b*osc->b/(4*PI*PI);
	if (y != height)
		osc->v_b += dt*f_b*f_b/(4*PI*PI)*c_b*( get_neighbour(x,y,NORTH)->b - osc->b );
	if (x != width)
		osc->v_b += dt*f_b*f_b/(4*PI*PI)*c_b*( get_neighbour(x,y,EAST)->b - osc->b );
	if (y != 0)
		osc->v_b += dt*f_b*f_b/(4*PI*PI)*c_b*( get_neighbour(x,y,SOUTH)->b - osc->b );
	if (x != 0)
		osc->v_b += dt*f_b*f_b/(4*PI*PI)*c_b*( get_neighbour(x,y,WEST)->b - osc->b );
}

void apply_velocity(int x, int y) {
	struct oscillator *osc = lattice + x + width * y;

	osc->a += dt * osc->v_a;
	osc->b += dt * osc->v_b;
}


void on_frame(struct context *ctx) {

	
	for(uint i=0; i<number_fingers; ++i) {
		int x = (int)(width+fingers[i].x) % width;
		int y = (int)(2*height-fingers[i].y) % height;
		struct oscillator *osc = lattice + x + width * y;
		osc->a = 1;
		osc->b = 1;
	}
	//printf("a=%f v_a=%f\n", lattice[0].a, lattice[0].v_a);

	// Update velocities
	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			update_velocity(x, y);

	// Apply velocities
	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			apply_velocity(x, y);

	// Render lattice
	for (int x=0; x<width; ++x)
		for (int y=0; y<height; ++y)
			paint_at_coords(ctx, x, y,  get_osc_color(x,y));
}

void entry_point(struct context *ctx, char *params) {

	puts("Hello from the ripple plugin!");


	if (ctx->state->index_from_coords == NULL) {
		error("This game requires that grid drawing is supported!");
		return;
	}

	width = ctx->state->grid_width;
	height = ctx->state->grid_height;
	dt = (float)ctx->state->usec_per_frame / 1000000.0;
	lattice = malloc(sizeof(struct oscillator) * width * height);
	
	
	for (int x=0; x<width; ++x) {
		for (int y=0; y<height; ++y) {
			struct oscillator* osc = lattice + x + width * y;
			osc->a = 0;
			osc->v_a = 0;
			osc->b = 0;
			osc->v_b = 0;
		}
	}

	io_loop(ctx, on_input, on_frame);

	free(lattice);
}

