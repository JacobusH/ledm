/* A Bison parser, made by GNU Bison 3.5.3.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.5.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "src/ledm/parser.y"


#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>

#include <libledm.h>
#include <ledm.h>



static struct context *ctx = NULL;
static char *line = NULL;
static char *input = NULL;
static int end_of_file = 0;
static uint at_start_of_line = 1;

static int use_readline = 0;
static FILE *file = NULL;



void fetch_line();
int hexDigitToInt(char c);
int yylex();
void yyerror(char const *);

// implemented in core.c:
void cmd_jump(struct context *ctx, uint *args);
void cmd_jumpunless(struct context *ctx, uint *args);
void cmd_print_var(struct context *ctx, uint *args);
void cmd_scene_jump(struct context *ctx, uint *args);
void cmd_scene_set(struct context *ctx, uint *args);

// implemented in io.c:
void cmd_io_poll(struct context *ctx, uint *args);
void cmd_io_push(struct context *ctx, uint *args);
void cmd_waitframe(struct context *ctx, uint *args);

// implemented in painte.c:
void cmd_paint_mode(struct context *ctx, uint *args);



#line 122 "src/ledm/parser.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    END = 0,
    UNRECOGNIZED = 258,
    COMMENT = 259,
    VARIABLE = 260,
    LITERAL_COLOR = 261,
    LITERAL_STRING = 262,
    LITERAL_UINT = 263,
    LITERAL_FLOAT = 264,
    WORD = 265,
    IF = 266,
    WHILE = 267,
    ELSE = 268,
    CONTINUE = 269,
    BREAK = 270,
    LOOP = 271,
    SCENE = 272,
    NEG = 273
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{

  /* LITERAL_STRING  */
  char* LITERAL_STRING;
  /* WORD  */
  char* WORD;
  /* LITERAL_FLOAT  */
  float LITERAL_FLOAT;
  /* arg  */
  struct arg arg;
  /* arg_map  */
  struct arg arg_map;
  /* arg_map_entry  */
  struct arg arg_map_entry;
  /* arg_list  */
  struct arg* arg_list;
  /* expr  */
  struct expr expr;
  /* LITERAL_COLOR  */
  struct ledm_rgba LITERAL_COLOR;
  /* VARIABLE  */
  uint VARIABLE;
  /* LITERAL_UINT  */
  uint LITERAL_UINT;
  /* literal  */
  uint literal;
#line 216 "src/ledm/parser.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);





#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))

/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   434

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  40
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  20
/* YYNRULES -- Number of rules.  */
#define YYNRULES  67
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  142

#define YYUNDEFTOK  2
#define YYMAXUTOK   273


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      31,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    28,     2,     2,     2,    27,    21,     2,
      34,    35,    25,    23,    39,    24,     2,    26,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    36,     2,
      19,    18,    20,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    37,     2,    38,    30,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    33,    22,    32,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    29
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,    96,    96,    98,   105,   112,   119,   127,   127,   131,
     136,   136,   145,   145,   155,   155,   170,   170,   188,   188,
     204,   204,   222,   222,   222,   224,   229,   233,   236,   246,
     258,   269,   271,   278,   282,   286,   290,   302,   308,   311,
     316,   319,   324,   337,   342,   347,   352,   360,   363,   366,
     371,   376,   381,   386,   391,   396,   401,   406,   411,   416,
     421,   425,   429,   443,   464,   480,   499,   511
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "$undefined", "UNRECOGNIZED", "COMMENT",
  "VARIABLE", "LITERAL_COLOR", "LITERAL_STRING", "LITERAL_UINT",
  "LITERAL_FLOAT", "WORD", "IF", "WHILE", "ELSE", "CONTINUE", "BREAK",
  "LOOP", "SCENE", "'='", "'<'", "'>'", "'&'", "'|'", "'+'", "'-'", "'*'",
  "'/'", "'%'", "'!'", "NEG", "'^'", "'\\n'", "'}'", "'{'", "'('", "')'",
  "':'", "'['", "']'", "','", "$accept", "input", "whitespace", "ifblock",
  "$@1", "ifstart", "$@2", "$@3", "whileblock", "$@4", "sceneblock", "$@5",
  "$@6", "line", "arg_list", "arg", "arg_map", "arg_map_entry", "literal",
  "expr", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,    61,    60,
      62,    38,   124,    43,    45,    42,    47,    37,    33,   273,
      94,    10,   125,   123,    40,    41,    58,    91,    93,    44
};
# endif

#define YYPACT_NINF (-65)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-16)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -65,    58,   -65,   -19,   -13,    -9,   -65,   -14,    -7,    -6,
       5,    16,    45,   -65,   -65,    33,   -65,   -65,   -65,   -65,
     -65,   239,   -65,    -3,   239,   239,   -65,   -65,    29,    28,
      -5,   -65,   -65,   -65,   -65,   -65,    32,   239,   239,   239,
     -65,   390,   -65,   -65,   138,   -65,   -65,   239,   -65,   -65,
     300,   318,   -65,   -65,    57,    18,   -65,   208,    96,    46,
     336,   239,   239,   239,   239,   239,   239,   239,   239,   239,
     239,   239,   -65,   -65,     0,   -65,   279,    44,    47,   -65,
      49,    54,   -65,   -65,   235,   -65,   198,   198,   198,    75,
      75,    96,    96,    46,    46,    46,   -65,    60,   -65,   -65,
     -65,   -15,   -65,   -65,   -65,    80,   -65,   239,   -65,   -65,
     239,   239,   -65,   -65,   -65,   -65,    62,   -65,   354,   103,
     257,   404,    25,   126,   149,   -65,   172,    70,    78,   -65,
     239,   -65,    79,    85,   -65,   -65,   372,   -65,   -65,   -65,
     -65,   195
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       2,     0,     1,     0,     0,     0,    31,     0,     0,     0,
       0,     0,     0,    22,     4,     0,     5,     6,     3,    24,
      23,     0,    27,     0,     0,     0,    28,    29,     0,     0,
       0,    48,    43,    44,    45,    46,    66,     0,     0,     0,
      47,     0,    34,    33,     0,    25,     7,     0,    32,    35,
       0,     0,    30,    20,     0,     0,     9,     0,    61,    60,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    26,    36,     0,     7,     0,     0,     0,     2,
       0,     0,    10,    62,     0,    67,    53,    54,    55,    58,
      57,    49,    50,    51,    52,    56,    59,     0,     8,    39,
      40,     0,    37,    12,    16,     0,    18,     0,     2,    63,
       0,     0,    38,     7,     2,     2,     0,     2,     0,     0,
       0,    42,     0,     0,     0,    21,     0,     0,     0,    64,
       0,    41,     0,     0,    14,    11,     0,    17,    19,     2,
      65,     0
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -65,   -60,   -64,   -65,   -65,   -65,   -65,   -65,   -65,   -65,
     -65,   -65,   -65,   -65,   -65,   -65,   -65,     2,    27,   -24
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,    74,    14,   108,    15,   114,   139,    16,   115,
      17,   117,    79,    18,    23,    48,    75,   100,    40,    41
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      50,    51,    42,    32,    33,    34,    35,    43,    55,    21,
      97,   101,    19,    58,    59,    60,    98,   112,    20,   105,
      24,    44,    22,    76,   113,    26,    56,    25,    45,    81,
      46,    98,    99,    84,    47,    97,    27,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,   119,   122,
      49,    82,    28,    29,   123,   124,    98,   126,     2,     3,
      52,    53,     4,     5,    54,    30,    57,    80,     6,     7,
       8,    73,     9,    10,    11,    12,    71,   103,   111,   141,
     104,     3,   106,   118,     4,     5,   120,   121,   107,    13,
       6,     7,     8,   125,     9,    10,    11,    12,    66,    67,
      68,    69,    70,   134,     3,    71,   136,     4,     5,   135,
     137,    13,   116,     6,     7,     8,   138,     9,    10,    11,
      12,    68,    69,    70,   131,     0,    71,     3,     0,     0,
       4,     5,     0,     0,    13,   128,     6,     7,     8,     0,
       9,    10,    11,    12,    32,    33,    34,    35,     0,     0,
       3,     0,     0,     4,     5,     0,     0,    13,   -13,     6,
       7,     8,     0,     9,    10,    11,    12,     0,     0,     0,
       0,     0,     0,     3,     0,     0,     4,     5,     0,     0,
      13,   132,     6,     7,     8,     0,     9,    10,    11,    12,
       0,     0,     0,     0,     0,     0,     3,     0,     0,     4,
       5,     0,     0,    13,   133,     6,     7,     8,     0,     9,
      10,    11,    12,    31,    32,    33,    34,    35,    36,    64,
      65,    66,    67,    68,    69,    70,    13,   -15,    71,     0,
       0,     0,    37,     0,     0,     0,    38,     0,     0,     0,
       0,     0,    39,    83,    31,    32,    33,    34,    35,    36,
       0,     0,     0,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    37,     0,    71,     0,    38,     0,     0,
     109,     0,     0,    39,   110,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,     0,     0,    71,     0,     0,
       0,     0,   129,     0,     0,     0,   130,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,     0,     0,    71,
       0,     0,     0,     0,     0,     0,     0,   102,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,     0,     0,
      71,     0,     0,     0,     0,    77,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,     0,     0,    71,     0,
       0,     0,     0,    78,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,     0,     0,    71,     0,     0,     0,
       0,    85,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,     0,     0,    71,     0,     0,     0,     0,   127,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
       0,     0,    71,     0,     0,     0,     0,   140,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,     0,     0,
      71,    72,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,     0,     0,    71
};

static const yytype_int16 yycheck[] =
{
      24,    25,     5,     6,     7,     8,     9,    10,    13,    18,
      10,    75,    31,    37,    38,    39,    31,    32,    31,    79,
      34,    24,    31,    47,    39,    31,    31,    34,    31,    11,
      33,    31,    32,    57,    37,    10,    31,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,   108,   113,
      23,    33,    36,     8,   114,   115,    31,   117,     0,     1,
      31,    33,     4,     5,    36,    32,    34,    10,    10,    11,
      12,    44,    14,    15,    16,    17,    30,    33,    18,   139,
      33,     1,    33,   107,     4,     5,   110,   111,    34,    31,
      10,    11,    12,    31,    14,    15,    16,    17,    23,    24,
      25,    26,    27,    33,     1,    30,   130,     4,     5,    31,
      31,    31,    32,    10,    11,    12,    31,    14,    15,    16,
      17,    25,    26,    27,   122,    -1,    30,     1,    -1,    -1,
       4,     5,    -1,    -1,    31,    32,    10,    11,    12,    -1,
      14,    15,    16,    17,     6,     7,     8,     9,    -1,    -1,
       1,    -1,    -1,     4,     5,    -1,    -1,    31,    32,    10,
      11,    12,    -1,    14,    15,    16,    17,    -1,    -1,    -1,
      -1,    -1,    -1,     1,    -1,    -1,     4,     5,    -1,    -1,
      31,    32,    10,    11,    12,    -1,    14,    15,    16,    17,
      -1,    -1,    -1,    -1,    -1,    -1,     1,    -1,    -1,     4,
       5,    -1,    -1,    31,    32,    10,    11,    12,    -1,    14,
      15,    16,    17,     5,     6,     7,     8,     9,    10,    21,
      22,    23,    24,    25,    26,    27,    31,    32,    30,    -1,
      -1,    -1,    24,    -1,    -1,    -1,    28,    -1,    -1,    -1,
      -1,    -1,    34,    35,     5,     6,     7,     8,     9,    10,
      -1,    -1,    -1,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    24,    -1,    30,    -1,    28,    -1,    -1,
      35,    -1,    -1,    34,    39,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    -1,    -1,    30,    -1,    -1,
      -1,    -1,    35,    -1,    -1,    -1,    39,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    -1,    -1,    30,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    -1,    -1,
      30,    -1,    -1,    -1,    -1,    35,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    -1,    -1,    30,    -1,
      -1,    -1,    -1,    35,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    -1,    -1,    30,    -1,    -1,    -1,
      -1,    35,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    -1,    -1,    30,    -1,    -1,    -1,    -1,    35,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      -1,    -1,    30,    -1,    -1,    -1,    -1,    35,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    -1,    -1,
      30,    31,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    -1,    -1,    30
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    41,     0,     1,     4,     5,    10,    11,    12,    14,
      15,    16,    17,    31,    43,    45,    48,    50,    53,    31,
      31,    18,    31,    54,    34,    34,    31,    31,    36,     8,
      32,     5,     6,     7,     8,     9,    10,    24,    28,    34,
      58,    59,     5,    10,    24,    31,    33,    37,    55,    58,
      59,    59,    31,    33,    36,    13,    31,    34,    59,    59,
      59,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    30,    31,    58,    42,    56,    59,    35,    35,    52,
      10,    11,    33,    35,    59,    35,    59,    59,    59,    59,
      59,    59,    59,    59,    59,    59,    59,    10,    31,    32,
      57,    42,    38,    33,    33,    41,    33,    34,    44,    35,
      39,    18,    32,    39,    46,    49,    32,    51,    59,    41,
      59,    59,    42,    41,    41,    31,    41,    35,    32,    35,
      39,    57,    32,    32,    33,    31,    59,    31,    31,    47,
      35,    41
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    40,    41,    41,    41,    41,    41,    42,    42,    43,
      44,    43,    46,    45,    47,    45,    49,    48,    51,    50,
      52,    50,    53,    53,    53,    53,    53,    53,    53,    53,
      53,    54,    54,    55,    55,    55,    55,    55,    55,    55,
      56,    56,    57,    58,    58,    58,    58,    59,    59,    59,
      59,    59,    59,    59,    59,    59,    59,    59,    59,    59,
      59,    59,    59,    59,    59,    59,    59,    59
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     2,     2,     2,     0,     2,     3,
       0,     8,     0,     7,     0,    10,     0,     9,     0,     9,
       0,     7,     1,     2,     2,     3,     4,     2,     2,     2,
       3,     0,     2,     1,     1,     1,     2,     3,     4,     3,
       2,     5,     3,     1,     1,     1,     1,     1,     1,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       2,     2,     3,     4,     6,     8,     1,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[+yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
#  else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                yy_state_t *yyssp, int yytoken)
{
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Actual size of YYARG. */
  int yycount = 0;
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[+*yyssp];
      YYPTRDIFF_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
      yysize = yysize0;
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYPTRDIFF_T yysize1
                    = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    /* Don't count the "%s"s in the final size, but reserve room for
       the terminator.  */
    YYPTRDIFF_T yysize1 = yysize + (yystrlen (yyformat) - 2 * yycount) + 1;
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yytype)
    {
    case 10: /* WORD  */
#line 79 "src/ledm/parser.y"
            { free(((*yyvaluep).WORD)); }
#line 1278 "src/ledm/parser.c"
        break;

    case 54: /* arg_list  */
#line 83 "src/ledm/parser.y"
            { destroy_arg_list(((*yyvaluep).arg_list)); }
#line 1284 "src/ledm/parser.c"
        break;

    case 55: /* arg  */
#line 80 "src/ledm/parser.y"
            { destroy_arg(((*yyvaluep).arg)); }
#line 1290 "src/ledm/parser.c"
        break;

    case 56: /* arg_map  */
#line 81 "src/ledm/parser.y"
            { destroy_arg(((*yyvaluep).arg_map)); }
#line 1296 "src/ledm/parser.c"
        break;

    case 57: /* arg_map_entry  */
#line 82 "src/ledm/parser.y"
            { destroy_arg(((*yyvaluep).arg_map_entry)); }
#line 1302 "src/ledm/parser.c"
        break;

    case 59: /* expr  */
#line 84 "src/ledm/parser.y"
            { destroy_expr(((*yyvaluep).expr)); }
#line 1308 "src/ledm/parser.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYPTRDIFF_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 3:
#line 98 "src/ledm/parser.y"
                  {
	if (ctx->run_immediately) {
		execute_context(ctx);
		cmd_io_poll(ctx, NULL);
		cmd_io_push(ctx, NULL);
	}
}
#line 1584 "src/ledm/parser.c"
    break;

  case 4:
#line 105 "src/ledm/parser.y"
                     {
	if (ctx->run_immediately) {
		execute_context(ctx);
		cmd_io_poll(ctx, NULL);
		cmd_io_push(ctx, NULL);
	}
}
#line 1596 "src/ledm/parser.c"
    break;

  case 5:
#line 112 "src/ledm/parser.y"
                        {
	if (ctx->run_immediately) {
		execute_context(ctx);
		cmd_io_poll(ctx, NULL);
		cmd_io_push(ctx, NULL);
	}
}
#line 1608 "src/ledm/parser.c"
    break;

  case 6:
#line 119 "src/ledm/parser.y"
                        {
	if (ctx->run_immediately) {
		execute_context(ctx);
		cmd_io_poll(ctx, NULL);
		cmd_io_push(ctx, NULL);
	}
}
#line 1620 "src/ledm/parser.c"
    break;

  case 9:
#line 131 "src/ledm/parser.y"
                          {
	*(ctx->blocks->pointer_next) = ctx->instrs_used;
	set_block_end_pointers(ctx->blocks, ctx->instrs_used);
	pop_block(ctx);
}
#line 1630 "src/ledm/parser.c"
    break;

  case 10:
#line 136 "src/ledm/parser.y"
                              {
	uint *arg = malloc(sizeof(uint));
	add_block_end_pointer(ctx->blocks, arg);
	add_instr(ctx, (struct instr){cmd_jump,arg});
	*(ctx->blocks->pointer_next) = ctx->instrs_used;
}
#line 1641 "src/ledm/parser.c"
    break;

  case 11:
#line 141 "src/ledm/parser.y"
                 {
	set_block_end_pointers(ctx->blocks, ctx->instrs_used);
	pop_block(ctx);
}
#line 1650 "src/ledm/parser.c"
    break;

  case 12:
#line 145 "src/ledm/parser.y"
                             {
	struct block *b = add_block(ctx, BLOCK_IF);
	uint condition = new_var(ctx);
	add_expr_eval_instr(ctx, (yyvsp[-2].expr), condition);
	destroy_expr((yyvsp[-2].expr));
	uint *arg = malloc(2*sizeof(uint));
	arg[0] = condition;
	add_instr(ctx, (struct instr){cmd_jumpunless,arg});
	b->pointer_next = arg+1;
}
#line 1665 "src/ledm/parser.c"
    break;

  case 14:
#line 155 "src/ledm/parser.y"
                                              {
	uint *arg = malloc(sizeof(uint));
	add_block_end_pointer(ctx->blocks, arg);
	add_instr(ctx, (struct instr){cmd_jump,arg});
	*(ctx->blocks->pointer_next) = ctx->instrs_used;
	uint condition = new_var(ctx);
	add_expr_eval_instr(ctx, (yyvsp[-2].expr), condition);
	destroy_expr((yyvsp[-2].expr));
	arg = malloc(2*sizeof(uint));
	arg[0] = condition;
	add_instr(ctx, (struct instr){cmd_jumpunless,arg});
	ctx->blocks->pointer_next = arg+1;
}
#line 1683 "src/ledm/parser.c"
    break;

  case 16:
#line 170 "src/ledm/parser.y"
                                   {
	struct block *b = add_block(ctx, BLOCK_WHILE);
	uint condition = new_var(ctx);
	add_expr_eval_instr(ctx, (yyvsp[-2].expr), condition);
	destroy_expr((yyvsp[-2].expr));
	uint *arg = malloc(2*sizeof(uint));
	arg[0] = condition;
	add_instr(ctx, (struct instr){cmd_jumpunless,arg});
	add_block_end_pointer(b, arg+1);
}
#line 1698 "src/ledm/parser.c"
    break;

  case 17:
#line 179 "src/ledm/parser.y"
                 {
	uint *arg = malloc(sizeof(uint));
	arg[0] = ctx->blocks->first_instr;
	add_instr(ctx, (struct instr){cmd_jump,arg});
	set_block_end_pointers(ctx->blocks, ctx->instrs_used);
	pop_block(ctx);
}
#line 1710 "src/ledm/parser.c"
    break;

  case 18:
#line 188 "src/ledm/parser.y"
                                            {
	add_block(ctx, BLOCK_SCENE);
	ctx->scenes = realloc(ctx->scenes, (++ctx->scenes_size) * sizeof(struct scene));
	ctx->scenes[ctx->scenes_size-1].number = (yyvsp[-3].LITERAL_UINT);
	ctx->scenes[ctx->scenes_size-1].name = (yyvsp[-1].WORD);
	ctx->scenes[ctx->scenes_size-1].instr = ctx->instrs_used+1;
	add_instr_args(ctx, cmd_scene_set, 1, ctx->instrs_used+1);
	add_instr_args(ctx, cmd_io_poll, 0);
}
#line 1724 "src/ledm/parser.c"
    break;

  case 19:
#line 196 "src/ledm/parser.y"
                 {
	add_instr_args(ctx, cmd_waitframe, 0);
	add_instr_args(ctx, cmd_io_push, 0);
	add_instr_args(ctx, cmd_paint_mode, 1, 0);
	add_instr_args(ctx, cmd_scene_jump, 0);
	pop_block(ctx);
}
#line 1736 "src/ledm/parser.c"
    break;

  case 20:
#line 204 "src/ledm/parser.y"
                                   {
	add_block(ctx, BLOCK_SCENE);
	ctx->scenes = realloc(ctx->scenes, (++ctx->scenes_size) * sizeof(struct scene));
	ctx->scenes[ctx->scenes_size-1].number = (yyvsp[-1].LITERAL_UINT);
	ctx->scenes[ctx->scenes_size-1].name = NULL;
	ctx->scenes[ctx->scenes_size-1].instr = ctx->instrs_used+1;
	add_instr_args(ctx, cmd_scene_set, 1, ctx->instrs_used+1);
	add_instr_args(ctx, cmd_io_poll, 0);
}
#line 1750 "src/ledm/parser.c"
    break;

  case 21:
#line 212 "src/ledm/parser.y"
                 {
	add_instr_args(ctx, cmd_waitframe, 0);
	add_instr_args(ctx, cmd_io_push, 0);
	add_instr_args(ctx, cmd_paint_mode, 1, 0);
	add_instr_args(ctx, cmd_scene_jump, 0);
	pop_block(ctx);
}
#line 1762 "src/ledm/parser.c"
    break;

  case 24:
#line 222 "src/ledm/parser.y"
                                       {yyerrok;}
#line 1768 "src/ledm/parser.c"
    break;

  case 25:
#line 224 "src/ledm/parser.y"
                         {
	parse_command((yyvsp[-2].WORD), (yyvsp[-1].arg_list), ctx->cmds);
	free((yyvsp[-2].WORD));
	destroy_arg_list((yyvsp[-1].arg_list));
}
#line 1778 "src/ledm/parser.c"
    break;

  case 26:
#line 229 "src/ledm/parser.y"
                             {
	add_expr_eval_instr(ctx, (yyvsp[-1].expr), (yyvsp[-3].VARIABLE));
	destroy_expr((yyvsp[-1].expr));
}
#line 1787 "src/ledm/parser.c"
    break;

  case 27:
#line 233 "src/ledm/parser.y"
                    {
	add_instr_args(ctx, cmd_print_var, 1, (yyvsp[-1].VARIABLE));
}
#line 1795 "src/ledm/parser.c"
    break;

  case 28:
#line 236 "src/ledm/parser.y"
                    {
	struct block *b = ctx->blocks;
	while (b != NULL && b->type != BLOCK_WHILE)
		b = b->prev;
	if (!b) {
		parser_error(ctx, "No while loop to continue here.");
	} else {
		add_instr_args(ctx, cmd_jump, 1, b->first_instr);
	}
}
#line 1810 "src/ledm/parser.c"
    break;

  case 29:
#line 246 "src/ledm/parser.y"
                 {
	struct block *b = ctx->blocks;
	while (b != NULL && b->type != BLOCK_WHILE)
		b = b->prev;
	if (!b) {
		parser_error(ctx, "No while loop to break here.");
	} else {
		uint *arg = malloc(sizeof(uint));
		add_block_end_pointer(b, arg);
		add_instr(ctx, (struct instr){cmd_jump,arg});
	}
}
#line 1827 "src/ledm/parser.c"
    break;

  case 30:
#line 258 "src/ledm/parser.y"
                    {
	if(!ctx->blocks || ctx->blocks->type != BLOCK_SCENE) {
		parser_error(ctx, "loop statement must sit in scene block.");
	} else {
		add_instr_args(ctx, cmd_scene_set, 1, ctx->instrs_used);
		add_instr_args(ctx, cmd_io_poll, 0);
	}
}
#line 1840 "src/ledm/parser.c"
    break;

  case 31:
#line 269 "src/ledm/parser.y"
                 { (yyval.arg_list) = NULL; }
#line 1846 "src/ledm/parser.c"
    break;

  case 32:
#line 271 "src/ledm/parser.y"
                       {
	(yyval.arg_list) = add_to_arg_list((yyvsp[-1].arg_list), (yyvsp[0].arg));
	destroy_arg_list((yyvsp[-1].arg_list));
}
#line 1855 "src/ledm/parser.c"
    break;

  case 33:
#line 278 "src/ledm/parser.y"
          {
	(yyval.arg).type = ARG_WORD;
	(yyval.arg).content.word = (yyvsp[0].WORD);
}
#line 1864 "src/ledm/parser.c"
    break;

  case 34:
#line 282 "src/ledm/parser.y"
              {
	(yyval.arg).type = ARG_VAR;
	(yyval.arg).content.var = (yyvsp[0].VARIABLE);
}
#line 1873 "src/ledm/parser.c"
    break;

  case 35:
#line 286 "src/ledm/parser.y"
             {
	(yyval.arg).type = ARG_LITERAL;
	(yyval.arg).content.var = (yyvsp[0].literal);
}
#line 1882 "src/ledm/parser.c"
    break;

  case 36:
#line 290 "src/ledm/parser.y"
                 {
	(yyval.arg).type = ARG_LITERAL;
	(yyval.arg).content.var = (yyvsp[0].literal);
	if (ctx->vars_types[(yyvsp[0].literal)] == 'f') {
		ctx->vars[(yyvsp[0].literal)].f *= -1;
	} else if (ctx->vars_types[(yyvsp[0].literal)] == 'u') {
		ctx->vars[(yyvsp[0].literal)].f = -(float)ctx->vars[(yyvsp[0].literal)].u;
		ctx->vars_types[(yyvsp[0].literal)] = 'f';
	} else {
		YYERROR;
	}
}
#line 1899 "src/ledm/parser.c"
    break;

  case 37:
#line 302 "src/ledm/parser.y"
                  {
	(yyval.arg).type = ARG_VAR;
	(yyval.arg).content.var = new_var(ctx);
	add_expr_eval_instr(ctx, (yyvsp[-1].expr), (yyval.arg).content.var);
	destroy_expr((yyvsp[-1].expr));
}
#line 1910 "src/ledm/parser.c"
    break;

  case 38:
#line 308 "src/ledm/parser.y"
                                {
	(yyval.arg) = (yyvsp[-2].arg_map);
}
#line 1918 "src/ledm/parser.c"
    break;

  case 39:
#line 311 "src/ledm/parser.y"
                        {
	(yyval.arg).type = ARG_MAP;
	(yyval.arg).content.map = malloc(sizeof(struct arg_map));
	(yyval.arg).content.map->name = NULL;
}
#line 1928 "src/ledm/parser.c"
    break;

  case 40:
#line 316 "src/ledm/parser.y"
                                  {
	(yyval.arg_map) = (yyvsp[0].arg_map_entry);
}
#line 1936 "src/ledm/parser.c"
    break;

  case 41:
#line 319 "src/ledm/parser.y"
                                                         {
	(yyval.arg_map) = merge_arg_maps((yyvsp[-4].arg_map), (yyvsp[0].arg_map_entry));
	destroy_arg((yyvsp[-4].arg_map));
	destroy_arg((yyvsp[0].arg_map_entry));
}
#line 1946 "src/ledm/parser.c"
    break;

  case 42:
#line 324 "src/ledm/parser.y"
                             {
	//TODO change in order to not unneccessarily evaluate literal expression
	uint index = new_var(ctx);
	add_expr_eval_instr(ctx, (yyvsp[0].expr), index);
	destroy_expr((yyvsp[0].expr));
	(yyval.arg_map_entry).type = ARG_MAP;
	(yyval.arg_map_entry).content.map = malloc(2 * sizeof(struct arg_map));
	(yyval.arg_map_entry).content.map[0].name = (yyvsp[-2].WORD);
	(yyval.arg_map_entry).content.map[0].index = index;
	(yyval.arg_map_entry).content.map[1].name = NULL;
}
#line 1962 "src/ledm/parser.c"
    break;

  case 43:
#line 337 "src/ledm/parser.y"
                       {
	(yyval.literal) = new_var(ctx);
	ctx->vars_types[(yyval.literal)]='c';
	ctx->vars[(yyval.literal)].c = (yyvsp[0].LITERAL_COLOR);
}
#line 1972 "src/ledm/parser.c"
    break;

  case 44:
#line 342 "src/ledm/parser.y"
                        {
	(yyval.literal) = new_var(ctx);
	ctx->vars_types[(yyval.literal)]='s';
	ctx->vars[(yyval.literal)].s = (yyvsp[0].LITERAL_STRING);
}
#line 1982 "src/ledm/parser.c"
    break;

  case 45:
#line 347 "src/ledm/parser.y"
                      {
	(yyval.literal) = new_var(ctx);
	ctx->vars_types[(yyval.literal)]='u';
	ctx->vars[(yyval.literal)].u = (yyvsp[0].LITERAL_UINT);
}
#line 1992 "src/ledm/parser.c"
    break;

  case 46:
#line 352 "src/ledm/parser.y"
                       {
	(yyval.literal) = new_var(ctx);
	ctx->vars_types[(yyval.literal)]='f';
	ctx->vars[(yyval.literal)].f = (yyvsp[0].LITERAL_FLOAT);
}
#line 2002 "src/ledm/parser.c"
    break;

  case 47:
#line 360 "src/ledm/parser.y"
              {
	(yyval.expr) = create_expr_from_var((yyvsp[0].literal));
}
#line 2010 "src/ledm/parser.c"
    break;

  case 48:
#line 363 "src/ledm/parser.y"
               {
	(yyval.expr) = create_expr_from_var((yyvsp[0].VARIABLE));
}
#line 2018 "src/ledm/parser.c"
    break;

  case 49:
#line 366 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(ADDITION, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2028 "src/ledm/parser.c"
    break;

  case 50:
#line 371 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(SUBTRACTION, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2038 "src/ledm/parser.c"
    break;

  case 51:
#line 376 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(MULTIPLICATION, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2048 "src/ledm/parser.c"
    break;

  case 52:
#line 381 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(DIVISION, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2058 "src/ledm/parser.c"
    break;

  case 53:
#line 386 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(EQUALITY, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2068 "src/ledm/parser.c"
    break;

  case 54:
#line 391 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(LOWER_THAN, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2078 "src/ledm/parser.c"
    break;

  case 55:
#line 396 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(GREATER_THAN, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2088 "src/ledm/parser.c"
    break;

  case 56:
#line 401 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(MODULUS, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2098 "src/ledm/parser.c"
    break;

  case 57:
#line 406 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(LOGICAL_OR, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2108 "src/ledm/parser.c"
    break;

  case 58:
#line 411 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(LOGICAL_AND, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2118 "src/ledm/parser.c"
    break;

  case 59:
#line 416 "src/ledm/parser.y"
                    {
	(yyval.expr) = expr_binary_op(POWER, (yyvsp[-2].expr), (yyvsp[0].expr));
	destroy_expr((yyvsp[-2].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2128 "src/ledm/parser.c"
    break;

  case 60:
#line 421 "src/ledm/parser.y"
               {
	(yyval.expr) = expr_unary_op(LOGICAL_NEGATION, (yyvsp[0].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2137 "src/ledm/parser.c"
    break;

  case 61:
#line 425 "src/ledm/parser.y"
               {
	(yyval.expr) = expr_unary_op(NEGATION, (yyvsp[0].expr));
	destroy_expr((yyvsp[0].expr));
}
#line 2146 "src/ledm/parser.c"
    break;

  case 62:
#line 429 "src/ledm/parser.y"
                   {
	int error = 0;
	if (strcmp((yyvsp[-2].WORD), "rand") == 0) {
		(yyval.expr) = expr_nullary_op(RANDOM);
	} else if (strcmp((yyvsp[-2].WORD), "now") == 0) {
		(yyval.expr) = expr_nullary_op(TIME);
	} else {
		parser_error(ctx, "Unknown nullary maths function '%s'.", (yyvsp[-2].WORD));
		error = 1;
	}
	free((yyvsp[-2].WORD));
	if (error)
		YYERROR;
}
#line 2165 "src/ledm/parser.c"
    break;

  case 63:
#line 443 "src/ledm/parser.y"
                        {
	int error = 0;
	if (strcmp((yyvsp[-3].WORD), "sin") == 0) {
		(yyval.expr) = expr_unary_op(SINE, (yyvsp[-1].expr));
	} else if (strcmp((yyvsp[-3].WORD), "cos") == 0) {
		(yyval.expr) = expr_unary_op(COSINE, (yyvsp[-1].expr));
	} else if (strcmp((yyvsp[-3].WORD), "exp") == 0) {
		(yyval.expr) = expr_unary_op(EXPONENTIAL, (yyvsp[-1].expr));
	} else if (strcmp((yyvsp[-3].WORD), "uint") == 0) {
		(yyval.expr) = expr_unary_op(TO_UINT, (yyvsp[-1].expr));
	} else if (strcmp((yyvsp[-3].WORD), "inv") == 0) {
		(yyval.expr) = expr_unary_op(INVERT_COLOR, (yyvsp[-1].expr));
	} else {
		parser_error(ctx, "Unknown unary maths function '%s'.", (yyvsp[-3].WORD));
		error = 1;
	}
	free((yyvsp[-3].WORD));
	destroy_expr((yyvsp[-1].expr));
	if (error)
		YYERROR;
}
#line 2191 "src/ledm/parser.c"
    break;

  case 64:
#line 464 "src/ledm/parser.y"
                                 {
	int error = 0;
	if (strcmp((yyvsp[-5].WORD), "max") == 0) {
		(yyval.expr) = expr_binary_op(MAXIMUM, (yyvsp[-3].expr), (yyvsp[-1].expr));
	} else if (strcmp((yyvsp[-5].WORD), "min") == 0) {
		(yyval.expr) = expr_binary_op(MINIMUM, (yyvsp[-3].expr), (yyvsp[-1].expr));
	} else {
		parser_error(ctx, "Unknown binary maths function '%s'.", (yyvsp[-5].WORD));
		error = 1;
	}
	free((yyvsp[-5].WORD));
	destroy_expr((yyvsp[-3].expr));
	destroy_expr((yyvsp[-1].expr));
	if (error)
		YYERROR;
}
#line 2212 "src/ledm/parser.c"
    break;

  case 65:
#line 480 "src/ledm/parser.y"
                                          {
	int error = 0;
	if (strcmp((yyvsp[-7].WORD), "rgb") == 0) {
		(yyval.expr) = expr_ternary_op(RGB_TO_COLOR, (yyvsp[-5].expr), (yyvsp[-3].expr), (yyvsp[-1].expr));
	} else if (strcmp((yyvsp[-7].WORD), "hsv") == 0) {
		(yyval.expr) = expr_ternary_op(HSV_TO_COLOR, (yyvsp[-5].expr), (yyvsp[-3].expr), (yyvsp[-1].expr));
	} else if (strcmp((yyvsp[-7].WORD), "ifthenelse") == 0) {
		(yyval.expr) = expr_ternary_op(TERNARY, (yyvsp[-5].expr), (yyvsp[-3].expr), (yyvsp[-1].expr));
	} else {
		parser_error(ctx, "Unknown ternary maths function '%s'.", (yyvsp[-7].WORD));
		error = 1;
	}
	free((yyvsp[-7].WORD));
	destroy_expr((yyvsp[-5].expr));
	destroy_expr((yyvsp[-3].expr));
	destroy_expr((yyvsp[-1].expr));
	if (error)
		YYERROR;
}
#line 2236 "src/ledm/parser.c"
    break;

  case 66:
#line 499 "src/ledm/parser.y"
           {
	int error = 0;
	if (strcmp((yyvsp[0].WORD), "pi") == 0) {
		(yyval.expr) = expr_nullary_op(CONST_PI);
	} else {
		parser_error(ctx, "Unknown maths constant '%s'.", (yyvsp[0].WORD));
		error = 1;
	}
	free((yyvsp[0].WORD));
	if (error)
		YYERROR;
}
#line 2253 "src/ledm/parser.c"
    break;

  case 67:
#line 511 "src/ledm/parser.y"
                   {
	(yyval.expr) = (yyvsp[-1].expr);
}
#line 2261 "src/ledm/parser.c"
    break;


#line 2265 "src/ledm/parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *, YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[+*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 518 "src/ledm/parser.y"




struct arg *add_to_arg_list(struct arg *list, struct arg element) {
	uint length = 0;
	if (list)
		while (list[length].type != ARG_END) ++length;

	struct arg *out = malloc( sizeof(struct arg) * (length+2) );
	for (uint i=0; i<length; ++i) {
		out[i] = list[i];

		// have words and maps have their own memory
		if (list[i].type == ARG_WORD) {
			out[i].content.word = strdup(list[i].content.word);
		} else if (list[i].type == ARG_MAP) {
			int maplength = 0;
			for (struct arg_map *map = list[i].content.map; map->name != NULL; ++map)
				++maplength;
			out[i].content.map = malloc( (maplength+1) * sizeof( struct arg_map ) );
			for (int j=0; j<maplength; ++j) {
				out[i].content.map[j].name = strdup(list[i].content.map[j].name);
				out[i].content.map[j].index = list[i].content.map[j].index;
			}
			out[i].content.map[maplength].name = NULL;
		}
	}
	out[length] = element;
	out[length + 1].type = ARG_END;
	
	return out;
}

void destroy_arg(struct arg arg) {

	if (arg.type == ARG_WORD) {
		free(arg.content.word);
	} else if (arg.type == ARG_MAP) {
		for(struct arg_map *map = arg.content.map; map->name != NULL; ++map)
			free(map->name);
		free(arg.content.map);
	}
}

void destroy_arg_list(struct arg *list) {
	if (!list)
		return;

	for (uint i=0; list[i].type != ARG_END; ++i)
		destroy_arg(list[i]);

	free(list);
}

struct arg merge_arg_maps(struct arg a, struct arg b) {
	struct arg out;
	out.type = ARG_MAP;
	out.content.map = NULL;

	if (a.type != ARG_MAP || b.type != ARG_MAP)
		return out;

	// Get total length of both arg maps
	int a_length = 0;
	int b_length = 0;
	for ( struct arg_map *map = a.content.map; map->name != NULL; ++map)
		++a_length;
	for ( struct arg_map *map = b.content.map; map->name != NULL; ++map)
		++b_length;

	out.content.map = malloc( (a_length+b_length+1) * sizeof(struct arg_map));

	for (int i=0; i<a_length; ++i) {
		out.content.map[i].name = strdup(a.content.map[i].name);
		out.content.map[i].index = a.content.map[i].index;
	}
	for (int i=0; i<b_length; ++i) {
		out.content.map[a_length+i].name = strdup(b.content.map[i].name);
		out.content.map[a_length+i].index = b.content.map[i].index;
	}
	out.content.map[a_length+b_length].name = NULL;

	return out;
}

void print_arg_list(struct arg *list) {
	if (!list) {
		printf("NULL\n");
		return;
	}

	for (uint i=0; list[i].type != ARG_END; ++i) {
		if (list[i].type == ARG_WORD)
			printf("ARG_WORD (%s) ", list[i].content.word);
		else if (list[i].type == ARG_VAR)
			printf("ARG_VAR (%u) ", list[i].content.var);
		else if (list[i].type == ARG_LITERAL)
			printf("ARG_LITERAL (%u) ", list[i].content.var);
		else if (list[i].type == ARG_MAP) {
			printf("ARG_MAP: ");
			for (struct arg_map *map = list[i].content.map; map->name != NULL; ++map)
				printf("%s=%u ", map->name, map->index);
		}
	}
	printf("ARG_END\n");
}


uint arg_count(struct arg *list) {
	// check if arg list is zero
	if (!list)
		return 0;

	for (uint i=0; ; ++i)
		if (list[i].type == ARG_END)
			return i;
}


uint arg_count_total(struct arg *list) {
	// check if arg list is zero
	if (!list)
		return 0;

	uint count=0;
	for (uint i=0; ; ++i) {
		if (list[i].type == ARG_END) {
			return count;
		} else if (list[i].type == ARG_MAP) {
			for (struct arg_map *map = list[i].content.map; map->name != NULL; ++map)
				++count;
		} else  {
			++count;
		}
	}
}


int check_arg_var(struct context *ctx, struct arg *list, uint nr, char expected_type) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	// check type of arg
	if (list[nr].type != ARG_VAR && list[nr].type != ARG_LITERAL)
		return 0;
	if (ctx->vars_types[list[nr].content.var] != expected_type)
		return 0;

	return 1;
}


int check_arg_word(struct arg *list, uint nr, const char *expected_word) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	// check type of arg
	if (list[nr].type != ARG_WORD)
		return 0;

	// check whether word is expected
	if (expected_word && strcmp(list[nr].content.word, expected_word) != 0)
		return 0;

	return 1;
}


int check_arg_literal(struct arg *list, uint nr) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	if (list[nr].type == ARG_LITERAL)
		return 1;

	return 0;
}

int format_string_count(const char *format, uint *len_list_a, uint *len_args_a) {
	const char *current = format;
	uint len_list = 0;
	uint len_args = 0;

	while (*current != '\0') {
		switch (*current) {
		case 's':
		case 'c':
		case 'f':
		case 'u':
			++len_list;
			++len_args;
			++current;
			break;
		case '[':
			do { ++current; } while (*current != ']' && *current != '\0');
			if (*current == '\0') return 0;
			++current;
			++len_list;
			++len_args;
			break;
		case '{':
			do {
				++current;
				if (*current == ',')
					++len_args;
			} while (*current != '}' && *current != '\0');
			if (*current == '\0') return 0;
			++current;
			++len_list;
			++len_args;
			break;
		default:
			return 0;
		}
	}

	if (len_list_a)
		*len_list_a = len_list;
	if (len_args_a)
		*len_args_a = len_args;

	return 1;
}


enum parse_result parse_arg_list(struct context *ctx, struct arg *arg_list, const char *format, uint **args) {

	if (!format)
		return SUCCESS;

	uint expected_nr_list = 0;
	uint expected_nr_args = 0;
	const char *current = format;
	int return_val;

	if (!format_string_count(format, &expected_nr_list, &expected_nr_args))
		return MALFORMED_FORMAT;

	*args = malloc(sizeof(uint) * expected_nr_args);

	uint arg_i = 0;
	for (uint i=0; i<expected_nr_list; ++i) {

		if (!arg_list || arg_list[i].type == ARG_END) {
			return_val = WRONG_ARG_COUNT;
			goto cleanup;
		}

		switch (*current) {

		case 'c':
		case 'u':
		case 's':
			if (check_arg_var(ctx, arg_list, i, *current)) {
				(*args)[arg_i++] = arg_list[i].content.var;
				++current;
				break;
			} else {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}

		case 'f':
			if (check_arg_var(ctx, arg_list, i, 'f')) {
				(*args)[arg_i++] = arg_list[i].content.var;
				++current;
				break;
			} else if (check_arg_var(ctx, arg_list, i, 'u') && check_arg_literal(arg_list, i)) {
				(*args)[arg_i++] = arg_list[i].content.var;
				ctx->vars_types[arg_list[i].content.var] = 'f';
				ctx->vars[arg_list[i].content.var].f = (float) ctx->vars[arg_list[i].content.var].u;
				++current;
				break;
			} else {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
		case '[':
			if (arg_list[i].type != ARG_WORD) {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
			
			int wordmatch = 0;
			uint matchnr = 0;
			while (*current != '\0' && *current != ']') {
				++current;
				int wordlen = 0;
				while (current[wordlen] != '\0' && current[wordlen] != ',' && current[wordlen] != ']') ++wordlen;
				
				if (strncmp(current, arg_list[i].content.word, wordlen) == 0 && wordlen == (int)strlen(arg_list[i].content.word)) {
					wordmatch = 1;
					(*args)[arg_i++] = matchnr;
					while (*current != '\0' && *current != ']') ++current;
				} else {
					current += wordlen;
					++matchnr;
				}
			}
			if (*current != '\0')
				++current;
			if (!wordmatch) {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
			break;
		case '{':
			if (arg_list[i].type != ARG_MAP) {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}

			while (*current != '\0' && *current != '}') {
				++current;
				int segmentlen = 0;
				while (current[segmentlen] != '\0' && current[segmentlen] != ',' && current[segmentlen] != '}') ++segmentlen;

				int index = -1;
				char name[50];
				float defaultval = 0;
				
				// Attempt to parse segment of the form <word>=<float>
				if (sscanf(current, "%49[^=]=%f", name, &defaultval) != 2) {
					return_val = MALFORMED_FORMAT;
					goto cleanup;
				}

				// go see if name matches a name of supplied arg list
				for (struct arg_map *map = arg_list[i].content.map; map->name!=NULL; ++map)
					if (!strcmp(name, map->name))
						index = map->index;


				if (index == -1) {
					index = new_var(ctx);
					ctx->vars[index].f = defaultval;
					ctx->vars_types[index] = 'f';
				}

				(*args)[arg_i++] = index;
				current += segmentlen;
			}
			if (*current != '\0')
				++current;
			break;
		default:
			return_val = MALFORMED_FORMAT;
			goto cleanup;
		}
	}

	if (arg_list[expected_nr_list].type == ARG_END)
		return SUCCESS;
	else
		return_val = WRONG_ARG_COUNT;

cleanup:
	free(*args);
	*args = NULL;
	return return_val;
}


void parse_command(const char *cmd, struct arg *arg_list, struct cmd *command_list) {

	int match = -1;
	for (int i = 0; command_list[i].name != NULL; i++)
		if ( strcmp(cmd, command_list[i].name) == 0 )
			match = i;

	if (match < 0) {
		parser_error(ctx, "Could not find (sub)command '%s'!", cmd);

	} else if (command_list[match].subcommands) {
		if (arg_list && arg_list[0].type == ARG_WORD)
			parse_command(arg_list[0].content.word, arg_list+1, command_list[match].subcommands);
		else if (!arg_list || arg_list[0].type == ARG_END)
			parse_command("", arg_list, command_list[match].subcommands);
		else
			parser_error(ctx, "Need a word as subcommand of '%s'!", cmd);

	} else if (command_list[match].cmd) {
		uint *args = NULL;

		switch (parse_arg_list(ctx, arg_list, command_list[match].format, &args)) {
		case WRONG_ARG_COUNT:
			parser_error(ctx, "Bad number of arguments!");
			break;

		case MISMATCHED_ARGS:
			parser_error(ctx, "Type error when evaluating arguments!");
			break;

		case MALFORMED_FORMAT:
			parser_error(ctx, "Command format string is malformed!");
			break;

		case SUCCESS:
			if (command_list[match].parser)
				command_list[match].parser(ctx, arg_list);
			add_instr(ctx, (struct instr){command_list[match].cmd,args});
			break;
		}

	} else if (command_list[match].parser) {
		command_list[match].parser(ctx, arg_list);

	} else {
		parser_error(ctx, "Something went wrong - the command '%s' seems to lack parsing information!\n", cmd);
	}
}


int hexDigitToInt(char c) {

	if (c >= '0' && c <= '9') {
		return (c - '0');
	} else if (c >= 'a' && c <= 'f') {
		return (10 + c - 'a');
	} else if (c >= 'A' && c <= 'F') {
		return (10 + c - 'F');
	} else {
		return -1;
	}
}


int yylex() {

	if (ctx->quit)
		return EOF;

	if (!input || strlen(input)==0)
		fetch_line();

	if (end_of_file)
		return EOF;

	input += strspn(input, WHITESPACE);
	uint length = 0;

	// discard comments
	if (at_start_of_line && *input == '#') {

		while (input[length] != '\n' && input[length] != '\0') ++length;
		input += length;
		return COMMENT;

	} else {
		at_start_of_line = 0;
	}

	switch (*input) {
	
	case '\n':
	case '\0':
		at_start_of_line = 1;
		++ctx->line_number;
		return *(input++);
	case '=': case '+': case '-': case '*': case '/':
	case '&': case '|': case '%': case ',': case '?':
	case ':': case '@': case '<': case '>': case '[':
	case ']': case '(': case ')': case '^': case '!':
	case '}': case '{':
		return *(input++);


	case '#':
		++input;
		for (uint i=0; i<6 && input[i]!='\0'; ++i)
			if (hexDigitToInt(input[i]) < 0)
				return UNRECOGNIZED;
		yylval.LITERAL_COLOR.r = 16*hexDigitToInt(input[0]) + hexDigitToInt(input[1]);
		yylval.LITERAL_COLOR.g = 16*hexDigitToInt(input[2]) + hexDigitToInt(input[3]);
		yylval.LITERAL_COLOR.b = 16*hexDigitToInt(input[4]) + hexDigitToInt(input[5]);
		yylval.LITERAL_COLOR.a = 255;
		input += 6;
		return LITERAL_COLOR;


	case '"': ;
		++input;
		while (input[length] != '\n' && input[length] != '\0' && input[length] != '"') ++length;

		if (input[length] != '"')
			return UNRECOGNIZED;

		yylval.LITERAL_STRING = strndup(input, length);
		input += length + 1;
		return LITERAL_STRING;


	case '$':
		++input;
		if (!isalpha(*input))
			return UNRECOGNIZED;
		
		while (isdigit(input[length]) || isalpha(input[length]) || input[length]=='_') ++length;

		char *tmp = strndup(input, length);
		yylval.VARIABLE = get_var_index(ctx, tmp);
		free(tmp);
		input += length;
		return VARIABLE;


	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		yylval.LITERAL_UINT = 0;

		while (isdigit(input[length])) ++length;
		uint power_of_ten = 1;
		for (int i=length-1; i>=0; --i) {
			yylval.LITERAL_UINT += power_of_ten * (uint)(input[i] - '0');
			power_of_ten *= 10;
		}
		input += length;

		if (*input != '.')
			return LITERAL_UINT;

		++input;
		yylval.LITERAL_FLOAT = (float) yylval.LITERAL_UINT;
		length = 0;
		while (isdigit(input[length])) ++length;
		for (uint i=0; i<length; ++i)
			yylval.LITERAL_FLOAT += pow(0.1, 1+(float) i) * (float) (input[i] - '0');
		input += length;
		return LITERAL_FLOAT;


	default:
		if (isalpha(*input)) {
			while (isdigit(input[length]) || isalpha(input[length]) || input[length]=='_') ++length;

			if (length==2 && !strncmp(input, "if", 2)) {
				input += length;
				return IF;
			} else if (length==4 && !strncmp(input, "else", 4)) {
				input += length;
				return ELSE;
			} else if (length==5 && !strncmp(input, "while", 5)) {
				input += length;
				return WHILE;
			} else if (length==8 && !strncmp(input, "continue", 8)) {
				input += length;
				return CONTINUE;
			} else if (length==5 && !strncmp(input, "break", 5)) {
				input += length;
				return BREAK;
			} else if (length==4 && !strncmp(input, "loop", 4)) {
				input += length;
				return LOOP;
			} else if (length==5 && !strncmp(input, "scene", 5)) {
				input += length;
				return SCENE;
			} else {
				yylval.WORD = strndup(input, length);
				input += length;
				return WORD;
			}
		} else {
			++input;
			return UNRECOGNIZED;
		}
	}
}

void yyerror(char const *str) {
	parser_error(ctx, "%s", str);
}


void fetch_line() {

	if(line)
		free(line);
	line = NULL;

	if (use_readline) {

		// Display prompt
		char prompt[20];
		printf("\033[1m");
		if(!ctx->blocks)
			snprintf(prompt, 20, "[%2d] ", ctx->line_number);
		else
			snprintf(prompt, 20, "<%2d> ", ctx->line_number);

		//  Get input
		char *orig_line = readline(prompt);
		printf("\033[0m");

		// Unless EOF was encountered, add to history
		if (orig_line) {
			add_history(orig_line);
			line = malloc(sizeof(char) * (strlen(orig_line)+2) );
			sprintf(line, "%s\n", orig_line);
			input = line;
		} else {
			printf("\n");
			end_of_file = 1;
		}
	
	} else if (file) {
		char *line = NULL;
		size_t len = 0;
		if (getline(&line, &len, file) == -1)
			end_of_file = 1;
		input = line;
	} else {
		end_of_file = 1;
	}
};

void parse_interactive(struct context *context) {
	ctx = context;
	ctx->run_immediately = 1;
	ctx->line_number = 1;
	ctx->file_number = 0;
	use_readline = 1;
	end_of_file = 0;

	yyparse();

	if (line)
		free(line);
	line = NULL;
	input = NULL;
}

void parse_file(struct context *context, const char* filename) {
	ctx = context;
	ctx->run_immediately = 0;
	ctx->line_number = 1;
	use_readline = 0;
	end_of_file = 0;

	// If filename is '-', read from stdin, otherwise from the file with that name. Then, set filenr appropriately
	if (strcmp(filename, "-") == 0) {
		file = stdin;
		ctx->file_number = 0;
	} else {
		file = fopen(filename, "r");

		if (file == NULL) {
			error("Cannot parse '%s': %s", filename, strerror(errno));
			++ctx->errors;
			return;
		}

		yyparse();

		++ctx->files_size;
		ctx->files = realloc(ctx->files, ctx->files_size * sizeof(char*));
		ctx->file_number = ctx->files_size;
		ctx->files[ctx->file_number-1] = strdup(filename);
	}

	// Cleanup
	ctx->file_number = 0;
	ctx->line_number = 1;
	if (line)
		free(line);
	if (file != stdin)
		fclose(file);
	file = NULL;
	line = NULL;
	input = NULL;
}


struct block *add_block(struct context *ctx, enum block_type type) {

	struct block *block = malloc(sizeof(struct block));
	block->type = type;
	block->first_instr = ctx->instrs_used;
	block->pointer_next = NULL;
	block->pointers_end = NULL;
	block->count_pointers_end = 0;
	block->prev = NULL;

	if (ctx->blocks)
		block->prev = ctx->blocks;

	ctx->blocks = block;
	return block;
};

void add_block_end_pointer(struct block *block, uint *ptr) {

	++block->count_pointers_end;
	block->pointers_end = realloc(block->pointers_end, block->count_pointers_end * sizeof(uint**));
	block->pointers_end[block->count_pointers_end-1] = ptr;
};

void set_block_end_pointers(struct block *block, uint val) {

	for (uint i=0; i<block->count_pointers_end; ++i)
		*(block->pointers_end[i]) = val;
};

void pop_block(struct context *ctx) {
	if (ctx->blocks) {
		struct block *prev = ctx->blocks->prev;
		free(ctx->blocks->pointers_end);
		free(ctx->blocks);
		ctx->blocks = prev;
	}
}
