#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <SDL2/SDL.h>
#include <alsa/asoundlib.h>

#include "libledm.h"
#include "libledm_utils.h"
#include "ledm.h"


void on_input(struct ledm_state *state, void *context, struct snd_seq_event *midi_event, union SDL_Event *sdl_event) {

	struct context *ctx = (struct context*) context;
	int index;

	if (midi_event) {
		switch (midi_event->type) {

			case SND_SEQ_EVENT_NOTEON:
				index = ctx->midi_var_index[midi_event->data.note.note];
				if (index >= 0) {
					ctx->vars[index+1].f = midi_event->data.note.velocity / 127.0f; // update velocity
					ctx->vars[index+2].u = ledm_time_ms(); // update note on time
				}
				break;

			case SND_SEQ_EVENT_NOTEOFF:
				index = ctx->midi_var_index[midi_event->data.note.note];
				if (index >= 0) {
					ctx->vars[index+1].f = 0; // reset velocity
					ctx->midi_timer_offset[midi_event->data.note.note] = ctx->vars[index+4].u; // update offset
				}
				break;

			case SND_SEQ_EVENT_CONTROLLER:
				index = ctx->midi_var_index[midi_event->data.control.param];
				if (index >= 0)
					ctx->vars[index].f = midi_event->data.control.value / 127.0f;
				if (midi_event->data.control.param == 127)
					for (uint i=0; i<ctx->scenes_size; ++i)
						if (ctx->scenes[i].number == (uint)midi_event->data.control.value)
							ctx->jump_instr = ctx->scenes[i].instr;
				break;

		}
	}

	if (sdl_event) {
		if (sdl_event->type == SDL_QUIT || (sdl_event->type == SDL_KEYDOWN && sdl_event->key.keysym.sym == SDLK_q)) {
			ctx->quit = 1;
		//else if (sdl_event->type == SDL_WINDOWEVENT && sdl_event->window.event == SDL_WINDOWEVENT_RESIZED)
		}
	}
};

void cmd_io_poll(struct context *ctx, uint *args) {
	ledm_io_poll(ctx->state, (void*)ctx, on_input);

	// Update note duration variables
	for (int i=0; i<128; ++i) {
		int index = ctx->midi_var_index[i];
		if (index>=0 && ctx->vars[index+1].f>0) {
			ctx->vars[index+3].u = (uint)(ledm_time_ms()-ctx->vars[index+2].u);
			ctx->vars[index+4].u = ctx->midi_timer_offset[i] + ctx->vars[index+3].u;
		}
	}
}

void cmd_io_push(struct context *ctx, uint *args) {
	ledm_io_push(ctx->state);
}

void cmd_waitframe(struct context *ctx, uint *args) {
	ledm_io_wait_frame(ctx->state);
}

struct cmd subcommands_io[] = {
	CMD_SIMPLE("poll", cmd_io_poll),
	CMD_SIMPLE("push", cmd_io_push),
	CMD_LIST_END
};

void add_io_cmds(struct context *ctx) {
	add_cmd(ctx, CMD_SIMPLE("waitframe", cmd_waitframe));
	add_cmd(ctx, CMD_SUBCOMMANDS("io", subcommands_io));
}


// takes a string like "<prefix><integer>" and returns the integer. on failure, or if the integer is not between 0 and max, returns -1
int getSuffixNr(const char *str, const char *prefix, int max) {

	if (strncmp(str, prefix, strlen(prefix)) != 0)
		return -1;

	char *end;
	long value = strtol(str+strlen(prefix), &end, 10);
	if (end == str+strlen(prefix) || *end != '\0' || errno == ERANGE || value < 0 || value > (long)max)
		return -1;

	return (int)value;
}

uint on_new_var(struct context *ctx, const char *name) {

	int suffix = -1;
	int index;
	int index_offset = 0;

	if ( (suffix=getSuffixNr(name, "midi_cc", 127)) >= 0 ) {
		index_offset = 0;
		goto allocate_midi;

	} else if ( (suffix=getSuffixNr(name, "midi_note", 127)) >= 0 ) {
		index_offset = 1;
		goto allocate_midi;

	} else if ( (suffix=getSuffixNr(name, "midi_start", 127)) >= 0 ) {
		index_offset = 2;
		goto allocate_midi;

	} else if ( (suffix=getSuffixNr(name, "midi_timer", 127)) >= 0 ) {
		index_offset = 3;
		goto allocate_midi;

	} else if ( (suffix=getSuffixNr(name, "midi_duration", 127)) >= 0 ) {
		index_offset = 4;
		goto allocate_midi;

	} else {
		index = new_var(ctx);
		ctx->vars_names[index] = strdup(name);
	}


	return index;

allocate_midi:

	index = new_var(ctx); new_var(ctx); new_var(ctx); new_var(ctx); new_var(ctx);
	ctx->midi_var_index[suffix] = index;

	asprintf(ctx->vars_names+index+0, "midi_cc%d", suffix);
	asprintf(ctx->vars_names+index+1, "midi_note%d", suffix);
	asprintf(ctx->vars_names+index+2, "midi_start%d", suffix);
	asprintf(ctx->vars_names+index+3, "midi_timer%d", suffix);
	asprintf(ctx->vars_names+index+4, "midi_duration%d", suffix);
	ctx->vars_types[index+0] = 'f';
	ctx->vars_types[index+1] = 'f';
	ctx->vars_types[index+2] = 'u';
	ctx->vars_types[index+3] = 'u';
	ctx->vars_types[index+4] = 'u';
	ctx->vars[index+0].f = 0;
	ctx->vars[index+1].f = 0;
	ctx->vars[index+2].u = 0;
	ctx->vars[index+3].u = 0;
	ctx->vars[index+4].u = 0;

	return index + index_offset;
}
