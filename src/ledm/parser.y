%{

#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>

#include <libledm.h>
#include <ledm.h>



static struct context *ctx = NULL;
static char *line = NULL;
static char *input = NULL;
static int end_of_file = 0;
static uint at_start_of_line = 1;

static int use_readline = 0;
static FILE *file = NULL;



void fetch_line();
int hexDigitToInt(char c);
int yylex();
void yyerror(char const *);

// implemented in core.c:
void cmd_jump(struct context *ctx, uint *args);
void cmd_jumpunless(struct context *ctx, uint *args);
void cmd_print_var(struct context *ctx, uint *args);
void cmd_scene_jump(struct context *ctx, uint *args);
void cmd_scene_set(struct context *ctx, uint *args);

// implemented in io.c:
void cmd_io_poll(struct context *ctx, uint *args);
void cmd_io_push(struct context *ctx, uint *args);
void cmd_waitframe(struct context *ctx, uint *args);

// implemented in painte.c:
void cmd_paint_mode(struct context *ctx, uint *args);


%}


%define api.value.type union
%token UNRECOGNIZED COMMENT
%token <uint> VARIABLE
%token <struct ledm_rgba> LITERAL_COLOR
%token <char*> LITERAL_STRING
%token <uint> LITERAL_UINT
%token <float> LITERAL_FLOAT
%token <char*> WORD
%token IF
%token WHILE
%token ELSE
%token CONTINUE
%token BREAK
%token LOOP
%token SCENE
%token END 0 "end of file"


%type <uint> literal
%type <struct arg> arg
%type <struct arg> arg_map
%type <struct arg> arg_map_entry
%type <struct arg*> arg_list
%type <struct expr> expr;

%destructor { free($$); } WORD
%destructor { destroy_arg($$); } arg
%destructor { destroy_arg($$); } arg_map
%destructor { destroy_arg($$); } arg_map_entry
%destructor { destroy_arg_list($$); } arg_list
%destructor { destroy_expr($$); } expr

%left '=' '<' '>'
%left '&' '|'
%left '+' '-'
%left '*' '/' '%'
%left '!'
%precedence NEG
%left '^'

%%

input: %empty;

input: input line {
	if (ctx->run_immediately) {
		execute_context(ctx);
		cmd_io_poll(ctx, NULL);
		cmd_io_push(ctx, NULL);
	}
};
input: input ifblock {
	if (ctx->run_immediately) {
		execute_context(ctx);
		cmd_io_poll(ctx, NULL);
		cmd_io_push(ctx, NULL);
	}
};
input: input whileblock {
	if (ctx->run_immediately) {
		execute_context(ctx);
		cmd_io_poll(ctx, NULL);
		cmd_io_push(ctx, NULL);
	}
};
input: input sceneblock {
	if (ctx->run_immediately) {
		execute_context(ctx);
		cmd_io_poll(ctx, NULL);
		cmd_io_push(ctx, NULL);
	}
};

whitespace: %empty | whitespace '\n';

// I apologize for this convoluted code for if/else handling. All of parsing, really.

ifblock: ifstart '}' '\n' {
	*(ctx->blocks->pointer_next) = ctx->instrs_used;
	set_block_end_pointers(ctx->blocks, ctx->instrs_used);
	pop_block(ctx);
};
ifblock: ifstart '}' ELSE '{' {
	uint *arg = malloc(sizeof(uint));
	add_block_end_pointer(ctx->blocks, arg);
	add_instr(ctx, (struct instr){cmd_jump,arg});
	*(ctx->blocks->pointer_next) = ctx->instrs_used;
} input '}' '\n' {
	set_block_end_pointers(ctx->blocks, ctx->instrs_used);
	pop_block(ctx);
};
ifstart: IF '(' expr ')' '{' {
	struct block *b = add_block(ctx, BLOCK_IF);
	uint condition = new_var(ctx);
	add_expr_eval_instr(ctx, $3, condition);
	destroy_expr($3);
	uint *arg = malloc(2*sizeof(uint));
	arg[0] = condition;
	add_instr(ctx, (struct instr){cmd_jumpunless,arg});
	b->pointer_next = arg+1;
} input ;
ifstart: ifstart '}' ELSE IF '(' expr ')' '{' {
	uint *arg = malloc(sizeof(uint));
	add_block_end_pointer(ctx->blocks, arg);
	add_instr(ctx, (struct instr){cmd_jump,arg});
	*(ctx->blocks->pointer_next) = ctx->instrs_used;
	uint condition = new_var(ctx);
	add_expr_eval_instr(ctx, $6, condition);
	destroy_expr($6);
	arg = malloc(2*sizeof(uint));
	arg[0] = condition;
	add_instr(ctx, (struct instr){cmd_jumpunless,arg});
	ctx->blocks->pointer_next = arg+1;
} input;


whileblock: WHILE '(' expr ')' '{' {
	struct block *b = add_block(ctx, BLOCK_WHILE);
	uint condition = new_var(ctx);
	add_expr_eval_instr(ctx, $3, condition);
	destroy_expr($3);
	uint *arg = malloc(2*sizeof(uint));
	arg[0] = condition;
	add_instr(ctx, (struct instr){cmd_jumpunless,arg});
	add_block_end_pointer(b, arg+1);
} input '}' '\n' {
	uint *arg = malloc(sizeof(uint));
	arg[0] = ctx->blocks->first_instr;
	add_instr(ctx, (struct instr){cmd_jump,arg});
	set_block_end_pointers(ctx->blocks, ctx->instrs_used);
	pop_block(ctx);
};


sceneblock: SCENE LITERAL_UINT ':' WORD '{' {
	add_block(ctx, BLOCK_SCENE);
	ctx->scenes = realloc(ctx->scenes, (++ctx->scenes_size) * sizeof(struct scene));
	ctx->scenes[ctx->scenes_size-1].number = $2;
	ctx->scenes[ctx->scenes_size-1].name = $4;
	ctx->scenes[ctx->scenes_size-1].instr = ctx->instrs_used+1;
	add_instr_args(ctx, cmd_scene_set, 1, ctx->instrs_used+1);
	add_instr_args(ctx, cmd_io_poll, 0);
} input '}' '\n' {
	add_instr_args(ctx, cmd_waitframe, 0);
	add_instr_args(ctx, cmd_io_push, 0);
	add_instr_args(ctx, cmd_paint_mode, 1, 0);
	add_instr_args(ctx, cmd_scene_jump, 0);
	pop_block(ctx);
}

sceneblock: SCENE LITERAL_UINT '{' {
	add_block(ctx, BLOCK_SCENE);
	ctx->scenes = realloc(ctx->scenes, (++ctx->scenes_size) * sizeof(struct scene));
	ctx->scenes[ctx->scenes_size-1].number = $2;
	ctx->scenes[ctx->scenes_size-1].name = NULL;
	ctx->scenes[ctx->scenes_size-1].instr = ctx->instrs_used+1;
	add_instr_args(ctx, cmd_scene_set, 1, ctx->instrs_used+1);
	add_instr_args(ctx, cmd_io_poll, 0);
} input '}' '\n' {
	add_instr_args(ctx, cmd_waitframe, 0);
	add_instr_args(ctx, cmd_io_push, 0);
	add_instr_args(ctx, cmd_paint_mode, 1, 0);
	add_instr_args(ctx, cmd_scene_jump, 0);
	pop_block(ctx);
}



line: '\n' | COMMENT '\n' | error '\n' {yyerrok;} ;

line: WORD arg_list '\n' {
	parse_command($1, $2, ctx->cmds);
	free($1);
	destroy_arg_list($2);
};
line: VARIABLE '=' expr '\n' {
	add_expr_eval_instr(ctx, $3, $1);
	destroy_expr($3);
};
line: VARIABLE '\n' {
	add_instr_args(ctx, cmd_print_var, 1, $1);
};
line: CONTINUE '\n' {
	struct block *b = ctx->blocks;
	while (b != NULL && b->type != BLOCK_WHILE)
		b = b->prev;
	if (!b) {
		parser_error(ctx, "No while loop to continue here.");
	} else {
		add_instr_args(ctx, cmd_jump, 1, b->first_instr);
	}
};
line: BREAK '\n' {
	struct block *b = ctx->blocks;
	while (b != NULL && b->type != BLOCK_WHILE)
		b = b->prev;
	if (!b) {
		parser_error(ctx, "No while loop to break here.");
	} else {
		uint *arg = malloc(sizeof(uint));
		add_block_end_pointer(b, arg);
		add_instr(ctx, (struct instr){cmd_jump,arg});
	}
};
line: LOOP ':' '\n' {
	if(!ctx->blocks || ctx->blocks->type != BLOCK_SCENE) {
		parser_error(ctx, "loop statement must sit in scene block.");
	} else {
		add_instr_args(ctx, cmd_scene_set, 1, ctx->instrs_used);
		add_instr_args(ctx, cmd_io_poll, 0);
	}
};



arg_list: %empty { $$ = NULL; } ;

arg_list: arg_list arg {
	$$ = add_to_arg_list($1, $2);
	destroy_arg_list($1);
};



arg: WORD {
	$$.type = ARG_WORD;
	$$.content.word = $1;
};
arg: VARIABLE {
	$$.type = ARG_VAR;
	$$.content.var = $1;
};
arg: literal {
	$$.type = ARG_LITERAL;
	$$.content.var = $1;
};
arg: '-' literal {
	$$.type = ARG_LITERAL;
	$$.content.var = $2;
	if (ctx->vars_types[$2] == 'f') {
		ctx->vars[$2].f *= -1;
	} else if (ctx->vars_types[$2] == 'u') {
		ctx->vars[$2].f = -(float)ctx->vars[$2].u;
		ctx->vars_types[$2] = 'f';
	} else {
		YYERROR;
	}
};
arg: '[' expr ']' {
	$$.type = ARG_VAR;
	$$.content.var = new_var(ctx);
	add_expr_eval_instr(ctx, $2, $$.content.var);
	destroy_expr($2);
};
arg: '{' arg_map whitespace '}' {
	$$ = $2;
};
arg: '{' whitespace '}' {
	$$.type = ARG_MAP;
	$$.content.map = malloc(sizeof(struct arg_map));
	$$.content.map->name = NULL;
};
arg_map: whitespace arg_map_entry {
	$$ = $2;
};
arg_map: arg_map whitespace ',' whitespace arg_map_entry {
	$$ = merge_arg_maps($1, $5);
	destroy_arg($1);
	destroy_arg($5);
};
arg_map_entry: WORD '=' expr {
	//TODO change in order to not unneccessarily evaluate literal expression
	uint index = new_var(ctx);
	add_expr_eval_instr(ctx, $3, index);
	destroy_expr($3);
	$$.type = ARG_MAP;
	$$.content.map = malloc(2 * sizeof(struct arg_map));
	$$.content.map[0].name = $1;
	$$.content.map[0].index = index;
	$$.content.map[1].name = NULL;
};


literal: LITERAL_COLOR {
	$$ = new_var(ctx);
	ctx->vars_types[$$]='c';
	ctx->vars[$$].c = $1;
};
literal: LITERAL_STRING {
	$$ = new_var(ctx);
	ctx->vars_types[$$]='s';
	ctx->vars[$$].s = $1;
};
literal: LITERAL_UINT {
	$$ = new_var(ctx);
	ctx->vars_types[$$]='u';
	ctx->vars[$$].u = $1;
};
literal: LITERAL_FLOAT {
	$$ = new_var(ctx);
	ctx->vars_types[$$]='f';
	ctx->vars[$$].f = $1;
};



expr: literal {
	$$ = create_expr_from_var($1);
};
expr: VARIABLE {
	$$ = create_expr_from_var($1);
};
expr: expr '+' expr {
	$$ = expr_binary_op(ADDITION, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '-' expr {
	$$ = expr_binary_op(SUBTRACTION, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '*' expr {
	$$ = expr_binary_op(MULTIPLICATION, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '/' expr {
	$$ = expr_binary_op(DIVISION, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '=' expr {
	$$ = expr_binary_op(EQUALITY, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '<' expr {
	$$ = expr_binary_op(LOWER_THAN, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '>' expr {
	$$ = expr_binary_op(GREATER_THAN, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '%' expr {
	$$ = expr_binary_op(MODULUS, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '|' expr {
	$$ = expr_binary_op(LOGICAL_OR, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '&' expr {
	$$ = expr_binary_op(LOGICAL_AND, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: expr '^' expr {
	$$ = expr_binary_op(POWER, $1, $3);
	destroy_expr($1);
	destroy_expr($3);
};
expr: '!' expr {
	$$ = expr_unary_op(LOGICAL_NEGATION, $2);
	destroy_expr($2);
};
expr: '-' expr {
	$$ = expr_unary_op(NEGATION, $2);
	destroy_expr($2);
};
expr: WORD '(' ')' {
	int error = 0;
	if (strcmp($1, "rand") == 0) {
		$$ = expr_nullary_op(RANDOM);
	} else if (strcmp($1, "now") == 0) {
		$$ = expr_nullary_op(TIME);
	} else {
		parser_error(ctx, "Unknown nullary maths function '%s'.", $1);
		error = 1;
	}
	free($1);
	if (error)
		YYERROR;
};
expr: WORD '(' expr ')' {
	int error = 0;
	if (strcmp($1, "sin") == 0) {
		$$ = expr_unary_op(SINE, $3);
	} else if (strcmp($1, "cos") == 0) {
		$$ = expr_unary_op(COSINE, $3);
	} else if (strcmp($1, "exp") == 0) {
		$$ = expr_unary_op(EXPONENTIAL, $3);
	} else if (strcmp($1, "uint") == 0) {
		$$ = expr_unary_op(TO_UINT, $3);
	} else if (strcmp($1, "inv") == 0) {
		$$ = expr_unary_op(INVERT_COLOR, $3);
	} else {
		parser_error(ctx, "Unknown unary maths function '%s'.", $1);
		error = 1;
	}
	free($1);
	destroy_expr($3);
	if (error)
		YYERROR;
};
expr: WORD '(' expr ',' expr ')' {
	int error = 0;
	if (strcmp($1, "max") == 0) {
		$$ = expr_binary_op(MAXIMUM, $3, $5);
	} else if (strcmp($1, "min") == 0) {
		$$ = expr_binary_op(MINIMUM, $3, $5);
	} else {
		parser_error(ctx, "Unknown binary maths function '%s'.", $1);
		error = 1;
	}
	free($1);
	destroy_expr($3);
	destroy_expr($5);
	if (error)
		YYERROR;
};
expr: WORD '(' expr ',' expr ',' expr ')' {
	int error = 0;
	if (strcmp($1, "rgb") == 0) {
		$$ = expr_ternary_op(RGB_TO_COLOR, $3, $5, $7);
	} else if (strcmp($1, "hsv") == 0) {
		$$ = expr_ternary_op(HSV_TO_COLOR, $3, $5, $7);
	} else if (strcmp($1, "ifthenelse") == 0) {
		$$ = expr_ternary_op(TERNARY, $3, $5, $7);
	} else {
		parser_error(ctx, "Unknown ternary maths function '%s'.", $1);
		error = 1;
	}
	free($1);
	destroy_expr($3);
	destroy_expr($5);
	destroy_expr($7);
	if (error)
		YYERROR;
};
expr: WORD {
	int error = 0;
	if (strcmp($1, "pi") == 0) {
		$$ = expr_nullary_op(CONST_PI);
	} else {
		parser_error(ctx, "Unknown maths constant '%s'.", $1);
		error = 1;
	}
	free($1);
	if (error)
		YYERROR;
};
expr: '(' expr ')' {
	$$ = $2;
};




%%



struct arg *add_to_arg_list(struct arg *list, struct arg element) {
	uint length = 0;
	if (list)
		while (list[length].type != ARG_END) ++length;

	struct arg *out = malloc( sizeof(struct arg) * (length+2) );
	for (uint i=0; i<length; ++i) {
		out[i] = list[i];

		// have words and maps have their own memory
		if (list[i].type == ARG_WORD) {
			out[i].content.word = strdup(list[i].content.word);
		} else if (list[i].type == ARG_MAP) {
			int maplength = 0;
			for (struct arg_map *map = list[i].content.map; map->name != NULL; ++map)
				++maplength;
			out[i].content.map = malloc( (maplength+1) * sizeof( struct arg_map ) );
			for (int j=0; j<maplength; ++j) {
				out[i].content.map[j].name = strdup(list[i].content.map[j].name);
				out[i].content.map[j].index = list[i].content.map[j].index;
			}
			out[i].content.map[maplength].name = NULL;
		}
	}
	out[length] = element;
	out[length + 1].type = ARG_END;
	
	return out;
}

void destroy_arg(struct arg arg) {

	if (arg.type == ARG_WORD) {
		free(arg.content.word);
	} else if (arg.type == ARG_MAP) {
		for(struct arg_map *map = arg.content.map; map->name != NULL; ++map)
			free(map->name);
		free(arg.content.map);
	}
}

void destroy_arg_list(struct arg *list) {
	if (!list)
		return;

	for (uint i=0; list[i].type != ARG_END; ++i)
		destroy_arg(list[i]);

	free(list);
}

struct arg merge_arg_maps(struct arg a, struct arg b) {
	struct arg out;
	out.type = ARG_MAP;
	out.content.map = NULL;

	if (a.type != ARG_MAP || b.type != ARG_MAP)
		return out;

	// Get total length of both arg maps
	int a_length = 0;
	int b_length = 0;
	for ( struct arg_map *map = a.content.map; map->name != NULL; ++map)
		++a_length;
	for ( struct arg_map *map = b.content.map; map->name != NULL; ++map)
		++b_length;

	out.content.map = malloc( (a_length+b_length+1) * sizeof(struct arg_map));

	for (int i=0; i<a_length; ++i) {
		out.content.map[i].name = strdup(a.content.map[i].name);
		out.content.map[i].index = a.content.map[i].index;
	}
	for (int i=0; i<b_length; ++i) {
		out.content.map[a_length+i].name = strdup(b.content.map[i].name);
		out.content.map[a_length+i].index = b.content.map[i].index;
	}
	out.content.map[a_length+b_length].name = NULL;

	return out;
}

void print_arg_list(struct arg *list) {
	if (!list) {
		printf("NULL\n");
		return;
	}

	for (uint i=0; list[i].type != ARG_END; ++i) {
		if (list[i].type == ARG_WORD)
			printf("ARG_WORD (%s) ", list[i].content.word);
		else if (list[i].type == ARG_VAR)
			printf("ARG_VAR (%u) ", list[i].content.var);
		else if (list[i].type == ARG_LITERAL)
			printf("ARG_LITERAL (%u) ", list[i].content.var);
		else if (list[i].type == ARG_MAP) {
			printf("ARG_MAP: ");
			for (struct arg_map *map = list[i].content.map; map->name != NULL; ++map)
				printf("%s=%u ", map->name, map->index);
		}
	}
	printf("ARG_END\n");
}


uint arg_count(struct arg *list) {
	// check if arg list is zero
	if (!list)
		return 0;

	for (uint i=0; ; ++i)
		if (list[i].type == ARG_END)
			return i;
}


uint arg_count_total(struct arg *list) {
	// check if arg list is zero
	if (!list)
		return 0;

	uint count=0;
	for (uint i=0; ; ++i) {
		if (list[i].type == ARG_END) {
			return count;
		} else if (list[i].type == ARG_MAP) {
			for (struct arg_map *map = list[i].content.map; map->name != NULL; ++map)
				++count;
		} else  {
			++count;
		}
	}
}


int check_arg_var(struct context *ctx, struct arg *list, uint nr, char expected_type) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	// check type of arg
	if (list[nr].type != ARG_VAR && list[nr].type != ARG_LITERAL)
		return 0;
	if (ctx->vars_types[list[nr].content.var] != expected_type)
		return 0;

	return 1;
}


int check_arg_word(struct arg *list, uint nr, const char *expected_word) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	// check type of arg
	if (list[nr].type != ARG_WORD)
		return 0;

	// check whether word is expected
	if (expected_word && strcmp(list[nr].content.word, expected_word) != 0)
		return 0;

	return 1;
}


int check_arg_literal(struct arg *list, uint nr) {

	// check if arg list is long enough
	if (nr >= arg_count(list))
		return 0;

	if (list[nr].type == ARG_LITERAL)
		return 1;

	return 0;
}

int format_string_count(const char *format, uint *len_list_a, uint *len_args_a) {
	const char *current = format;
	uint len_list = 0;
	uint len_args = 0;

	while (*current != '\0') {
		switch (*current) {
		case 's':
		case 'c':
		case 'f':
		case 'u':
			++len_list;
			++len_args;
			++current;
			break;
		case '[':
			do { ++current; } while (*current != ']' && *current != '\0');
			if (*current == '\0') return 0;
			++current;
			++len_list;
			++len_args;
			break;
		case '{':
			do {
				++current;
				if (*current == ',')
					++len_args;
			} while (*current != '}' && *current != '\0');
			if (*current == '\0') return 0;
			++current;
			++len_list;
			++len_args;
			break;
		default:
			return 0;
		}
	}

	if (len_list_a)
		*len_list_a = len_list;
	if (len_args_a)
		*len_args_a = len_args;

	return 1;
}


enum parse_result parse_arg_list(struct context *ctx, struct arg *arg_list, const char *format, uint **args) {

	if (!format)
		return SUCCESS;

	uint expected_nr_list = 0;
	uint expected_nr_args = 0;
	const char *current = format;
	int return_val;

	if (!format_string_count(format, &expected_nr_list, &expected_nr_args))
		return MALFORMED_FORMAT;

	*args = malloc(sizeof(uint) * expected_nr_args);

	uint arg_i = 0;
	for (uint i=0; i<expected_nr_list; ++i) {

		if (!arg_list || arg_list[i].type == ARG_END) {
			return_val = WRONG_ARG_COUNT;
			goto cleanup;
		}

		switch (*current) {

		case 'c':
		case 'u':
		case 's':
			if (check_arg_var(ctx, arg_list, i, *current)) {
				(*args)[arg_i++] = arg_list[i].content.var;
				++current;
				break;
			} else {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}

		case 'f':
			if (check_arg_var(ctx, arg_list, i, 'f')) {
				(*args)[arg_i++] = arg_list[i].content.var;
				++current;
				break;
			} else if (check_arg_var(ctx, arg_list, i, 'u') && check_arg_literal(arg_list, i)) {
				(*args)[arg_i++] = arg_list[i].content.var;
				ctx->vars_types[arg_list[i].content.var] = 'f';
				ctx->vars[arg_list[i].content.var].f = (float) ctx->vars[arg_list[i].content.var].u;
				++current;
				break;
			} else {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
		case '[':
			if (arg_list[i].type != ARG_WORD) {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
			
			int wordmatch = 0;
			uint matchnr = 0;
			while (*current != '\0' && *current != ']') {
				++current;
				int wordlen = 0;
				while (current[wordlen] != '\0' && current[wordlen] != ',' && current[wordlen] != ']') ++wordlen;
				
				if (strncmp(current, arg_list[i].content.word, wordlen) == 0 && wordlen == (int)strlen(arg_list[i].content.word)) {
					wordmatch = 1;
					(*args)[arg_i++] = matchnr;
					while (*current != '\0' && *current != ']') ++current;
				} else {
					current += wordlen;
					++matchnr;
				}
			}
			if (*current != '\0')
				++current;
			if (!wordmatch) {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}
			break;
		case '{':
			if (arg_list[i].type != ARG_MAP) {
				return_val = MISMATCHED_ARGS;
				goto cleanup;
			}

			while (*current != '\0' && *current != '}') {
				++current;
				int segmentlen = 0;
				while (current[segmentlen] != '\0' && current[segmentlen] != ',' && current[segmentlen] != '}') ++segmentlen;

				int index = -1;
				char name[50];
				float defaultval = 0;
				
				// Attempt to parse segment of the form <word>=<float>
				if (sscanf(current, "%49[^=]=%f", name, &defaultval) != 2) {
					return_val = MALFORMED_FORMAT;
					goto cleanup;
				}

				// go see if name matches a name of supplied arg list
				for (struct arg_map *map = arg_list[i].content.map; map->name!=NULL; ++map)
					if (!strcmp(name, map->name))
						index = map->index;


				if (index == -1) {
					index = new_var(ctx);
					ctx->vars[index].f = defaultval;
					ctx->vars_types[index] = 'f';
				}

				(*args)[arg_i++] = index;
				current += segmentlen;
			}
			if (*current != '\0')
				++current;
			break;
		default:
			return_val = MALFORMED_FORMAT;
			goto cleanup;
		}
	}

	if (arg_list[expected_nr_list].type == ARG_END)
		return SUCCESS;
	else
		return_val = WRONG_ARG_COUNT;

cleanup:
	free(*args);
	*args = NULL;
	return return_val;
}


void parse_command(const char *cmd, struct arg *arg_list, struct cmd *command_list) {

	int match = -1;
	for (int i = 0; command_list[i].name != NULL; i++)
		if ( strcmp(cmd, command_list[i].name) == 0 )
			match = i;

	if (match < 0) {
		parser_error(ctx, "Could not find (sub)command '%s'!", cmd);

	} else if (command_list[match].subcommands) {
		if (arg_list && arg_list[0].type == ARG_WORD)
			parse_command(arg_list[0].content.word, arg_list+1, command_list[match].subcommands);
		else if (!arg_list || arg_list[0].type == ARG_END)
			parse_command("", arg_list, command_list[match].subcommands);
		else
			parser_error(ctx, "Need a word as subcommand of '%s'!", cmd);

	} else if (command_list[match].cmd) {
		uint *args = NULL;

		switch (parse_arg_list(ctx, arg_list, command_list[match].format, &args)) {
		case WRONG_ARG_COUNT:
			parser_error(ctx, "Bad number of arguments!");
			break;

		case MISMATCHED_ARGS:
			parser_error(ctx, "Type error when evaluating arguments!");
			break;

		case MALFORMED_FORMAT:
			parser_error(ctx, "Command format string is malformed!");
			break;

		case SUCCESS:
			if (command_list[match].parser)
				command_list[match].parser(ctx, arg_list);
			add_instr(ctx, (struct instr){command_list[match].cmd,args});
			break;
		}

	} else if (command_list[match].parser) {
		command_list[match].parser(ctx, arg_list);

	} else {
		parser_error(ctx, "Something went wrong - the command '%s' seems to lack parsing information!\n", cmd);
	}
}


int hexDigitToInt(char c) {

	if (c >= '0' && c <= '9') {
		return (c - '0');
	} else if (c >= 'a' && c <= 'f') {
		return (10 + c - 'a');
	} else if (c >= 'A' && c <= 'F') {
		return (10 + c - 'F');
	} else {
		return -1;
	}
}


int yylex() {

	if (ctx->quit)
		return EOF;

	if (!input || strlen(input)==0)
		fetch_line();

	if (end_of_file)
		return EOF;

	input += strspn(input, WHITESPACE);
	uint length = 0;

	// discard comments
	if (at_start_of_line && *input == '#') {

		while (input[length] != '\n' && input[length] != '\0') ++length;
		input += length;
		return COMMENT;

	} else {
		at_start_of_line = 0;
	}

	switch (*input) {
	
	case '\n':
	case '\0':
		at_start_of_line = 1;
		++ctx->line_number;
		return *(input++);
	case '=': case '+': case '-': case '*': case '/':
	case '&': case '|': case '%': case ',': case '?':
	case ':': case '@': case '<': case '>': case '[':
	case ']': case '(': case ')': case '^': case '!':
	case '}': case '{':
		return *(input++);


	case '#':
		++input;
		for (uint i=0; i<6 && input[i]!='\0'; ++i)
			if (hexDigitToInt(input[i]) < 0)
				return UNRECOGNIZED;
		yylval.LITERAL_COLOR.r = 16*hexDigitToInt(input[0]) + hexDigitToInt(input[1]);
		yylval.LITERAL_COLOR.g = 16*hexDigitToInt(input[2]) + hexDigitToInt(input[3]);
		yylval.LITERAL_COLOR.b = 16*hexDigitToInt(input[4]) + hexDigitToInt(input[5]);
		yylval.LITERAL_COLOR.a = 255;
		input += 6;
		return LITERAL_COLOR;


	case '"': ;
		++input;
		while (input[length] != '\n' && input[length] != '\0' && input[length] != '"') ++length;

		if (input[length] != '"')
			return UNRECOGNIZED;

		yylval.LITERAL_STRING = strndup(input, length);
		input += length + 1;
		return LITERAL_STRING;


	case '$':
		++input;
		if (!isalpha(*input))
			return UNRECOGNIZED;
		
		while (isdigit(input[length]) || isalpha(input[length]) || input[length]=='_') ++length;

		char *tmp = strndup(input, length);
		yylval.VARIABLE = get_var_index(ctx, tmp);
		free(tmp);
		input += length;
		return VARIABLE;


	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		yylval.LITERAL_UINT = 0;

		while (isdigit(input[length])) ++length;
		uint power_of_ten = 1;
		for (int i=length-1; i>=0; --i) {
			yylval.LITERAL_UINT += power_of_ten * (uint)(input[i] - '0');
			power_of_ten *= 10;
		}
		input += length;

		if (*input != '.')
			return LITERAL_UINT;

		++input;
		yylval.LITERAL_FLOAT = (float) yylval.LITERAL_UINT;
		length = 0;
		while (isdigit(input[length])) ++length;
		for (uint i=0; i<length; ++i)
			yylval.LITERAL_FLOAT += pow(0.1, 1+(float) i) * (float) (input[i] - '0');
		input += length;
		return LITERAL_FLOAT;


	default:
		if (isalpha(*input)) {
			while (isdigit(input[length]) || isalpha(input[length]) || input[length]=='_') ++length;

			if (length==2 && !strncmp(input, "if", 2)) {
				input += length;
				return IF;
			} else if (length==4 && !strncmp(input, "else", 4)) {
				input += length;
				return ELSE;
			} else if (length==5 && !strncmp(input, "while", 5)) {
				input += length;
				return WHILE;
			} else if (length==8 && !strncmp(input, "continue", 8)) {
				input += length;
				return CONTINUE;
			} else if (length==5 && !strncmp(input, "break", 5)) {
				input += length;
				return BREAK;
			} else if (length==4 && !strncmp(input, "loop", 4)) {
				input += length;
				return LOOP;
			} else if (length==5 && !strncmp(input, "scene", 5)) {
				input += length;
				return SCENE;
			} else {
				yylval.WORD = strndup(input, length);
				input += length;
				return WORD;
			}
		} else {
			++input;
			return UNRECOGNIZED;
		}
	}
}

void yyerror(char const *str) {
	parser_error(ctx, "%s", str);
}


void fetch_line() {

	if(line)
		free(line);
	line = NULL;

	if (use_readline) {

		// Display prompt
		char prompt[20];
		printf("\033[1m");
		if(!ctx->blocks)
			snprintf(prompt, 20, "[%2d] ", ctx->line_number);
		else
			snprintf(prompt, 20, "<%2d> ", ctx->line_number);

		//  Get input
		char *orig_line = readline(prompt);
		printf("\033[0m");

		// Unless EOF was encountered, add to history
		if (orig_line) {
			add_history(orig_line);
			line = malloc(sizeof(char) * (strlen(orig_line)+2) );
			sprintf(line, "%s\n", orig_line);
			input = line;
		} else {
			printf("\n");
			end_of_file = 1;
		}
	
	} else if (file) {
		char *line = NULL;
		size_t len = 0;
		if (getline(&line, &len, file) == -1)
			end_of_file = 1;
		input = line;
	} else {
		end_of_file = 1;
	}
};

void parse_interactive(struct context *context) {
	ctx = context;
	ctx->run_immediately = 1;
	ctx->line_number = 1;
	ctx->file_number = 0;
	use_readline = 1;
	end_of_file = 0;

	yyparse();

	if (line)
		free(line);
	line = NULL;
	input = NULL;
}

void parse_file(struct context *context, const char* filename) {
	ctx = context;
	ctx->run_immediately = 0;
	ctx->line_number = 1;
	use_readline = 0;
	end_of_file = 0;

	// If filename is '-', read from stdin, otherwise from the file with that name. Then, set filenr appropriately
	if (strcmp(filename, "-") == 0) {
		file = stdin;
		ctx->file_number = 0;
	} else {
		file = fopen(filename, "r");

		if (file == NULL) {
			error("Cannot parse '%s': %s", filename, strerror(errno));
			++ctx->errors;
			return;
		}

		yyparse();

		++ctx->files_size;
		ctx->files = realloc(ctx->files, ctx->files_size * sizeof(char*));
		ctx->file_number = ctx->files_size;
		ctx->files[ctx->file_number-1] = strdup(filename);
	}

	// Cleanup
	ctx->file_number = 0;
	ctx->line_number = 1;
	if (line)
		free(line);
	if (file != stdin)
		fclose(file);
	file = NULL;
	line = NULL;
	input = NULL;
}


struct block *add_block(struct context *ctx, enum block_type type) {

	struct block *block = malloc(sizeof(struct block));
	block->type = type;
	block->first_instr = ctx->instrs_used;
	block->pointer_next = NULL;
	block->pointers_end = NULL;
	block->count_pointers_end = 0;
	block->prev = NULL;

	if (ctx->blocks)
		block->prev = ctx->blocks;

	ctx->blocks = block;
	return block;
};

void add_block_end_pointer(struct block *block, uint *ptr) {

	++block->count_pointers_end;
	block->pointers_end = realloc(block->pointers_end, block->count_pointers_end * sizeof(uint**));
	block->pointers_end[block->count_pointers_end-1] = ptr;
};

void set_block_end_pointers(struct block *block, uint val) {

	for (uint i=0; i<block->count_pointers_end; ++i)
		*(block->pointers_end[i]) = val;
};

void pop_block(struct context *ctx) {
	if (ctx->blocks) {
		struct block *prev = ctx->blocks->prev;
		free(ctx->blocks->pointers_end);
		free(ctx->blocks);
		ctx->blocks = prev;
	}
}
