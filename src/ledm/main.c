#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <fcntl.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <dlfcn.h>

#include "libledm.h"
#include "ledm.h"


int main(int argc, char **argv) {
	int return_value = 0;

	// Seed random number generation
	srand((uint)time(NULL));

	// Create libledm state
	struct ledm_config *cfg = ledm_config_create();
	ledm_config_load_default(cfg);

	// Handle command-line arguments
	static struct option long_options[] = {
		{"serial", required_argument, NULL, 's'},
		{"no-serial", no_argument, NULL, 'S'},
		{"opc", required_argument, NULL, 'o'},
		{"no-opc", no_argument, NULL, 'O'},
		{"sdl", no_argument, NULL, 'g'},
		{"no-sdl", no_argument, NULL, 'G'},
		{"midi", optional_argument, NULL, 'm'},
		{"no-midi", no_argument, NULL, 'M'},
		{"fps", required_argument, NULL, 'f'},
		{"help", no_argument, NULL, 'h'},
		{"version", no_argument, NULL, 'v'},
		{0, 0, 0, 0}
	};

	const char* usage =
		"Usage: ledm [options] [files]\n"
		"Run ledm scripts. Starts interactive session if no file is specified, otherwise parses files in the order they are given and then executes. If a file is set to -, parses from stdin instead until encountering an EOF. \n"
		"\n"
		"Options:\n"
		"  -i, --init <file>        Load config <file>.\n"
		"  -s, --serial <tty>@<br>  Enable serial output to <tty> with baudrate <br>.\n"
		"  -S, --no-serial          Disable serial output.\n"
		"  -o, --opc <host>:<port>  Enable Open Pixel Control output.\n"
		"  -O, --no-opc             Disable Open Pixel Control output.\n"
		"  -g, --sdl                Enable SDL output.\n"
		"  -G, --no-sdl             Disable SDL output.\n"
		"  -m, --midi               Enable MIDI input.\n"
		"  -M, --no-midi            Disable MIDI input.\n"
		"  -c <client>:<port>       Enables MIDI input and autoconnects to <client>:<port>.\n"
		"  -p <params>              Specifies parameters to be passed along to plugin.\n"
		"  -f, --fps <fps>          Specifies default framerate. Zero turns off framerate limiting.\n"
		"  -h, --help               Print this help message and exit.\n"
		"  -v, --version            Print version information and exit.\n"
		"\n";


	while (1) {
		int option_index = 0;
		int c = getopt_long(argc, argv, "o:Os:SgGmMc:i:x:p:f:Fhv", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
			case 'i': ledm_config_load_file(cfg, optarg); break;
			case 's': ledm_config_set(cfg, "io_serial", optarg); break;
			case 'S': ledm_config_set(cfg, "io_serial", "false"); break;
			case 'o': ledm_config_set(cfg, "io_opc", optarg); break;
			case 'O': ledm_config_set(cfg, "io_opc", NULL); break;
			case 'g': ledm_config_set(cfg, "io_sdl", "true"); break;
			case 'G': ledm_config_set(cfg, "io_sdl", "false"); break;
			case 'm': ledm_config_set(cfg, "io_midi", "true"); break;
			case 'M': ledm_config_set(cfg, "io_midi", "false"); break;
			case 'c':
				ledm_config_set(cfg, "io_midi", "true");
				ledm_config_set(cfg, "io_midi_autoconnect", optarg);
				break;
			case 'p': // TODO
				break;
			case 'f': ledm_config_set(cfg, "io_fps", optarg); break;
			case 'h':
				printf(usage);
				goto cleanup1;
			case 'v':
				printf("ledm " VERSION "\n");
				goto cleanup1;
			case '?':
			default:
				printf("Could not parse command line parameters!\n\n");
				printf(usage);
				return_value = 1;
				goto cleanup1;
		}
	}

	// Initialize libledm state and interpreter context
	struct ledm_state *state = ledm_initialize(cfg);
	struct context *ctx = create_context(state);

	// Add commands to context
	add_core_cmds(ctx);
	add_io_cmds(ctx);
	add_paint_cmds(ctx);

	// Load ledm plugins
	char *file = NULL;
	for (int i=0; ledm_config_get_nth(cfg, "plugin_interpreter", i, &file) == LEDM_SUCCESS; ++i) {
		void* handle = dlopen(file, RTLD_LAZY);
		if (handle == NULL) {
			warning("Could not open ledm interpreter plugin: %s", dlerror());
			continue;
		}

		void (*f)(struct context*, struct ledm_config*) = dlsym(handle, "plugin_entry_point_interpreter");
		if (f)
			f(ctx, cfg);
		else
			warning("Could not find plugin_entry_point_interpreter in plugin: %s", dlerror());
	}
	free(file);

	// From here on: Parsing and execution

	if (optind == argc) { // if in an interactive session since no files specified
		printf("ledm " VERSION "\n");

		// Load readline history from file
		char *history_file = NULL;
		char *config_path = ledm_config_get_path();
		if (config_path != NULL) {
			asprintf(&history_file, "%shistory", config_path);

			// Create history file if it does not exist
			FILE* fd_tmp = fopen(history_file, "ab+");
			if (fd_tmp)
				fclose(fd_tmp);

			// Attempt reading it
			int n = read_history(history_file);
			if (n)
				warning("Cannot read history file: %s", strerror(n));
		}
		free(config_path);

		// Parse interactive input
		parse_interactive(ctx);

		// Be polite
		printf("Bye!\n");

		// Write readline history to file
		if (history_file != NULL) {
			int n = write_history(history_file);
			if (n)
				warning("Cannot write to history file: %s", strerror(n));
			history_truncate_file(history_file, 500);
		}
		free(history_file);


	} else { // if not in an interactive session

		// Parse all files
		for (int i=optind; i<argc; ++i)
			parse_file(ctx, argv[i]);

		// Execute context if no errors were encountered
		if (ctx->errors) {
			printf("Found %d error(s) while parsing. Exiting...\n", ctx->errors);
			return_value = 1;
			goto cleanup2;
		} else {
			execute_context(ctx);
		}
	}


cleanup2:
	destroy_context(ctx);
	ledm_destroy(state);

cleanup1:
	ledm_config_destroy(cfg);

	return return_value;
}

