#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <dlfcn.h>

#include "ledm.h"


struct context *create_context(struct ledm_state *state) {
	struct context *ctx = malloc(sizeof(struct context));

	// Initialize execution state
	ctx->quit = 0;
	ctx->current = 0;
	ctx->next = 0;
	ctx->line_number = 1;
	ctx->file_number = 0;
	ctx->errors = 0;
	ctx->warnings = 0;
	ctx->run_immediately = 0;

	// Initialize other state and graphics
	ctx->state = state;

	// Initialize file table
	ctx->files = NULL;
	ctx->files_size = 0;

	// Initialize command table
	ctx->cmds = malloc(INITIAL_CMDS_SIZE * sizeof(struct cmd));
	ctx->cmds[0] = CMD_LIST_END;
	ctx->cmds_size = INITIAL_CMDS_SIZE;
	ctx->cmds_used = 1;

	// Initialize instruction table
	ctx->instrs = malloc(INITIAL_INSTRS_SIZE * sizeof(struct instr));
	ctx->instrs_linenumber = malloc(INITIAL_INSTRS_SIZE * sizeof(uint));
	ctx->instrs_filenr = malloc(INITIAL_INSTRS_SIZE * sizeof(uint));
	ctx->instrs_size = INITIAL_INSTRS_SIZE;
	ctx->instrs_used = 0;

	// Initialize variable table
	ctx->vars = malloc(INITIAL_VARS_SIZE * sizeof(union ledm_var));
	ctx->vars_names = malloc(INITIAL_VARS_SIZE * sizeof(char*));
	ctx->vars_types = malloc(INITIAL_VARS_SIZE * sizeof(char));
	ctx->vars_size = INITIAL_VARS_SIZE;
	ctx->vars_used = 0;

	// Initialize MIDI data
	for (int i=0; i<128; ++i) {
		ctx->midi_var_index[i] = -1;
		ctx->midi_timer_offset[i] = 0;
	}

	// Initialize block lists
	ctx->blocks = NULL;

	// Initialize scene table and state
	ctx->jump_instr = 0;
	ctx->scenes = NULL;
	ctx->scenes_size = 0;

	return ctx;
}

void destroy_context(struct context *ctx) {

	// Free file table
	for (uint i=0; i < ctx->files_size; ++i)
		free(ctx->files[i]);
	free(ctx->files);

	// Free command table
	free(ctx->cmds);

	// Free instruction table
	for (uint i = 0; i < ctx->instrs_used; ++i)
		free(ctx->instrs[i].args);
	free(ctx->instrs);
	free(ctx->instrs_linenumber);
	free(ctx->instrs_filenr);

	// Free variable table
	for (uint i = 0; i < ctx->vars_used; ++i) {
		free(ctx->vars_names[i]);
		if (ctx->vars_types[i] == 's')
			free(ctx->vars[i].s);
	}
	free(ctx->vars);
	free(ctx->vars_names);
	free(ctx->vars_types);

	// Free block list
	struct block *b;
	while ( (b = ctx->blocks) != NULL ) {
		ctx->blocks = b->prev;
		free(b);
	}

	// Free scene table
	for (uint i=0; i<ctx->scenes_size; ++i)
		free(ctx->scenes[i].name);
	free(ctx->scenes);

	// Free context itself
	free(ctx);
}



void execute_context(struct context *ctx) {
	while (ctx->next < ctx->instrs_used && !ctx->quit && !ctx->blocks) {
		ctx->current = ctx->next;
		ctx->next++;
		struct instr instr = ctx->instrs[ctx->current];
		instr.func(ctx, instr.args);
	}
}


void add_instr(struct context *ctx, struct instr instr) {
	if (ctx->instrs_used == ctx->instrs_size) {
		ctx->instrs_size *= 2;
		ctx->instrs = realloc(ctx->instrs, ctx->instrs_size * sizeof(struct instr));
		ctx->instrs_linenumber = realloc(ctx->instrs_linenumber, ctx->instrs_size * sizeof(uint));
		ctx->instrs_filenr = realloc(ctx->instrs_filenr, ctx->instrs_size * sizeof(uint));
	}
	ctx->instrs[ctx->instrs_used] = instr;
	ctx->instrs_linenumber[ctx->instrs_used] = ctx->line_number;
	ctx->instrs_filenr[ctx->instrs_used] = ctx->file_number;
	++ctx->instrs_used;
}

void add_instr_args(struct context *ctx, void (*instr)(struct context*, uint*), int num, ...) {

	uint *args = NULL;

	if (num>0) {
		args = malloc(num * sizeof(uint));
		va_list list;
		va_start(list, num);
		for (int i=0; i<num; ++i)
			args[i] = va_arg(list, unsigned int);
		va_end(list);
	}

	add_instr(ctx, (struct instr){instr, args});
}

uint new_var(struct context *ctx) {
	if (ctx->vars_used == ctx->vars_size) {
		ctx->vars_size *= 2;
		ctx->vars = realloc(ctx->vars, ctx->vars_size * sizeof(union ledm_var));
		ctx->vars_names = realloc(ctx->vars_names, ctx->vars_size * sizeof(char*));
		ctx->vars_types = realloc(ctx->vars_types, ctx->vars_size * sizeof(char));
	}
	ctx->vars[ctx->vars_used].u = 0;
	ctx->vars_names[ctx->vars_used] = NULL;
	ctx->vars_types[ctx->vars_used] = '%'; // set type to uninitialized and let caller set it properly.
	return ctx->vars_used++;
}

uint get_var_index(struct context *ctx, const char *name) {
	if (name)
		for (uint i = 0; i < ctx->vars_used; i++)
			if(ctx->vars_names[i] && !strcmp(name, ctx->vars_names[i]))
				return i;

	return on_new_var(ctx, name);
}

void print_var(struct context *ctx, uint index) {
	if (index >= ctx->vars_used)
		return;


	if (ctx->vars_names[index])
		printf("%c $%s ", ctx->vars_types[index], ctx->vars_names[index]);
	else
		printf("%c $%u ", ctx->vars_types[index], index);

	switch (ctx->vars_types[index]) {
	case 's': printf("= %s", ctx->vars[index].s); break;
	case 'c': printf("= #%02hhx%02hhx%02hhx",
				ctx->vars[index].c.r,
				ctx->vars[index].c.g,
				ctx->vars[index].c.b);
		break;
	case 'f': printf("= %f", ctx->vars[index].f); break;
	case 'u': printf("= %u", ctx->vars[index].u); break;
	case '%': break;
	default: printf("- unknown variable type!"); break;
	}
	printf("\n");
}

void add_cmd(struct context *ctx, struct cmd cmd) {

	// If command exists, overwrite it
	for (uint i=0; i+1<ctx->cmds_used; ++i) {
		if (!strcmp(ctx->cmds[i].name, cmd.name)) {
			warning("Overwriting previously registered command '%s'!", cmd.name);
			ctx->cmds[i] = cmd;
			return;
		}
	}

	// Increase allocated size if necessary
	if (ctx->cmds_used == ctx->cmds_size) {
		ctx->cmds_size *= 2;
		ctx->cmds = realloc(ctx->cmds, ctx->cmds_size * sizeof(struct cmd));
	}

	// Add new command at end of list
	ctx->cmds[ctx->cmds_used-1] = cmd;
	ctx->cmds[ctx->cmds_used] = CMD_LIST_END;
	ctx->cmds_used++;
}

uint remove_cmd(struct context *ctx, char* name) {
	uint matches = 0;
	for (uint i=0; i+matches<ctx->cmds_used; ++i) {
		if (i+matches+1<ctx->cmds_used && !strcmp(ctx->cmds[i].name, name))
			matches++;
		ctx->cmds[i] = ctx->cmds[i+matches];
	}
	ctx->cmds_used -= matches;
	if (matches == 0)
		warning("Tried to remove command '%s', which did not exist in the first place.", name);
	return matches;
}


void printf_to_stderr(const char *format, ...) {
	va_list args;
	va_start (args, format);
	vfprintf (stderr, format, args);
	va_end (args);
}
