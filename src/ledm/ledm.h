#ifndef LEDM_H
#define LEDM_H

#include <stdlib.h>
#include "libledm.h"


// DEFINES
#define WHITESPACE " \f\r\t\v"
#define PI    3.14159265358979323846
#define EULER 2.7182818284590452354
#define MAX_EXPR_SIZE 50 // maximal number of operators+vars in maths formulae

// Initial sizes for dynamic arrays in context
#define INITIAL_CMDS_SIZE 32
#define INITIAL_INSTRS_SIZE 64
#define INITIAL_VARS_SIZE 64


// TYPES
struct context; // defined further down in this file

struct instr {
	void (*func)(struct context*, uint*);
	uint *args;
};
// Represents all data required to execute an instruction: A pointer to the function to execute together with a uint pointer to be passed along to it. This pointer points to the first block of a region of memory that holds a variable number of uints, which often are to be interpreted as the indices of the variables in the context variable table vars on which the function is supposed to act. The *args of instructions that get stored in the context instruction table instrs get freed upon context destruction.

struct arg_map {
	char *name;
	uint index;
};

struct arg {
	enum {ARG_WORD, ARG_VAR, ARG_LITERAL, ARG_MAP, ARG_END} type;
	union {char *word; uint var; struct arg_map* map;} content;
};

struct cmd {
	const char *name;
	void (*parser)(struct context*, struct arg *args);
	struct cmd *subcommands;
	void (*cmd)(struct context*, uint*);
	const char *format;
};

enum math_operation {
	LOAD_VAR = '.',
	ADDITION = '+',
	SUBTRACTION = '-',
	MULTIPLICATION = '*',
	DIVISION = '/',
	POWER = '^',
	LOWER_THAN = '<',
	GREATER_THAN = '>',
	EQUALITY = '=',
	TERNARY = '?',
	LOGICAL_NEGATION = '!',
	NEGATION = '~',
	MINIMUM = 'm',
	MAXIMUM = 'M',
	COSINE = 'c',
	SINE = 's',
	EXPONENTIAL = 'e',
	TO_UINT = 'u',
	MODULUS = '%',
	RANDOM = 'r',
	CONST_PI = 'p',
	LOGICAL_AND = '&',
	LOGICAL_OR = '|',
	TIME = 'n',
	RGB_TO_COLOR = '#',
	HSV_TO_COLOR = '@',
	INVERT_COLOR = 'I'
};

struct expr {
	char *formula;
	uint *vars;
};


enum block_type { BLOCK_IF, BLOCK_WHILE, BLOCK_SCENE };
struct block {
	enum block_type type;
	uint   first_instr;
	uint  *pointer_next;
	uint  **pointers_end;
	uint count_pointers_end;
	struct block *prev;
};

struct scene {
	uint number;
	char *name;
	uint instr;
};


// The following macros allow for a more readable construction of cmd_t's:
#define CMD_PARSER(name, parser) (struct cmd){name, parser, NULL, NULL, NULL}
#define CMD_SUBCOMMANDS(name, list) (struct cmd){name, NULL, list, NULL, NULL}
#define CMD_SIMPLE(name, command) (struct cmd){name, NULL, NULL, command, NULL}
#define CMD_ARGUMENTS(name, command, args) (struct cmd){name, NULL, NULL, command, args}
#define CMD_ARG_ONPARSE(name, command, args, onparse) (struct cmd){name, onparse, NULL, command, args}
#define CMD_LIST_END (struct cmd){NULL, NULL, NULL, NULL, NULL}

// CONTEXT

struct context {

	// Script execution state
	uint quit;
	uint current; // index of the currently executing instruction
	uint next; // index of the next instruction to execute
	uint line_number; // line number of the line currently being parsed
	uint file_number; // nr of file currently being parsed
	uint errors; // number of parsing and runtime errors
	uint warnings; // number of parsing and runtime warnings
	uint run_immediately; // whether to attempt execution during parsing


	// libledm state
	struct ledm_state *state;


	// File table
	char** files; // contains names of files that have been parsed for error output
	size_t files_size; // contains current allocated size for filename storage


	// Command table
	struct cmd* cmds; // contains cmd_t for each of the commands registered for parsing. always ends with a cmd_t filled with nulls, i.e. CMD_LIST_END
	size_t cmds_used; // contains the number of commands in the table, including the final CMD_LIST_END
	size_t cmds_size; // contains the maximal number of commands (including final CMD_LIST_END) that cmds can hold before reallocating it


	// Instruction table
	struct instr* instrs; // contains all instructions that have been parsed
	uint*    instrs_linenumber; // for every instruction, contains the line number it originally came from
	uint*    instrs_filenr; // for every instruction, contains index to filename in file table where it originally came from
	size_t   instrs_used; // contains the number of instructions in the table at the moment
	size_t   instrs_size; // contains the maximum number the instruction table can hold without reallocating


	// Variable table
	union ledm_var* vars; // contains variable contents
	char** vars_names; // contains pointer to dynamically allocated var names or NULL
	char*  vars_types; // contains a char representing variable type or '%' for uninitialized
	size_t vars_used; // contains the number of variables in use at the moment
	size_t vars_size; // contains the number of variables the table can hold without reallocating


	// MIDI state
	int midi_var_index[128]; // contains start of [midi_cc midi_note midi_start midi_timer midi_duration] block in variable table for each midi index, or -1 if not used yet
	uint midi_timer_offset[128]; //contains time that note had been on already before last note on event


	// Parser state to keep track of incomplete blocks
	struct block *blocks;


	// Scene table and state
	uint jump_instr;
	struct scene *scenes;
	size_t scenes_size;
};


// Creation, destruction and modification of context - implemented in context.c
struct context *create_context(struct ledm_state*);
void destroy_context(struct context *ctx);
void execute_context(struct context *ctx);
void add_instr(struct context *ctx, struct instr instr);
void add_instr_args(struct context *ctx, void (*inst)(struct context*, uint*), int num, ...);
void add_cmd(struct context *ctx, struct cmd cmd); // adds command to parsing table
uint remove_cmd(struct context *ctx, char* name); // removes parsing information associated with that name
uint new_var(struct context *ctx); // adds a new anonymous variable of unitialized type '%' and returns index
uint get_var_index(struct context *ctx, const char *name); // looks up index of a variable based on its name, creating it if necessary
void print_var(struct context *ctx, uint index); // prints variable type, name and value, e.g. 'u $counter = 15'


// Parsing commands - implemented in parser.y
void destroy_arg(struct arg arg);
struct arg merge_arg_maps(struct arg a, struct arg b);
struct arg *add_to_arg_list(struct arg *list, struct arg element);
void destroy_arg_list(struct arg *list);
void print_arg_list(struct arg *list);
uint arg_count(struct arg *list);
uint arg_count_total(struct arg *list);
int check_arg_var(struct context *ctx, struct arg *list, uint nr, char expected_type);
int check_arg_word(struct arg *list, uint nr, const char *expected_word);
int check_arg_literal(struct arg *list, uint nr);
void parse_command(const char *cmd, struct arg *arg_list, struct cmd *command_list);
enum parse_result {SUCCESS, WRONG_ARG_COUNT, MISMATCHED_ARGS, MALFORMED_FORMAT};
int format_string_count(const char *format, uint *len_list, uint *len_args);
enum parse_result parse_arg_list(struct context *ctx, struct arg *arg_list, const char *format, uint **args);
void parse_interactive(struct context *ctx);
void parse_file(struct context *ctx, const char* filename);
struct block *add_block(struct context *ctx, enum block_type type);
void add_block_end_pointer(struct block *block, uint *ptr);
void set_block_end_pointers(struct block *block, uint val);
void pop_block(struct context *ctx);



// Adding commands - implemented in core.c, paint.c and io.c
void add_core_cmds(struct context *ctx);
void add_paint_cmds(struct context *ctx);
void add_io_cmds(struct context *ctx);


uint on_new_var(struct context *ctx, const char *name); // called whenever interpreter encounters a previously undefined named variable, implemented in io.c to automatically fill variables with some input




// Math functions to deal with expr's - implemented in maths.c
struct expr create_expr_from_var(uint var);
void destroy_expr(struct expr expr);
uint expr_count_vars(struct expr expr);
struct expr expr_nullary_op(enum math_operation op);
struct expr expr_unary_op(enum math_operation op, struct expr expr1);
struct expr expr_binary_op(enum math_operation op, struct expr expr1, struct expr expr2);
struct expr expr_ternary_op(enum math_operation op, struct expr expr1, struct expr expr2, struct expr expr3);
void add_expr_eval_instr(struct context *ctx, struct expr expr, uint index);
void cmd_maths(struct context *ctx, uint *args);





// ERROR HANDLING
// All these error and warning printing functions accept a format string and a variable number of variables as arguments, just like printf(). Parts of the program that do not deal with interpretation or execution can print errors or warnings with error(...) and warning(...). Errors and warnings that occur during parsing or execution of instructions, respectively, should be printed using the additional prefixes parser_ and runtime_, which then also increase the error and warning counters of the context. In order to do this, they take as an additional first parameter a pointer to the context. These macros call a printf_to_stderr() function implemented in interpreter.c to not draw in any headers here.

void printf_to_stderr(const char *format, ...);

#define parser_error(CONTEXT, FORMAT, ...) do { \
	if (CONTEXT->file_number == 0) {\
		printf_to_stderr("\033[31;1mParser error\033[0m: " FORMAT "\n", ##__VA_ARGS__); \
	} else { \
		printf_to_stderr("\033[31;1mParser error\033[0m in \033[36;1m%s:%d\033[0m: " FORMAT "\n", CONTEXT->files[CONTEXT->file_number-1], CONTEXT->line_number, ##__VA_ARGS__); \
	} \
	++CONTEXT->errors; \
	} while (0)

#define runtime_error(CONTEXT, FORMAT, ...) do { \
	if (CONTEXT->instrs_filenr[CONTEXT->current] == 0) {\
		printf_to_stderr("\033[31;1mRuntime error\033[0m: " FORMAT "\n", __func__+4, ##__VA_ARGS__); \
	} else { \
		printf_to_stderr("\033[31;1mRuntime error\033[0m in instruction \033[36;1m%s (%s:%d)\033[0m: " FORMAT "\n", __func__+4, CONTEXT->files[CONTEXT->instrs_filenr[CONTEXT->current]-1], CONTEXT->instrs_linenumber[CONTEXT->current], ##__VA_ARGS__); \
	} \
	++CONTEXT->errors; \
	} while (0)

#define parser_warning(CONTEXT, FORMAT, ...) do { \
	if (CONTEXT->file_number == 0) {\
		printf_to_stderr("\033[31;1mParser warning\033[0m: " FORMAT "\n", ##__VA_ARGS__); \
	} else { \
		printf_to_stderr("\033[31;1mParser warning\033[0m in \033[36;1m%s:%d\033[0m: " FORMAT "\n", CONTEXT->files[CONTEXT->file_number-1], CONTEXT->line_number, ##__VA_ARGS__); \
	} \
	++CONTEXT->warnings; \
	} while (0)

#define runtime_warning(CONTEXT, FORMAT, ...) do { \
	if (CONTEXT->instrs_filenr[CONTEXT->current] == 0) {\
		printf_to_stderr("\033[31;1mRuntime warning\033[0m: " FORMAT "\n", __func__+4, ##__VA_ARGS__); \
	} else { \
		printf_to_stderr("\033[31;1mRuntime warning\033[0m in instruction \033[36;1m%s (%s:%d)\033[0m: " FORMAT "\n", __func__+4, CONTEXT->files[CONTEXT->instrs_filenr[CONTEXT->current]-1], CONTEXT->instrs_linenumber[CONTEXT->current], ##__VA_ARGS__); \
	} \
	++CONTEXT->warnings; \
	} while (0)

#define error(FORMAT, ...) do { \
	printf_to_stderr("\033[31;1mError\033[0m: " FORMAT "\n", ##__VA_ARGS__); \
	} while (0)

#define warning(FORMAT, ...) do { \
	printf_to_stderr("\033[35;1mWarning\033[0m: " FORMAT "\n", ##__VA_ARGS__); \
	} while (0)



#endif // LEDM_H

