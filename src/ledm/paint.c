#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "ledm.h"

#define MAX_PAINT_PARAMS 16

void cmd_paint(struct context *ctx, uint *args) {
	
	// args content: patternNr patternArgCount painterNr painterArgCount <pattern arg var nrs> <painter arg var nrs>
	// build from that a union ledm_var* for both pattern and painter, then call libledm

	static union ledm_var pattern_params[MAX_PAINT_PARAMS];
	static union ledm_var painter_params[MAX_PAINT_PARAMS];
	uint pattern = args[0];
	uint pattern_arg_count = args[1];
	uint painter = args[2];
	uint painter_arg_count = args[3];

	for (uint i=0; i<pattern_arg_count;++i)
		pattern_params[i] = ctx->vars[args[4+i]];
	for (uint i=0; i<painter_arg_count;++i)
		painter_params[i] = ctx->vars[args[4+pattern_arg_count+i]];

	ledm_paint(ctx->state, pattern, pattern_params, painter, painter_params);
}


void parser_paint(struct context *ctx, struct arg *arg_list) {

	// find separator between pattern and painter parts
	int separator_pos = -1;
	for (uint i=1; arg_list[i].type != ARG_END; ++i)
		if (check_arg_word(arg_list, i, "with"))
			separator_pos = i;
	if (separator_pos < 0) {
		parser_error(ctx, "Use the word 'with' to separate pattern and painter.");
		return;
	}

	// find pattern
	if (!check_arg_word(arg_list, 0, NULL)) {
		parser_error(ctx, "Please specify pattern.");
		return;
	}
	char *pattern_name = arg_list[0].content.word;
	int pattern = ledm_get_pattern_number(ctx->state, pattern_name);
	if (pattern < 0) {
		parser_error(ctx, "Unknown pattern.");
		return;
	}

	// find painter
	if (!check_arg_word(arg_list, separator_pos+1, NULL)) {
		parser_error(ctx, "Please specify painter.");
		return;
	}
	char *painter_name = arg_list[separator_pos+1].content.word;
	int painter = ledm_get_painter_number(ctx->state, painter_name);
	if (painter < 0) {
		parser_error(ctx, "Unknown painter.");
		return;
	}

	// create separate pattern and painter arg lists
	arg_list[separator_pos].type = ARG_END;
	struct arg *pattern_arg_list = arg_list + 1;
	struct arg *painter_arg_list = arg_list + separator_pos + 2;
	uint num_pattern_arg, num_painter_arg;
	format_string_count(ledm_get_pattern_format(ctx->state, pattern_name), NULL, &num_pattern_arg);
	format_string_count(ledm_get_painter_format(ctx->state, painter_name), NULL, &num_painter_arg);

	// Define args pointers
	uint *painter_args = NULL;
	uint *pattern_args = NULL;

	if ( parse_arg_list(ctx, pattern_arg_list, ledm_get_pattern_format(ctx->state, pattern_name), &pattern_args) != SUCCESS ) {
		parser_error(ctx, "Could not parse pattern arguments.");
		goto cleanup;
	}
	
	if ( parse_arg_list(ctx, painter_arg_list, ledm_get_painter_format(ctx->state, painter_name), &painter_args) != SUCCESS ) {
		parser_error(ctx, "Could not parse painter arguments.");
		goto cleanup;
	}

	// undo splitting of arg list so that it will get freed properly
	arg_list[separator_pos].type = ARG_WORD;
	

	// Build argument pointer
	uint *args = malloc(sizeof(uint) * (4+num_pattern_arg+num_painter_arg) );
	args[0] = pattern;
	args[1] = num_pattern_arg;
	args[2] = painter;
	args[3] = num_painter_arg;
	for (uint i=0; i<num_pattern_arg; ++i)
		args[4+i] = pattern_args[i];
	for (uint i=0; i<num_painter_arg; ++i)
		args[4+num_pattern_arg+i] = painter_args[i];

	// Add draw instruction
	add_instr(ctx, (struct instr){cmd_paint, args});

cleanup:
	free(painter_args);
	free(pattern_args);
}


void cmd_paint_mode(struct context *ctx, uint *args) {
	switch (*args) {
		default:
		case 0: ledm_paint_set_mode(ctx->state, OVERWRITE); break;
		case 1: ledm_paint_set_mode(ctx->state, MULTIPLY); break;
		case 2: ledm_paint_set_mode(ctx->state, SCREEN); break;
		case 3: ledm_paint_set_mode(ctx->state, ON_BLACK); break;
	}
}


void add_paint_cmds(struct context *ctx) {
	add_cmd(ctx, CMD_PARSER("paint", parser_paint));
	add_cmd(ctx, CMD_ARGUMENTS("paintmode", cmd_paint_mode, "[overwrite,multiply,screen,on_black]"));
}
