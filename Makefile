VERSION=0.0.1

CFLAGS+=-DVERSION='"$(VERSION)"' -Wall -Wextra -Werror -Wno-unused-parameter -std=c99
DEBUG=-g -ggdb -D_DEBUG
RELEASE=-O3
LDFLAGS+=-lm -lreadline -lSDL2 -ldl -lasound
# -lvlc -lpulse-simple -lpulse -lsamplerate -lfftw3 -DUSE_FFTW
INCLUDE+=-Isrc/libledm -Isrc/ledm
PREFIX?=/usr/local
_INSTDIR=$(DESTDIR)$(PREFIX)
BINDIR?=$(_INSTDIR)/bin
MANDIR?=$(_INSTDIR)/share/man
PCDIR?=$(_INSTDIR)/lib/pkgconfig
OUTDIR=.build
.DEFAULT_GOAL=all
.PHONY: all clean install debug docs arduino games


LIBLEDM_OBJECTS=\
	$(OUTDIR)/libledm/config.o \
	$(OUTDIR)/libledm/log.o \
	$(OUTDIR)/libledm/state.o \
	$(OUTDIR)/libledm/canvas.o \
	$(OUTDIR)/libledm/register.o \
	$(OUTDIR)/libledm/layouts.o \
	$(OUTDIR)/libledm/util_time.o \
	$(OUTDIR)/libledm/paint.o \
	$(OUTDIR)/libledm/io.o \
	$(OUTDIR)/libledm/io_midi.o \
	$(OUTDIR)/libledm/io_sdl.o \
	$(OUTDIR)/libledm/io_serial.o \
	$(OUTDIR)/libledm/io_opc.o \
	$(OUTDIR)/libledm/painters.o \
	$(OUTDIR)/libledm/patterns.o \
	$(OUTDIR)/libledm/particles.o \
	$(OUTDIR)/libledm/util_hsv2rgb.o \
	$(OUTDIR)/libledm/util_noise1234.o \
	$(OUTDIR)/libledm/util_opc_client.o

LEDM_OBJECTS=\
	$(OUTDIR)/ledm/main.o \
	$(OUTDIR)/ledm/context.o \
	$(OUTDIR)/ledm/core.o \
	$(OUTDIR)/ledm/maths.o \
	$(OUTDIR)/ledm/parser.o \
	$(OUTDIR)/ledm/io.o \
	$(OUTDIR)/ledm/paint.o

LEDM_NEXT_OBJECTS=\
	$(OUTDIR)/ledm_next/main.o\
	$(OUTDIR)/ledm_next/source.o\
	$(OUTDIR)/ledm_next/song.o\
	$(OUTDIR)/ledm_next/init_openGL.o\
	$(OUTDIR)/ledm_next/load_song.o\
	$(OUTDIR)/ledm_next/preprocess_song.o\
	$(OUTDIR)/ledm_next/play_song.o\
	$(OUTDIR)/ledm_next/logmsg.o

PLUGINS=\
	$(OUTDIR)/plugins/aaron.so


$(OUTDIR)/%.o: src/%.c
	@mkdir -p $(dir $@)
	$(CC) -c -o $@ $(CFLAGS) $(RELEASE) -fPIC $(INCLUDE) $<

$(OUTDIR)/%.so: src/%.c
	@mkdir -p $(dir $@)
	$(CC) -shared -o $@ $(CFLAGS) $(RELEASE) -fPIC $(INCLUDE) $<

$(OUTDIR)/libledm.a: src/libledm/libledm.h src/libledm/libledm_utils.h src/libledm/libledm_internal.h $(LIBLEDM_OBJECTS)
	@mkdir -p $(dir $@)
	ar rcs $@ $(LIBLEDM_OBJECTS)

$(OUTDIR)/libledm.so: src/libledm/libledm.h src/libledm/libledm_utils.h src/libledm/libledm_internal.h $(LIBLEDM_OBJECTS)
	@mkdir -p $(dir $@)
	$(CC) $(LDFLAGS) $(CFLAGS) -shared -o $@ $(LIBLEDM_OBJECTS)

games:  games/snake games/conway games/connect_four

games/%: src/games/%.c $(OUTDIR)/libledm.a
	@mkdir -p $(dir $@)
	$(CC) $(LDFLAGS) $(CFLAGS) $(INCLUDE) -rdynamic -o $@ $< $(OUTDIR)/libledm.a

ledm_next: src/ledm_next/ledm.h $(LEDM_NEXT_OBJECTS) $(OUTDIR)/libledm.a
	$(CC) $(LDFLAGS) -lEGL -lGLESv2 $(INCLUDE) -rdynamic -o $@ $^

src/ledm_next/load_song.c: src/ledm_next/load_song.y
	bison -o $@ $<

ledm_test: src/ledm_test/main.c $(OUTDIR)/libledm.a
	@mkdir -p $(dir $@)
	$(CC) $(LDFLAGS) $(CFLAGS) $(INCLUDE) -rdynamic -o $@ $< $(OUTDIR)/libledm.a

ledmplay: src/ledm_play/main.c
	$(CC) $(CFLAGS) -lasound -o $@ $^

ledm_live: src/ledm_live/main.c $(OUTDIR)/libledm.a
	@mkdir -p $(dir $@)
	$(CC) $(LDFLAGS) $(CFLAGS) $(INCLUDE) -rdynamic -o $@ $< $(OUTDIR)/libledm.a

src/ledm/parser.c: src/ledm/parser.y
	bison -o $@ $<

ledm: src/ledm/ledm.h $(LEDM_OBJECTS) $(OUTDIR)/libledm.a
	$(CC) $(LDFLAGS) $(INCLUDE) -rdynamic -o $@ $^






arduino: src/arduino/arduino.ino
	cd src/arduino && \
	arduino-cli compile --fqbn arduino:avr:pro . && \
	arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:pro .

doc/%: doc/%.scd
	scdoc < $< > $@




all: ledm ledm_test ledm_live games docs $(PLUGINS) $(OUTDIR)/libledm.so

debug: RELEASE=$(DEBUG)
debug: all

docs: doc/ledm.1 doc/ledm.5

clean:
	rm -rf $(OUTDIR) ./ledm ./ledm_test games/

install: all
	mkdir -p $(BINDIR) $(MANDIR)/man1 $(MANDIR)/man5 $(PCDIR)
	install -m755 ledm $(BINDIR)/ledm


