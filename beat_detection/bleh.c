#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#include <pulse/error.h>
#include "./kissfft/kiss_fft.h"

#ifndef M_PI
#define M_PI 3.14159265358979324
#endif

#define N 30
#define BUFSIZE 1024

void test_fft(const char* title, const kiss_fft_cpx in[N], kiss_fft_cpx out[N])
{
  kiss_fft_cfg cfg;

  printf("%s\n", title);

  if ((cfg = kiss_fft_alloc(N, 0/*is_inverse_fft*/, NULL, NULL)) != NULL)
  {
    size_t i;

    kiss_fft(cfg, in, out);
    free(cfg);

    for (i = 0; i < N; i++)
      printf(" in[%2zu] = %+f , %+f    "
             "out[%2zu] = %+f , %+f\n",
             i, in[i].r, in[i].i,
             i, out[i].r, out[i].i);
  }
  else
  {
    printf("not enough memory?\n");
    exit(-1);
  }
}

/* A simple routine calling UNIX write() in a loop */
static ssize_t loop_write(int fd, const void*data, size_t size) {
    ssize_t ret = 0;
    while (size > 0) {
        ssize_t r;
        if ((r = write(fd, data, size)) < 0)
            return r;
        if (r == 0)
            break;
        ret += r;
        data = (const uint8_t*) data + r;
        size -= (size_t) r;
    }
    return ret;
}

int test_mp3() {
    // kiss_fft_cpx in[N], out[N];
    // size_t i;

    // int fs_sample_rate = 441000; // 44.1kHz // avg # of samples obtained in 1 sec
    // double bl_block_len = pow(2, 10); // selected # of samples
    // int fn_bandwidth = fs_sample_rate / 2; // Nyquist freq, maximum freq that can be determined
    // int d_meas_dur = bl_block_len / fs_sample_rate; // duration in ms of the whole sample 

        /* The sample type to use */
    static const pa_sample_spec ss = {
        .format = PA_SAMPLE_S16LE,
        .rate = 44100,
        .channels = 2
    };
    pa_simple *s = NULL;
    int ret = 1;
    int error;
    /* Create the recording stream */
    if (!(s = pa_simple_new(NULL, "test_file", PA_STREAM_RECORD, NULL, "record", &ss, NULL, NULL, &error))) {
        fprintf(stderr, __FILE__": pa_simple_new() failed: %s\n", pa_strerror(error));
        goto finish;
    }
    for (;;) {
        uint8_t buf[BUFSIZE];
        /* Record some data ... */
        if (pa_simple_read(s, buf, sizeof(buf), &error) < 0) {
            fprintf(stderr, __FILE__": pa_simple_read() failed: %s\n", pa_strerror(error));
            goto finish;
        }
        /* And write it to STDOUT */
        if (loop_write(STDOUT_FILENO, buf, sizeof(buf)) != sizeof(buf)) {
            fprintf(stderr, __FILE__": write() failed: %s\n", strerror(errno));
            goto finish;
        }
    }
    ret = 0;
finish:
    if (s)
        pa_simple_free(s);
    return ret;
}

int main(void)
{
//   kiss_fft_cpx in[N], out[N];
//   size_t i;

//   for (i = 0; i < N; i++)
//     in[i].r = in[i].i = 0;
// //   test_fft("Zeroes (complex)", in, out);

//   for (i = 0; i < N; i++)
//     in[i].r = 1, in[i].i = 0;
// //   test_fft("Ones (complex)", in, out);

//   for (i = 0; i < N; i++)
//     in[i].r = sin(2 * M_PI * 4 * i / N), in[i].i = 0;
// //   test_fft("SineWave (complex)", in, out);


    test_mp3();
    return 0;
}

	



// gcc bleh.c -o bleh # compile basic
// gcc bleh.c -o bleh -lm # compile with math library
// gcc -o bleh bleh.c ./kissfft/tools/kiss_fftr.c ./kissfft/kiss_fft.c -lm # compile with kiss_fft